package sbp.generator.popup.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import sbp.generator.ui.SBPUIGenerator;
import sbp.generator.utils.WorkbenchUtils;

public class GenerateSBPUIFilesAction implements IObjectActionDelegate {

	private Shell shell;

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		IFile smlFile = WorkbenchUtils.getSelectedSMLFile();
		SBPUIGenerator generator = new SBPUIGenerator();
		generator.doGenerate(smlFile);
		MessageDialog.openInformation(shell, "Generator", "Successfully generated Java SBP UI Implementation.");
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

}
