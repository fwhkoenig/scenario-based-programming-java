package sbp.generator

import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreEList
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.scenariotools.sml.runtime.configuration.Configuration

class RunconfigurationGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()

	def generate(Configuration configuration,
		boolean generateUI) '''
		package «configuration.packageName»;
		
		«val specification = configuration.specification»
		import «specification.FQN»;
		import «GeneratorStatics.FRAMEWORK_SPECIFICATIONRUNCONFIG»;
		import «GeneratorStatics.FRAMEWORK_SETTINGS»;
		«IF generateUI»
			import «configuration.UIFQN»;
		«ENDIF»
		
		public class «configuration.className»«IF generateUI»UI«ENDIF» extends SpecificationRunconfig<«specification.className»> {
		
			public «configuration.className»«IF generateUI»UI«ENDIF»() {
				super(new «specification.className»());
				«IF generateUI»
					«specification.name»EnvironmentFrame logFrame = new «specification.name»EnvironmentFrame("«specification.className»",getAdapter());
					logFrame.setVisible(true);
					Settings.setRuntimeOut(logFrame.getRuntimeLog());
					Settings.setServerOut(logFrame.getServerLog());
				«ENDIF»
				Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
			}
		
			@Override
			protected void registerParticipatingObjects() {
				«specification.name»ObjectSystem objectSystem = «specification.name»ObjectSystem.getInstance();
				«FOR rootObject : configuration.instanceModelRootObjects»
					«val rootObjectNameAttribute = rootObject.eClass.getEStructuralFeature("name")»
					«val rootObjectName = rootObject.eGet(rootObjectNameAttribute).toString»
					«IF specification.uncontrollableEClasses.contains(rootObject.eClass)»
						registerObject(objectSystem.«rootObjectName», UNCONTROLLABLE); // «rootObjectName»
					«ELSE»
						registerObject(objectSystem.«rootObjectName», CONTROLLABLE); // «rootObjectName»
					«ENDIF»
					«FOR object : rootObject.eAllContents.toList»
						«IF specification.controllableEClasses.contains(object.eClass)»
							«val path = object.pathToRoot»
							registerObject(objectSystem.«path», CONTROLLABLE); // «object.name»
						«ELSEIF specification.uncontrollableEClasses.contains(object.eClass)»
							«val path = object.pathToRoot»
							registerObject(objectSystem.«path», UNCONTROLLABLE); // «object.name»
						«ELSE»
							«val path = object.pathToRoot»
							registerObject(objectSystem.«path», CONTROLLABLE); // «object.name» // TODO: Check subclasses
						«ENDIF»
					«ENDFOR»
				«ENDFOR»
			}
		
			@Override
			protected void registerNetworkAdressesForObjects() {
				«specification.name»ObjectSystem objectSystem = «specification.name»ObjectSystem.getInstance();
				«FOR rootObject : configuration.instanceModelRootObjects»
					«FOR object : rootObject.eContents»
						«val nameAttribute = object.eClass.getEStructuralFeature("name")»
						«val objectName = object.eGet(nameAttribute).toString»
						// registerAddress(objectSystem.«object.pathToRoot», «objectName.toUpperCase»_HOSTNAME, «objectName.toUpperCase»_PORT);
					«ENDFOR»
				«ENDFOR»
			}
			
			@Override
			protected void registerObservers() {
				// Add Observers here.
				// addScenarioObserver(scenarioObserver);
			}
			
			public static void main(String[] args) {
				SpecificationRunconfig.run(«configuration.className»«IF generateUI»UI«ENDIF».class);
			}
		
		}
	'''

	def getPathToRoot(EObject object) {
		return recursiveGetPathToRoot(object, "")
	}

	def String recursiveGetPathToRoot(EObject object, String prePath) {
		var path = prePath
		val container = object.eContainer
		val objectContainingFeature = object.eContainingFeature
		path = ".get" + objectContainingFeature.name.toFirstUpper + "()"

		if (objectContainingFeature.upperBound != 1) {
			val featureList = container.eGet(objectContainingFeature) as EcoreEList<EObject>
			val index = featureList.indexOf(object)
			path = path + ".get(" + index.toString + ")"
		}

		if (container.eContainer != null) {
			path = recursiveGetPathToRoot(container, path) + path;
		} else {
			path = container.name + path
		}

		return path
	}

	def String getObjectSystemObjectFQN(EObject object) {
		val node = NodeModelUtils.getNode(object)
		return node.text
	}

	def getName(EObject object) {
		for (EAttribute eAttribute : object.eClass.EAllAttributes) {
			if (eAttribute.getName().equals("name")) {
				return object.eGet(eAttribute);
			}
		}
		return object.eClass.name.toFirstLower
	}

}
