package sbp.generator

import org.eclipse.emf.common.util.EList
import org.scenariotools.sml.Alternative
import org.scenariotools.sml.CaseCondition
import org.scenariotools.sml.Condition
import org.scenariotools.sml.ExecutionKind
import org.scenariotools.sml.ExpressionParameter
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.InterruptCondition
import org.scenariotools.sml.Loop
import org.scenariotools.sml.LoopCondition
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Parallel
import org.scenariotools.sml.ParameterBinding
import org.scenariotools.sml.ParameterExpression
import org.scenariotools.sml.RandomParameter
import org.scenariotools.sml.Role
import org.scenariotools.sml.VariableFragment
import org.scenariotools.sml.ViolationCondition
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
import org.scenariotools.sml.expressions.scenarioExpressions.Expression
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue
import org.scenariotools.sml.expressions.scenarioExpressions.NullValue
import org.scenariotools.sml.expressions.scenarioExpressions.StringValue
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.Variable
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue
import org.scenariotools.sml.utility.InteractionUtil

class InteractionFragmentGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()

	def String generate(InteractionFragment fragment) '''
	«IF fragment instanceof ModalMessage»
		«(fragment as ModalMessage).generateModalMessage»
	«ELSEIF fragment instanceof Alternative»
		«(fragment as Alternative).generateAlternative»
	«ELSEIF fragment instanceof Parallel»
		«(fragment as Parallel).generateParallel»
	«ELSEIF fragment instanceof Loop»
		«(fragment as Loop).generateLoop»
	«ELSEIF fragment instanceof VariableFragment»
		«(fragment as VariableFragment).generateVariableFragment»
	«ELSEIF fragment instanceof Condition»
		«(fragment as Condition).generateCondition»
	«ELSE»
		TODO: type «fragment.eClass.name» is not supported
	«ENDIF»'''

	def generateParallel(Parallel parallel) '''
	// Begin Parallel
	«FOR interaction : parallel.parallelInteraction»
		«val id = parallel.parallelInteraction.indexOf(interaction)»
		ParallelCase case_«parallel.hashCode»_«id» = new ParallelCase(this) {
			@Override
			protected void body() throws Violation {
				«interaction.generateInteraction»
			}
		};
	«ENDFOR»
	«FOR interaction : parallel.parallelInteraction BEFORE "runParallel(" SEPARATOR ", " AFTER ");
"»
		«val id = parallel.parallelInteraction.indexOf(interaction)»
		case_«parallel.hashCode»_«id»
	«ENDFOR»
	// End Parallel'''

	def generateModalMessage(ModalMessage message) '''
	«IF message.executionKind == ExecutionKind.NONE»
		waitFor(«IF message.isStrict»STRICT, «ENDIF»«message.sender.name», «message.receiver.name», "«message.modelElement.modifiedName»"«message.parameters.generateParameters»);
	«ELSE»
		request(«IF message.isStrict»STRICT, «ENDIF»«message.sender.name», «message.receiver.name», "«message.modelElement.modifiedName»"«message.parameters.generateParameters»);
	«ENDIF»'''

	def generateAlternative(Alternative alternative) '''
	«alternative.generateAlternativeHead»
	«alternative.generateAlternativeBody»
	'''
	
	def String generateAlternativeHead(Alternative alternative)'''
	// Begin Alternative
	List<Message> requestedMessages = new ArrayList<Message>();
	List<Message> waitedForMessages = new ArrayList<Message>();
	«FOR alternativeCase : alternative.cases»
		«IF alternativeCase.caseCondition == null»
			if (true) {
		«ELSE»
			if («alternativeCase.caseCondition.generate») {
		«ENDIF»
		«alternativeCase.caseInteraction.generateUntilFirstMessageAlternative»
		}
	«ENDFOR»
	// Insert into BP
	doStep(requestedMessages, waitedForMessages);'''
	
	def String generateAlternativeBody(Alternative alternative)'''
	// Determine which path has been chosen
	«FOR alternativeCase : alternative.cases SEPARATOR "else"»
		«val m = InteractionUtil.getInitializingMessages(alternativeCase.caseInteraction).get(0)»
		if (getLastMessage().equals(new Message(«m.sender.name», «m.receiver.name», "«m.modelElement.modifiedName»"«m.parameters.generateParameters»))) {
			«alternativeCase.caseInteraction.generateAfterFirstMessage»
		}
	«ENDFOR»
	// End Alternative'''
	
	

	def generateParameters(EList<ParameterBinding> list) '''
	«FOR parameter : list BEFORE ", " SEPARATOR ", "»
		«val bindingExpression = parameter.bindingExpression as ParameterExpression»
		«IF bindingExpression instanceof RandomParameter»
			RANDOM
		«ELSEIF bindingExpression instanceof ExpressionParameter»
			«val exp = bindingExpression as ExpressionParameter»
			«val binding = exp.value»
			«binding.generateExpression»
		«ELSE»
			Error: Expression «bindingExpression» not supported!
		«ENDIF»
	«ENDFOR»'''

	def generateLoop(Loop loop) '''
	// Begin Loop
	«IF loop.loopCondition == null»
		while (true) {
	«ELSE»
		while («loop.loopCondition.generateCondition») {
	«ENDIF»
		«val interaction = loop.bodyInteraction»
		«FOR fragment : interaction.fragments»
		«fragment.generate»
		«ENDFOR»
	}
	// End Loop'''

	def String generateExpression(Expression expression) '''
	«IF expression instanceof BinaryOperationExpression»
		«val exp = expression as BinaryOperationExpression»
		«val left = exp.left»
		«val right = exp.right»
		«val operator = exp.operator»
		(«left.generateExpression» «operator» «right.generateExpression»)
	«ELSEIF expression instanceof UnaryOperationExpression»
		«val exp = expression as UnaryOperationExpression»
		«val operand = exp.operand»
		«val operator = exp.operator»
		«operator» «operand.generateExpression»
	«ELSEIF expression instanceof IntegerValue»
		«val exp = expression as IntegerValue»
		«exp.value»
	«ELSEIF expression instanceof BooleanValue»
		«val exp = expression as BooleanValue»
		«exp.value»
	«ELSEIF expression instanceof StringValue»
		«val exp = expression as StringValue»
		"«exp.value»"
	«ELSEIF expression instanceof VariableValue»
		«val exp = expression as VariableValue»
		«val variable = exp.value»
		«IF variable instanceof Role»
			«variable.name».getBinding()
		«ELSE»
			«variable.name»
		«ENDIF»
	«ELSEIF expression instanceof NullValue»
		null
	«ELSEIF expression instanceof StructuralFeatureValue»
		«val exp = expression as StructuralFeatureValue»
		«val feature = exp.value»
		get«feature.name.toFirstUpper»()
	«ELSEIF expression instanceof FeatureAccess»
		«val exp = expression as FeatureAccess»
		«val target = exp.target as Variable»
		«val value = exp.value»
		«target.generateVariable».getBinding().«value.generateExpression»
		«IF exp.collectionAccess != null»
			«val ca = exp.collectionAccess»
			«val operation = ca.collectionOperation»
			«val param = ca.parameter»
			.«operation.generateCollectionOperation(param)»
		«ENDIF»
	«ELSE»
		TODO: type «expression.eClass.name» is not supported
	«ENDIF»'''

	def generateCollectionOperation(CollectionOperation operation, Expression param) {
		if (operation.equals(CollectionOperation.FIRST)) {
			return "get(0)"
		} else if (operation.equals(CollectionOperation.ANY)) {
			return "get(0)"
		} else {
			var out = operation + "("
			if (param != null) {
				out = out + param.generateExpression
			}
			return out + ")"
		}
	}

	def generateVariable(Variable variable) '''
		«IF variable instanceof Role»
			«variable.name»
		«ELSE»
			«variable.name»
		«ENDIF»
	'''

	def generateVariableFragment(VariableFragment fragment) '''
		«val expression = fragment.expression»
		«expression»
	'''

	def generateCondition(Condition condition) '''
	«IF condition instanceof CaseCondition»
		«(condition as CaseCondition).conditionExpression.expression.generateExpression»
	«ELSEIF condition instanceof LoopCondition»
		«(condition as LoopCondition).conditionExpression.expression.generateExpression»
	«ELSEIF condition instanceof InterruptCondition»
		if («(condition as InterruptCondition).conditionExpression.expression.generateExpression») {
			throwViolation(INTERRUPT);
		}
	«ELSEIF condition instanceof ViolationCondition»
		if («(condition as ViolationCondition).conditionExpression.expression.generateExpression») {
			throwViolation(SAFETY);
		}
	«ELSE»
		TODO: type «condition.eClass.name» is not supported
	«ENDIF»'''

	def generateUntilFirstMessage(Interaction interaction) '''
		«var Boolean firstMessageOccurred = false»
		«FOR fragment : interaction.fragments»
			«IF ! firstMessageOccurred»
				«fragment.generate»
				«IF (fragment instanceof ModalMessage)»
					//«firstMessageOccurred = true»
				«ENDIF»
			«ENDIF»
		«ENDFOR»
	'''

	def generateUntilFirstMessageAlternative(Interaction interaction) '''
		«var Boolean firstMessageOccurred = false»
		«FOR fragment : interaction.fragments»
			«IF ! firstMessageOccurred»
				«IF fragment instanceof ModalMessage»
					«IF fragment.executionKind == ExecutionKind.NONE»
						waitedForMessages.add(new Message(«IF fragment.isStrict»STRICT, «ENDIF»«fragment.sender.name», «fragment.receiver.name», "«fragment.modelElement.modifiedName»"«fragment.parameters.generateParameters»));
					«ELSE»
						requestedMessages.add(new Message(«IF fragment.isStrict»STRICT, «ENDIF»«fragment.sender.name», «fragment.receiver.name», "«fragment.modelElement.modifiedName»"«fragment.parameters.generateParameters»));
					«ENDIF»
				«ELSE»
					«fragment.generate»
				«ENDIF»
				«IF (fragment instanceof ModalMessage)»
					//«firstMessageOccurred = true»
				«ENDIF»
			«ENDIF»
		«ENDFOR»
	'''

	def generateAfterFirstMessage(Interaction interaction) '''
		«var firstMessageOccurred = false»
		«FOR fragment : interaction.fragments»
			«IF firstMessageOccurred»
				«fragment.generate»
			«ELSEIF fragment instanceof ModalMessage»
				//«firstMessageOccurred = true»
			«ELSEIF fragment instanceof Alternative»
				//«firstMessageOccurred = true»
				«val alternative = fragment as Alternative»
				«alternative.generateAlternativeBody»
			«ENDIF»
		«ENDFOR»
	'''

	def generateInteraction(Interaction interaction) '''
		«FOR fragment : interaction.fragments»
			«fragment.generate»
		«ENDFOR»
	'''

}
