package sbp.generator

import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.scenariotools.sml.runtime.configuration.Configuration
import sbp.generator.utils.WorkbenchUtils

class SBPGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()
	@Extension SpecificationGenerator specificationGenerator = new SpecificationGenerator()
	@Extension CollaborationGenerator collaborationGenerator = new CollaborationGenerator()
	@Extension ScenarioGenerator scenarioGenerator = new ScenarioGenerator()
	@Extension RunconfigurationGenerator runconfigurationGenerator = new RunconfigurationGenerator()
	@Extension ObjectSystemGenerator objectSystemGenerator = new ObjectSystemGenerator()

	def doGenerate(IFile smlFile) {
		val runconfiguration = WorkbenchUtils.getResource(smlFile).getContents().get(0) as Configuration
		val specification = runconfiguration.specification
		val IProject project = smlFile.project
		val fqn = specification.FQN
		val f = '''src/«fqn.toString.replace(".","/")».java'''
		val specificationFile = project.getFile(f)
		WorkbenchUtils.writeFile(specificationFile, specification.generate)
		for (collaboration : specification.collaborations) {
			val collaborationFile = project.getFile('''src/«collaboration.FQN.toString.replace(".","/")».java''')
			WorkbenchUtils.writeFile(collaborationFile, collaboration.generate)
			for (scenario : collaboration.scenarios) {
				val scenarioFile = project.getFile('''src/«scenario.FQN.toString.replace(".","/")».java''')
				WorkbenchUtils.writeFile(scenarioFile, scenario.generate())
			}
		}
		val objectSystemFile = project.getFile('''src/«runconfiguration.objectSystemFQN.toString.replace(".","/")».java''')
		WorkbenchUtils.writeFile(objectSystemFile, runconfiguration.generateObjectSystem)
		val runconfigFile = project.getFile('''src/«runconfiguration.FQN.toString.replace(".","/")».java''')
		WorkbenchUtils.writeFile(runconfigFile, runconfiguration.generate(false))
	}

}
