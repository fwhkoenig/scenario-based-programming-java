package sbp.generator

import java.util.ArrayList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.scenariotools.sml.runtime.configuration.Configuration

class ObjectSystemGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()

	def generateObjectSystem(Configuration configuration) '''
		package «configuration.packageName»;
		
		import java.util.Map;
		
		import org.eclipse.emf.common.util.URI;
		import org.eclipse.emf.ecore.resource.Resource;
		import org.eclipse.emf.ecore.resource.ResourceSet;
		import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
		import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
		
		«FOR EObject root : configuration.instanceModelRootObjects»
			import «root.eClass.EPackage.name».«root.eClass.EPackage.name.toFirstUpper»Package;
			import «root.eClass.fullyQualifiedName»;
		«ENDFOR»
		
		public class «configuration.objectSystemName» {
			
			private static «configuration.objectSystemName» instance;
			
			public static «configuration.objectSystemName» getInstance() {
				if (instance == null)
					instance = new «configuration.objectSystemName»();
				return instance;
			}
			
			«FOR EObject root : configuration.instanceModelRootObjects»
				public «root.eClass.name» «root.name» = («root.eClass.name») load«root.eClass.name»("«root.eResource.URI.uriToString»");
				
				public «root.eClass.name» load«root.eClass.name»(String uri) {
					«root.eClass.EPackage.name.toFirstUpper»Package.eINSTANCE.eClass();
				
						Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
						Map<String, Object> m = reg.getExtensionToFactoryMap();
						m.put("xmi", new XMIResourceFactoryImpl());
				
						ResourceSet resSet = new ResourceSetImpl();
				
						Resource resource = resSet.getResource(URI.createURI(uri), true);
						«root.eClass.name» «root.eClass.name.toFirstLower» = («root.eClass.name») resource.getContents().get(0);
						return «root.eClass.name.toFirstLower»;
				}
			«ENDFOR»
			
		}
	'''

	def String uriToString(URI uri) {
		val string = uri.toPlatformString(false)
		var split = string.split('/')
		val list = new ArrayList<String>()
		for (var i = 2; i < split.length; i++) {
			list.add(split.get(i))
		}
		return list.join('/')
	}

	def getName(EObject object) {
		for (EAttribute eAttribute : object.eClass.EAllAttributes) {
			if (eAttribute.getName().equals("name")) {
				return object.eGet(eAttribute);
			}
		}
		return object.eClass.name.toFirstLower
	}

}
