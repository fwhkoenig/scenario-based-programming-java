package sbp.generator

import java.util.ArrayList
import java.util.List
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClassifier
import org.scenariotools.sml.Alternative
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.FeatureAccessBindingExpression
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Parallel
import org.scenariotools.sml.RoleBindingConstraint
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.utility.InteractionFragmentUtil
import org.scenariotools.sml.utility.InteractionUtil

class ScenarioGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()
	@Extension InteractionFragmentGenerator interactionFragmentGenerator = new InteractionFragmentGenerator()

	def generate(
		Scenario scenario) '''
		package «scenario.packageName»;
		
		import «GeneratorStatics.FRAMEWORK_VIOLATION»;
		import «GeneratorStatics.FRAMEWORK_MESSAGE»;
		«IF !scenario.eAllContents.filter(typeof(Parallel)).empty»
			import «GeneratorStatics.FRAMEWORK_PARALLEL_CASE»;
		«ENDIF»
		
		«IF !scenario.eAllContents.filter(typeof(Alternative)).filter[a| !InteractionFragmentUtil.isInitializingFragmentOfScenario(a)] .empty»
			import java.util.List;
			import java.util.ArrayList;
		«ENDIF»
		
		// Collaboration
		«val collaboration = scenario.eContainer as Collaboration»
		import «collaboration.FQN»;
		
		// Roles
		«FOR type : scenario.roleBindings.retrieveTypes»
			import «type.fullyQualifiedName»;
		«ENDFOR»
		
		@SuppressWarnings("serial")
		public class «scenario.className» extends «collaboration.className» {
		
			@Override
			protected void registerAlphabet() {
				«FOR modalMessage : scenario.ownedInteraction.eAllContents.filter(typeof(ModalMessage)).toList.filterOutDuplicates»
					setBlocked(«modalMessage.sender.name», «modalMessage.receiver.name», "«modalMessage.modelElement.modifiedName»"«modalMessage.parameters.generateParameters»);
				«ENDFOR»
				«IF scenario.ownedInteraction.constraints != null»
					// Constraints
					«FOR consideredMessage : scenario.ownedInteraction.constraints.consider»
						setBlocked(«consideredMessage.sender.name», «consideredMessage.receiver.name», "«consideredMessage.modelElement.modifiedName»"«consideredMessage.parameters.generateParameters»);
					«ENDFOR»
					«FOR forbiddenMessage : scenario.ownedInteraction.constraints.forbidden»
						setForbidden(«forbiddenMessage.sender.name», «forbiddenMessage.receiver.name», "«forbiddenMessage.modelElement.modifiedName»"«forbiddenMessage.parameters.generateParameters»);
					«ENDFOR»
					«FOR interruptingMessage : scenario.ownedInteraction.constraints.interrupt»
						setInterrupting(«interruptingMessage.sender.name», «interruptingMessage.receiver.name», "«interruptingMessage.modelElement.modifiedName»"«interruptingMessage.parameters.generateParameters»);
					«ENDFOR»
				«ENDIF»
			}
			
			@Override
			protected void initialisation() {
				«scenario.ownedInteraction.generateInitializingMessages»
			}
			
			@Override
			protected void registerRoleBindings() {
				«FOR binding : scenario.roleBindings»
					«val expression = binding.bindingExpression as FeatureAccessBindingExpression»
					«val fa = expression.featureaccess»
					bindRoleToObject(«binding.role.name», («binding.role.type.name») «fa.generateExpression»);
				«ENDFOR»
			}
		
			@Override
			protected void body() throws Violation {
				«scenario.ownedInteraction.generateAfterFirstMessage»
			}
		
			
		
		}
	'''

	def generateInitializingMessages(Interaction interaction) '''
	«FOR event : InteractionUtil.getInitializingMessages(interaction)»
		addInitializingMessage(new Message(«event.sender.name», «event.receiver.name», "«event.modelElement.modifiedName»"«event.parameters.generateParameters»));
		«ENDFOR»'''

	def List<EClassifier> getRetrieveTypes(EList<RoleBindingConstraint> roleBindings) {
		val list = new ArrayList<EClassifier>()
		roleBindings.forEach [ r |
			val type = r.role.type
			if (!list.contains(type)) {
				list.add(type)
			}
		]
		return list
	}

	def static List<ModalMessage> filterOutDuplicates(List<ModalMessage> messages) {
		val out = new ArrayList<ModalMessage>()
		messages.forEach [ m |
			if (out.isEmpty) {
				out.add(m)
			} else {
				var found = false
				for (var i = 0; i < out.size; i++) {
					val mOut = out.get(i)
					if (m.sender == mOut.sender && m.receiver == mOut.receiver && m.modelElement == mOut.modelElement &&
						m.parameters.equals(mOut.parameters)) {
						found = true
					}
				}
				if (!found) {
					out.add(m)
				}
			}
		]
		return out
	}

}
