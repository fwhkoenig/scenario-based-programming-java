package sbp.generator

import org.scenariotools.sml.Collaboration

class CollaborationGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()

	def generate(Collaboration collaboration) '''
	package «collaboration.packageName»;
	
	// Framework
	import «GeneratorStatics.FRAMEWORK_ROLE»;
	import «GeneratorStatics.FRAMEWORK_SCENARIO»;
	
	// Roles
	«FOR role : collaboration.roles»
		import «role.FQN»;
	«ENDFOR»
	
	@SuppressWarnings("serial")
	public abstract class «collaboration.className» extends Scenario {
	
		// Roles
		«FOR role : collaboration.roles»
			protected Role<«role.type.name»> «role.name» = createRole(«role.type.name».class, "«role.name»");
		«ENDFOR»
		
	}'''

}
