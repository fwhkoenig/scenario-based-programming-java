package sbp.generator

class GeneratorStatics {

	public static final String FRAMEWORK_SPECIFICATION = "sbp.specification.Specification"
	public static final String FRAMEWORK_ROLE = "sbp.specification.role.Role"
	public static final String FRAMEWORK_SCENARIO = "sbp.specification.scenarios.Scenario"
	public static final String FRAMEWORK_VIOLATION = "sbp.specification.scenarios.violations.Violation"
	public static final String FRAMEWORK_SPECIFICATIONRUNCONFIG = "sbp.runtime.SpecificationRunconfig"
	public static final String FRAMEWORK_SETTINGS = "sbp.runtime.settings.Settings"
	public static final String FRAMEWORK_MESSAGE = "sbp.specification.events.Message"
	public static final String FRAMEWORK_PARALLEL_CASE = "sbp.specification.scenarios.ParallelCase"
	public static final String FRAMEWORK_CONSOLEFRAME = "sbp.ui.ConsoleFrame"
	public static final String FRAMEWORK_MESSAGE_BUTTON = "sbp.ui.MessageButton"
	public static final String FRAMEWORK_RUNTIME_ADAPTER = "sbp.runtime.adapter.RuntimeAdapter"
	public static final String FRAMEWORK_VIOLATION_KIND = "sbp.specification.scenarios.utils.ViolationKind"

}
