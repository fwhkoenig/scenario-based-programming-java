package sbp.generator.ui

import java.util.List
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreEList
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.runtime.configuration.Configuration
import sbp.generator.GeneratorStatics
import sbp.generator.SMLExtensionGenerator
import sbp.generator.ScenarioGenerator

class FrameGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()

	def generateFrame(
		Configuration configuration) '''
		package «configuration.UIPackageName»;
				
		«val specification = configuration.specification»
		import «GeneratorStatics.FRAMEWORK_CONSOLEFRAME»;
		import «GeneratorStatics.FRAMEWORK_MESSAGE_BUTTON»;
		import «GeneratorStatics.FRAMEWORK_RUNTIME_ADAPTER»;
		
		import javax.swing.BoxLayout;
		import javax.swing.JPanel;
		import javax.swing.JScrollPane;
		import javax.swing.ScrollPaneConstants;
		
		import «configuration.objectSystemFQN»;
		
		«FOR type : ScenarioGenerator.filterOutDuplicates(specification.eAllContents.filter(typeof(ModalMessage)).toList).roleTypes»
			import «type.fullyQualifiedName»;
		«ENDFOR»
		
		@SuppressWarnings("serial")
		public class «specification.name»EnvironmentFrame extends ConsoleFrame {
		
			private RuntimeAdapter runtimeAdapter;
		
			private void createButtons() {
				«specification.name»ObjectSystem objectSystem = «specification.name»ObjectSystem.getInstance();
				JPanel buttonPane = new JPanel();
				JScrollPane scrollPane = new JScrollPane(buttonPane, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
						ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));
				
				«FOR message : ScenarioGenerator.filterOutDuplicates(specification.eAllContents.filter(typeof(ModalMessage)).toList)»
					«val sender = message.sender»
					«val receiver = message.receiver»
					«val me = message.modelElement»
					«val senderType = sender.type»
					«val receiverType = receiver.type»
					«IF specification.uncontrollableEClasses.contains(senderType)»
						«FOR possibleSender : configuration.instanceModelRootObjects.getObjectsOfType(senderType)»
							«FOR possibleReceiver : configuration.instanceModelRootObjects.getObjectsOfType(receiverType)»
								«val senderPath = "objectSystem." + possibleSender.pathToRoot»
								«val receiverPath = "objectSystem." + possibleReceiver.pathToRoot»
								buttonPane.add(new MessageButton<«senderType.name», «receiverType.name»>(runtimeAdapter, «senderPath», «receiverPath», "«me.name»"));
							«ENDFOR»
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
				
				add(scrollPane);
			}
		
			public «specification.name»EnvironmentFrame(String name, RuntimeAdapter adapter) {
				super(name);
				this.runtimeAdapter = adapter;
				createButtons();
				pack();
			}
		
		}
	'''

	def EList<EClassifier> getRoleTypes(List<ModalMessage> messages) {
		val list = new BasicEList<EClassifier>()
		for (message : messages) {
			val senderType = message.sender.type
			val receiverType = message.receiver.type
			if (!list.contains(senderType)) {
				list.add(senderType)
			}
			if (!list.contains(receiverType)) {
				list.add(receiverType)
			}
		}
		return list
	}

	def getPathToRoot(EObject object) {
		return recursiveGetPathToRoot(object, "")
	}

	def String recursiveGetPathToRoot(EObject object, String prePath) {
		var path = prePath
		val container = object.eContainer
		val objectContainingFeature = object.eContainingFeature
		path = ".get" + objectContainingFeature.name.toFirstUpper + "()"

		if (objectContainingFeature.upperBound != 1) {
			val featureList = container.eGet(objectContainingFeature) as EcoreEList<EObject>
			val index = featureList.indexOf(object)
			path = path + ".get(" + index + ")"
		}

		if (container.eContainer != null) {
			path = recursiveGetPathToRoot(container, path) + path;
		} else {
			path = container.name + path
		}

		return path
	}

	def EList<EObject> getObjectsOfType(EList<EObject> list, EClassifier classifier) {
		val out = new BasicEList<EObject>()
		list.forEach [ rootObject |
			if (rootObject.eClass == classifier && !out.contains(rootObject)) {
				out.add(rootObject)
			}
			rootObject.eAllContents.forEach [ o |
				if (o.eClass == classifier && !out.contains(o)) {
					out.add(o)
				}
			]
		]
		return out
	}

	def getName(EObject object) {
		for (EAttribute eAttribute : object.eClass.EAllAttributes) {
			if (eAttribute.getName().equals("name")) {
				return object.eGet(eAttribute).toString;
			}
		}
		return object.eClass.name.toFirstLower
	}

}
