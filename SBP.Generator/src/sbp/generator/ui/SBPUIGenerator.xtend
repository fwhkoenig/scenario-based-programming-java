package sbp.generator.ui

import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.scenariotools.sml.runtime.configuration.Configuration
import sbp.generator.RunconfigurationGenerator
import sbp.generator.SMLExtensionGenerator
import sbp.generator.utils.WorkbenchUtils

class SBPUIGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()
	@Extension RunconfigurationGenerator runconfigurationGenerator = new RunconfigurationGenerator()
	@Extension FrameGenerator frameGenerator = new FrameGenerator()

	def doGenerate(IFile smlFile) {
		val runconfiguration = WorkbenchUtils.getResource(smlFile).getContents().get(0) as Configuration
		val IProject project = smlFile.project
		val runconfigFile = project.getFile('''src/«runconfiguration.FQN.toString.replace(".","/")»UI.java''')
		WorkbenchUtils.writeFile(runconfigFile, runconfiguration.generate(true))
		
		// TODO: CREATE FRAME FILE
		val UIFile = project.getFile('''src/«runconfiguration.UIFQN.toString.replace(".","/")».java''')
		WorkbenchUtils.writeFile(UIFile, runconfiguration.generateFrame())
	}

}
