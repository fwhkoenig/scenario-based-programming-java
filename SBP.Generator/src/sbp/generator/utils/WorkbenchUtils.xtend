package sbp.generator.utils

import java.io.ByteArrayInputStream
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.IWorkspace
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.jface.viewers.ISelection
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.ui.PlatformUI
import org.scenariotools.sml.Specification

class WorkbenchUtils {

	private def static ISelection retrieveWorkbenchSelection() {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection()
	}

	def static IFile createFile(IPath p) {
		val IWorkspace workspace = ResourcesPlugin.getWorkspace();
		val IWorkspaceRoot root = workspace.getRoot();
		val IFile file = root.getFile(p)
		return file
	}

	def static writeFile(IFile file, CharSequence content) {
		val parent = file.parent as IFolder
		if (!parent.exists()) {
			createFolder(parent)
		}
		if (!file.exists)
			file.create(new ByteArrayInputStream(content.toString.getBytes()), false, null)
	}

	def static createFolder(IFolder folder) {
		val parent = folder.parent
		if (parent instanceof IFolder) {
			if (!parent.exists()) {
				createFolder(parent)
			}
		}
		folder.create(IResource.NONE, true, null)
	}

	def static Resource getResource(IFile f) {
		val resourceSet = new ResourceSetImpl();
		return resourceSet.getResource(fromFile(f), true);
	}

	def static IFile getSelectedSMLFile() {
		val ISelection selection = retrieveWorkbenchSelection()
		var IFile smlFile = null

		if (selection instanceof IStructuredSelection) {
			for (Object object : selection.toArray()) {
				if (object instanceof IFile) {
					smlFile = object
				}
			}
		}

		return smlFile
	}

	def static Specification getSelectedSMLModel() {
		val IFile smlFile = getSelectedSMLFile()
		val Specification specification = getResource(smlFile).getContents().get(0) as Specification;
		return specification
	}

	private def static URI fromFile(IFile f) {
		return URI.createPlatformResourceURI(f.getFullPath().toString(), true);
	}

}
