package sbp.generator

import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.ETypedElement
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.Role
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.Specification
import org.scenariotools.sml.runtime.configuration.Configuration
import org.scenariotools.sml.utility.SpecificationUtil

class SMLExtensionGenerator {

	def getPackageName(Specification specification) '''
	«specification.name.toLowerCase».specification'''

	def getPackageName(Configuration configuration) '''
	«configuration.specification.name.toLowerCase».runtime'''

	def getUIPackageName(Configuration configuration) '''
	«configuration.specification.name.toLowerCase».ui'''

	def getPackageName(Collaboration collaboration) '''
	«val specification = SpecificationUtil.getContainingSpecification(collaboration)»
	«specification.name.toLowerCase».collaborations.«collaboration.name.toLowerCase»'''

	def getPackageName(Scenario scenario) '''
	«val collaboration = scenario.eContainer as Collaboration»
	«val specification = SpecificationUtil.getContainingSpecification(collaboration)»
	«specification.name.toLowerCase».collaborations.«collaboration.name.toLowerCase»'''

	def getFQN(Specification specification) '''
	«specification.packageName».«specification.className»'''

	def getFQN(Configuration configuration) '''
	«configuration.packageName».«configuration.className»'''

	def getUIFQN(Configuration configuration) '''
	«configuration.UIPackageName».«configuration.specification.name»EnvironmentFrame'''

	def getFQN(Collaboration collaboration) '''
	«collaboration.packageName».«collaboration.className»'''

	def getFQN(Scenario scenario) '''
	«scenario.packageName».«scenario.className»'''

	def getClassName(Specification specification) '''
	«specification.name»Specification'''

	def getClassName(Configuration configuration) '''
	«configuration.specification.name»Runconfig'''

	def getClassName(Collaboration collaboration) '''
	«collaboration.name»Collaboration'''

	def getClassName(Scenario scenario) '''
	«(scenario.eContainer as Collaboration).name»_«scenario.name»Scenario'''

	def getFQN(Role role) '''
	«role.type.fullyQualifiedName»'''

	def String fullyQualifiedName(EPackage ePackage) {
		if (ePackage.getESuperPackage() == null)
			return ePackage.getName()
		else
			return fullyQualifiedName(ePackage.getESuperPackage()) + "." + ePackage.getName()
	}

	def String fullyQualifiedName(EClassifier eClassifier) {
		return fullyQualifiedName(eClassifier.getEPackage()) + "." + eClassifier.getName();
	}

	def getObjectSystemFQN(Configuration configuration) '''
	«configuration.packageName».«configuration.objectSystemName»'''

	def getObjectSystemName(Configuration configuration) '''
	«configuration.specification.name»ObjectSystem'''

	def String getModifiedName(ETypedElement element) {
		if (element instanceof EStructuralFeature) {
			if (element.upperBound == 1) {
				return "set" + element.name.toFirstUpper
			} else {
				return element.name
			}
		} else if (element instanceof EOperation) {
			return element.name
		}
	}

}
