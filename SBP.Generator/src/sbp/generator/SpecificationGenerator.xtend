package sbp.generator

import org.scenariotools.sml.ScenarioKind
import org.scenariotools.sml.Specification

class SpecificationGenerator {

	@Extension SMLExtensionGenerator smlExtensionGenerator = new SMLExtensionGenerator()

	def generate(Specification specification) '''
	package «specification.packageName»;
	
	// Framework
	import «GeneratorStatics.FRAMEWORK_SPECIFICATION»;
	
	«FOR collaboration : specification.collaborations»
		// Collaboration «collaboration.name»
		«FOR scenario : collaboration.scenarios»
			import «scenario.FQN»;
		«ENDFOR»
	«ENDFOR»
	
	public class «specification.className» extends Specification {
	
		@Override
		protected void registerScenarios() {
			«FOR collaboration : specification.collaborations»
				// Collaboration «collaboration.name»
				«FOR scenario : collaboration.scenarios»
					«IF scenario.kind == ScenarioKind.SPECIFICATION»
						registerSpecificationScenario(«scenario.className».class);
					«ELSEIF scenario.kind == ScenarioKind.ASSUMPTION»
						registerAssumptionScenario(«scenario.className».class);
					«ENDIF»
				«ENDFOR»
			«ENDFOR»
		}
		
		@Override
		protected void registerTransformationRules() {
			// Add Model Transformation Rules here
			// registerRule(new MyTransformationRule());
		}
	
	}'''

}
