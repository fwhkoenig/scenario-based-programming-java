package examples.productioncell;

import examples.productioncell.runtime.ProductionCellRunconfig;
import sbp.runtime.SpecificationRunconfig;

public class ProductionCellMain {

	public static void main(String[] args) {
		// Logger.enableLogging();
		runProductionCellPlayout();
	}

	private static void runProductionCellPlayout() {
		SpecificationRunconfig.run(ProductionCellRunconfig.class);
	}

}