package examples.productioncell.runtime;

import examples.productioncell.specification.ProductionCellSpecification;
import sbp.runtime.SpecificationRunconfig;

public class ProductionCellRunconfig extends SpecificationRunconfig<ProductionCellSpecification> {

	public ProductionCellRunconfig() {
		super(new ProductionCellSpecification());
		ProductionCellEnvironmentFrame logFrame = new ProductionCellEnvironmentFrame("ProductionCell", getAdapter());
		logFrame.setVisible(true);
		// ConsoleFrame logFrame = new ConsoleFrame("ProductionCellLog");
		// logFrame.setVisible(true);
		ProductionCellSettings.setSettings(logFrame.getRuntimeLog(), logFrame.getServerLog());
	}

	@Override
	protected void registerParticipatingObjects() {
		ProductionCellObjectSystem objectSystem = ProductionCellObjectSystem.getInstance();
		registerObject(objectSystem.tableSensor);
		registerObject(objectSystem.controller);
		registerObject(objectSystem.armA);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {

	}

	public static void main(String[] args) {
		// Logger.enableLogging();
		SpecificationRunconfig.run(ProductionCellRunconfig.class);
	}

	@Override
	protected void registerObservers() {

	}

}
