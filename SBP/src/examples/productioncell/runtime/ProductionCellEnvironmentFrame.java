package examples.productioncell.runtime;

import examples.productioncell.model.Controller;
import examples.productioncell.model.TableSensor;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.ui.ConsoleFrame;
import sbp.ui.MessageButton;

@SuppressWarnings("serial")
public class ProductionCellEnvironmentFrame extends ConsoleFrame {

	private RuntimeAdapter runtimeAdapter;

	private Controller controller;
	private TableSensor tableSensor;

	private MessageButton<TableSensor, Controller> blankArrived;

	public ProductionCellEnvironmentFrame(String name, RuntimeAdapter adapter) {
		super(name);
		this.runtimeAdapter = adapter;
		controller = ProductionCellObjectSystem.getInstance().controller;
		tableSensor = controller.getTableSensor();
		createButtons();
		pack();
	}

	private void createButtons() {
		blankArrived = new MessageButton<TableSensor, Controller>(runtimeAdapter, tableSensor, controller,
				"blankArrived", true);
		add(blankArrived);
	}

}
