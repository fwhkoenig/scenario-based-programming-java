package examples.productioncell.model;

public class ArmA extends NamedElement {

	private int count;

	public void pickUp() {
		count++;
		System.out.println("ArmA: I picked up " + count + " blanks!");
	}

}
