package examples.productioncell.collaborations;

import examples.productioncell.model.ArmA;
import examples.productioncell.model.Controller;
import examples.productioncell.model.TableSensor;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

@SuppressWarnings("serial")
public abstract class ProductionCell extends Scenario {

	protected Role<TableSensor> ts = createRole(TableSensor.class, "ts");

	protected Role<ArmA> armA = new Role<ArmA>(ArmA.class, "armA");

	protected Role<Controller> c = new Role<Controller>(Controller.class, "c");

	protected Message blankArrived = new Message(ts, c, "blankArrived", true);
	protected Message pickUp = new Message(c, armA, "pickUp");
	protected Message pickedUpBlank = new Message(armA, c, "pickedUpBlank");
	protected Message moveToPress = new Message(c, armA, "moveToPress");
	protected Message arrivedAtPress = new Message(armA, c, "arrivedAtPress");

}
