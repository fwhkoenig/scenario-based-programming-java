package examples.productioncell.collaborations.productionCell;

import examples.productioncell.collaborations.ProductionCell;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class PickUpOnBlankArrived extends ProductionCell {

	@Override
	protected void registerAlphabet() {
		setBlocked(blankArrived);
		setBlocked(c, armA, "pickUp");
	}

	@Override
	protected void initialisation()  {
		addInitializingMessage(blankArrived);
	}

	@Override
	protected void body() throws Violation {
		request(true, c, armA, "pickUp");
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(armA, c.getBinding().getArmA());
	}

}
