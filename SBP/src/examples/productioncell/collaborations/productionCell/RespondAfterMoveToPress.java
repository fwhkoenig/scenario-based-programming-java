package examples.productioncell.collaborations.productionCell;

import examples.productioncell.collaborations.ProductionCell;
import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class RespondAfterMoveToPress extends ProductionCell {

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(c, armA, "moveToPress"));
	}

	@Override
	protected void registerRoleBindings() {

	}

	@Override
	protected void body() throws Violation {
		request(armA, c, "arrivedAtPress");
	}

	@Override
	protected void registerAlphabet() {
		setBlocked(c, armA, "moveToPress");
		setBlocked(armA, c, "arrivedAtPress");
	}

}
