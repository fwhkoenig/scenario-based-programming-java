package sbp.specification.role;

import java.util.ArrayList;
import java.util.List;

public class RoleToObjectMap {

	private List<List<Role>> roles = new ArrayList<List<Role>>();
	private List<Object> objects = new ArrayList<Object>();

	public List<List<Role>> getRoles() {
		return roles;
	}

	public List<Object> getObjects() {
		return objects;
	}

	public void addPair(Role role, Object object) {
		if (role.getType().getClass() != object.getClass()) {
			throw new IllegalArgumentException(
					"Role " + role.getName() + " with type " + role.getType().getClass().getName()
							+ " is not compatible with object of type " + object.getClass().getName() + ".");
		}
		List<Role> newRoles = getRolesForObject(object);
		newRoles.add(role);
		if (!objects.contains(object)) {
			objects.add(object);
			roles.add(newRoles);
		}
	}

	public List<Role> getRolesForObject(Object object) {
		List<Role> roles;
		if (objects.contains(object)) {
			int index = objects.indexOf(object);
			roles = this.roles.get(index);
		} else {
			roles = new ArrayList<Role>();
		}
		return roles;
	}

	public Object getObjectForRole(Role role) {
		for (List<Role> l : roles) {
			if (l.contains(role)) {
				int index = roles.indexOf(l);
				return objects.get(index);
			}
		}
		return null;
	}

}
