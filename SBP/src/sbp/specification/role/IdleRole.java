package sbp.specification.role;

public class IdleRole extends Role<IdleRole> {

	public IdleRole() {
		super(IdleRole.class, "IDLE");
		setBinding(this);
	}

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public void reset() {
		setBinding(this);
	}
	
	@Override
	public IdleRole getBinding() {
		return this;
	}
	
	@Override
	public void setBinding(IdleRole binding) {
		// Do nothing
	}
}
