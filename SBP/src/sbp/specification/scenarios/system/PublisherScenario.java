package sbp.specification.scenarios.system;

import sbp.runtime.ObjectRegistry;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class PublisherScenario extends SpectatorScenario {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1474165876242941529L;

	private RuntimeAdapter adapter;

	public PublisherScenario(RuntimeAdapter adapter) {
		this.adapter = adapter;
	}

	@Override
	protected void onMessageReceived() {
		Message last = getLastMessage();
		if (last.equals(Message.IDLE) || last.equals(Message.SYNCHRONIZE)) {
			return;
		}
		Role<?> sender = last.getSender();
		if (ObjectRegistry.getInstance().isControllable(sender.getBinding())) {
			adapter.publish(last);
		}
	}

}
