package sbp.specification.scenarios.system;

import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ExecutionMode;

@SuppressWarnings("serial")
public abstract class SystemScenario extends Scenario {

	protected static long time_superstep_start;
	protected static long time_superstep_end;
	
	public SystemScenario() {
		setExecutionMode(ExecutionMode.LOOP);
		setType(Scenario.SYSTEM);
	}

	@Override
	protected void initialisation() {
		// No initialization
	}

	@Override
	protected void registerRoleBindings() {
		// No role bindings
	}

	@Override
	protected void registerAlphabet() {
		// No blocked events
	}

}
