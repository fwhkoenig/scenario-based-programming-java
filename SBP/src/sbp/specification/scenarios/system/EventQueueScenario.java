package sbp.specification.scenarios.system;

import java.util.ArrayDeque;
import java.util.Queue;

import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class EventQueueScenario extends SystemScenario {

	private Queue<Message> eventQueue = new ArrayDeque<Message>();

	@Override
	protected void body() throws Violation {
		// Wait for Idle
		waitFor(Message.IDLE);
		// Wait for environment
		waitForEnvironmentMessage();
		raiseEnvironmentMessage();
		if (Settings.DEBUG__ENABLE_SUPERSTEP_PERFORMANCE_TEST) {
			time_superstep_start = System.currentTimeMillis();
			Settings.IDLE_OUT.println("DEBUG: Superstep started.");
		}
	}

	private void raiseEnvironmentMessage() throws Violation {
		Message event = popEvent();
		controlledRequest(event);
	}

	private void waitForEnvironmentMessage() {
		while (!hasMessageEventInQueue()) {
			sleep(0); // Sleep to refresh the queue
		}
	}

	public void enqueueEvent(Message event) {
		eventQueue.add(event);
	}

	public Message popEvent() {
		return eventQueue.poll();
	}

	public boolean hasMessageEventInQueue() {
		return !eventQueue.isEmpty();
	}

}
