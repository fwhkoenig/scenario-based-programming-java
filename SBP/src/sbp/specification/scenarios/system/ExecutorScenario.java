package sbp.specification.scenarios.system;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import sbp.runtime.ObjectRegistry;
import sbp.runtime.objectsystem.TransformationRule;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.violations.Violation;
import sbp.utils.ReflectionUtils;

@SuppressWarnings("serial")
public class ExecutorScenario extends SystemScenario {

	public static final boolean EXECUTE_UNCONTROLLABLE_OBJECTS = true;

	private static ExecutorScenario instance;
	private int messageCount;

	private List<TransformationRule> transformationRules;

	public static ExecutorScenario getInstance() {
		return instance;
	}

	public ExecutorScenario() {
		super();
		instance = this;
	}

	public List<TransformationRule> getTransformationRules() {
		if (transformationRules == null) {
			transformationRules = new ArrayList<TransformationRule>();
		}
		return transformationRules;
	}

	@Override
	protected void body() throws Violation {
		//Wait for events
		waitForAnything();
		// Event triggered
		Message lastEvent = getLastMessage();
		Role<?> receiver = lastEvent.getReceiver();
		String messageName = lastEvent.getMessage();
		List<Object> parameters = lastEvent.getParameters();
		Object receiverObject = receiver.getBinding();
		if (receiverObject != Role.IDLE) {
			if (Settings.EXECUTOR__EXECUTE_METHODS || (Settings.EXECUTOR__EXECUTE_SETTERS && isSetter(messageName))) {
				if (ObjectRegistry.getInstance().isControllable(receiverObject) || EXECUTE_UNCONTROLLABLE_OBJECTS) {
					tryInvokeMethod(receiver, messageName, parameters, receiverObject);
				}
			}
			if (Settings.EXECUTOR__EXECUTE_RULES) {
				for (TransformationRule transformationRule : getTransformationRules()) {
					if (transformationRule.match(lastEvent)) {
						transformationRule.execute(lastEvent);
						if (Settings.EXECUTOR__PRINT_ON_RULE_EXECUTION) {
							Settings.EXECUTOR_OUT.println(
									"|-> Executor: Running rule " + transformationRule.getClass().getSimpleName() + ".");
						}
					}
				}
			}
		}
		messageCount = bp.eventCounter;
	}

	private boolean isSetter(String messageName) {
		String n = messageName.charAt(3) + "";
		return messageName.startsWith("set") && n.equals(n.toUpperCase());
	}

	public synchronized boolean isFinished() {
		return bp.eventCounter == messageCount;
	}

	private void tryInvokeMethod(Role<?> receiver, String messageName, List<Object> parameters, Object receiverObject) {
		try {
			for (Method method : receiverObject.getClass().getMethods()) {
				if (!method.getName().equals(messageName)) {
					continue;
				}
				Class<?>[] parameterTypes = method.getParameterTypes();
				boolean matches = true;
				for (int i = 0; i < parameterTypes.length; i++) {
					if (!parameterTypes[i].isAssignableFrom(parameters.get(i).getClass())) {
						matches = false;
						break;
					}
				}
				if (matches) {
					if (Settings.EXECUTOR__PRINT_ON_METHOD_EXECUTION) {
						Settings.EXECUTOR_OUT.println("|-> Executor: Running method " + messageName + " on object "
								+ ReflectionUtils.findNameForObject(receiverObject) + " bound to role "
								+ receiver.getName() + ".");
					}
					method.invoke(receiverObject, parameters.toArray());
					break;
				}
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
