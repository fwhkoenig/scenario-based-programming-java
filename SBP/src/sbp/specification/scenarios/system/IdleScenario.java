package sbp.specification.scenarios.system;

import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.role.IdleRole;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.violations.Violation;

/**
 * The IdleScenario is a special {@link Scenario} for the specified system that
 * triggers a {@link Message} when there are no system messages enabled in any
 * other scenario.
 * 
 * @author Florian König
 * @see Scenario
 * @see IdleRole
 */
@SuppressWarnings("serial")
public class IdleScenario extends SystemScenario {

	@Override
	protected void body() throws Violation {
		// Request the IDLE message
		request(Message.IDLE);
		if (Settings.IDLE__PRINT_SUPERSTEP_IN_CONSOLE) {
			Settings.IDLE_OUT.println(
					"----------------------------------------------------------------------------------------------------");
		}
		if (Settings.DEBUG__ENABLE_SUPERSTEP_PERFORMANCE_TEST) {
			time_superstep_end = System.currentTimeMillis();
			Settings.IDLE_OUT
					.println("DEBUG: Superstep ended after " + (time_superstep_end - time_superstep_start) + " ms.");
		}
	}

}
