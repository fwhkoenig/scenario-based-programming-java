package sbp.specification.scenarios;

import static bp.eventSets.EventSetConstants.none;

import java.util.ArrayList;
import java.util.List;

import sbp.specification.events.Message;
import sbp.specification.scenarios.utils.ExecutionMode;
import sbp.specification.scenarios.violations.InterruptViolation;
import sbp.specification.scenarios.violations.SafetyViolation;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public abstract class ParallelCase extends Scenario {

	private Scenario host;

	public Scenario getHost() {
		return host;
	}

	@Override
	public String getName() {
		return getHost().getName() + "_P" + getHost().getParallelCaseScenarios().indexOf(this) + "#" + getPriority();
	}

	public ParallelCase(Scenario parent) {
		if (parent instanceof ParallelCase) {
			this.host = ((ParallelCase) parent).getHost();
		} else {
			this.host = parent;
		}
		setActive(true);
		setExecutionMode(ExecutionMode.CASE);
	}

	@Override
	protected void clearParallelCaseScenarios() {
		// Never clear case scenarios here!
	}

	@Override
	protected void initialisation() {
		// No initialization needed
	}

	@Override
	protected void registerRoleBindings() {
		// Role bindings are given by the host scenario
	}

	@Override
	protected void registerAlphabet() {
		// Blocked events are inherited by parent scenario
		getBlockedMessages().addAll(host.getBlockedMessages());
	}

	// @Override
	// protected void resetRoleBindings() {
	// // Do NOT reset role bindings!
	// }

	@Override
	protected void reset() {
		// Remove this ParallelCase scenario.
		getHost().getParallelCaseScenarios().remove(this);
	}

	@Override
	protected void raiseEvents() throws InterruptViolation, SafetyViolation, Violation {
		synchronizeWithHost();
		super.raiseEvents();
	}

	@Override
	protected void runParallel(ParallelCase... cases) throws Violation {
		// Add new parallel cases to all hosts
		for (ParallelCase parallelCase : cases) {
			Scenario host = this.host;
			host.getParallelCaseScenarios().add(parallelCase);
			while (host instanceof ParallelCase) {
				host = ((ParallelCase) host).getHost();
				host.getParallelCaseScenarios().add(parallelCase);
			}
		}
		super.runParallel(cases);
	}

	@Override
	protected List<Message> retrieveBlockedMessages(List<Message> requestedEvents,
			List<Message> waitedForEvents) {
		// Never block any events
		return new ArrayList<Message>();
	}

	private void synchronizeWithHost() throws Violation {
		bp.bSync(Message.SYNCHRONIZE, none, none);
	}

	@Override
	protected void runLifeCycle() throws Violation {
		super.runLifeCycle();
		synchronizeWithHost();
	}

}
