package sbp.specification.scenarios;

import static bp.eventSets.EventSetConstants.all;
import static bp.eventSets.EventSetConstants.none;

import java.util.ArrayList;
import java.util.List;

import bp.BThread;
import bp.Event;
import bp.eventSets.EventSet;
import bp.eventSets.EventSetInterface;
import bp.eventSets.RequestableEventSet;
import bp.eventSets.RequestableInterface;
import bp.exceptions.BPJRequestableSetException;
import sbp.runtime.ObjectRegistry;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.events.ParameterRandom;
import sbp.specification.role.Role;
import sbp.specification.scenarios.system.ExecutorScenario;
import sbp.specification.scenarios.utils.ExecutionMode;
import sbp.specification.scenarios.utils.ScenarioObserver;
import sbp.specification.scenarios.utils.ViolationKind;
import sbp.specification.scenarios.violations.InterruptViolation;
import sbp.specification.scenarios.violations.MessageViolatedCause;
import sbp.specification.scenarios.violations.SafetyViolation;
import sbp.specification.scenarios.violations.Violation;
import sbp.utils.ListUtils;

@SuppressWarnings("serial")
public abstract class Scenario extends BThread {

	public static final String ASSUMPTION = "ASSUMPTION";
	public static final String SPECIFICATION = "SPECIFICATION";
	public static final String SYSTEM = "SYSTEM";
	public static final String UNSET = "UNSET";
	public static final ViolationKind SAFETY = ViolationKind.SAFETY;
	public static final ViolationKind INTERRUPT = ViolationKind.INTERRUPT;
	public static final ParameterRandom RANDOM = ParameterRandom.getInstance();
	public static final boolean STRICT = true;

	private List<Message> initializingMessages;
	private List<Message> blockedMessages;
	private List<Message> interruptingMessages;
	private List<Message> forbiddenMessages;
	private boolean active;
	private boolean finished;
	private double priority;
	private String type = UNSET;

	private ExecutionMode executionMode = ExecutionMode.COPY_ON_INITIALIZATION;

	private List<Object> activeBindings;

	private List<ParallelCase> parallelCaseScenarios;

	/**
	 * The initializing part of the scenario. In this method there must be at
	 * least one initializing message!
	 */
	protected abstract void initialisation();

	/**
	 * This method registers role bindings for the scenario. It is called after
	 * the initialization of the scenario. Roles from initializing events are
	 * already bound by the event. Use bindRoleToObject(Role<T> role, T object)
	 * here to bind roles.
	 */
	protected abstract void registerRoleBindings();

	/**
	 * This method registers blocked events. Use addBlockedEvent({@link Message}
	 * messageEvent) here to register events.
	 */
	protected abstract void registerAlphabet();

	/**
	 * This is the main interaction of the scenario.
	 */
	protected abstract void body() throws Violation;

	/**
	 * By default this method does nothing. One can override this to reset
	 * certain values after termination of the scenario.
	 */
	protected void reset() {
		// Does nothing by default.
	};

	@Override
	public String getName() {
		return super.getName() + "#" + getPriority();
	}

	/**
	 * Sets the type of the scenario. Types are: SPECIFICATION, ASSUMPTION,
	 * SYSTEM
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Returns the type of the scenario.
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return getName();
	}

	protected void notifyObservers() {
		for (ScenarioObserver observer : getObservers()) {
			observer.trigger(this, getLastMessage());
		}
	}

	/**
	 * Returns the list of active {@link ParallelCase} scenarios.
	 * 
	 * @return
	 */
	public synchronized List<ParallelCase> getParallelCaseScenarios() {
		if (parallelCaseScenarios == null) {
			parallelCaseScenarios = new ArrayList<ParallelCase>();
		}
		return parallelCaseScenarios;
	}

	/**
	 * Creates a role with the given type and name.
	 * 
	 * @param type
	 * @param name
	 * @return
	 */
	protected <T> Role<T> createRole(Class<T> type, String name) {
		return new Role<T>(type, name) {
		};
	}

	public List<Message> getInitializingMessages() {
		if (initializingMessages == null) {
			initializingMessages = new ArrayList<Message>();
		}
		return initializingMessages;
	}

	/**
	 * Adds an initializing message to the scenario.
	 * 
	 * @param message
	 */
	protected void addInitializingMessage(Message message) {
		getInitializingMessages().add(message);
	}

	/**
	 * Returns whether or not the given message is initializing.
	 * 
	 * @param message
	 * @return
	 */
	public boolean isInitializedBy(Message message) {
		return getInitializingMessages().contains(message);
	}

	/**
	 * This method creates a {@link Message} with the given roles and message
	 * and adds it to the list of blocked messages.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @return
	 */
	protected Message setBlocked(Role<?> sender, Role<?> receiver, String message) {
		Message event = new Message(sender, receiver, message);
		getBlockedMessages().add(event);
		return event;
	}

	/**
	 * This method creates a {@link Message} with the given roles and message
	 * and adds it to the list of blocked messages.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 */
	protected Message setBlocked(Role<?> sender, Role<?> receiver, String message, Object... parameters) {
		Message event = new Message(sender, receiver, message, parameters);
		getBlockedMessages().add(event);
		return event;
	}

	/**
	 * This method adds a given {@link Message} to the list of blocked messages.
	 * 
	 * @param messageEvent
	 * @return messageEvent
	 */
	protected Message setBlocked(Message messageEvent) {
		getBlockedMessages().add(messageEvent);
		return messageEvent;
	}

	/**
	 * Returns the list of blocked messages specified by the scenario.
	 * 
	 * @return blockedEvents
	 */
	public List<Message> getBlockedMessages() {
		if (blockedMessages == null) {
			blockedMessages = new ArrayList<Message>();
		}
		return blockedMessages;
	}

	/**
	 * This method adds a given {@link Message} to the list of interrupting
	 * messages.
	 * 
	 * @param messageEvent
	 * @return messageEvent
	 */
	protected Message setInterrupting(Role<?> sender, Role<?> receiver, String message) {
		Message event = new Message(sender, receiver, message);
		getInterruptingMessages().add(event);
		return event;
	}

	/**
	 * This method adds a given {@link Message} to the list of interrupting
	 * messages.
	 * 
	 * @param messageEvent
	 * @return messageEvent
	 */
	protected Message setInterrupting(Role<?> sender, Role<?> receiver, String message, Object... parameters) {
		Message event = new Message(sender, receiver, message, parameters);
		getInterruptingMessages().add(event);
		return event;
	}

	/**
	 * This method adds a given {@link Message} to the list of interrupting
	 * messages.
	 * 
	 * @param messageEvent
	 * @return messageEvent
	 */
	protected Message setInterrupting(Message messageEvent) {
		getInterruptingMessages().add(messageEvent);
		return messageEvent;
	}

	/**
	 * Returns the list of interrupting messages specified by the scenario.
	 * 
	 * @return interruptingEvents
	 */
	public List<Message> getInterruptingMessages() {
		if (interruptingMessages == null) {
			interruptingMessages = new ArrayList<Message>();
		}
		return interruptingMessages;
	}

	/**
	 * This method adds a given {@link Message} to the list of forbidden
	 * messages.
	 * 
	 * @param messageEvent
	 * @return messageEvent
	 */
	protected Message setForbidden(Role<?> sender, Role<?> receiver, String message) {
		Message event = new Message(sender, receiver, message);
		getForbiddenMessages().add(event);
		return event;
	}

	/**
	 * This method adds a given {@link Message} to the list of forbidden
	 * messages.
	 * 
	 * @param messageEvent
	 * @return messageEvent
	 */
	protected Message setForbidden(Role<?> sender, Role<?> receiver, String message, Object... parameters) {
		Message event = new Message(sender, receiver, message, parameters);
		getForbiddenMessages().add(event);
		return event;
	}

	/**
	 * This method adds a given {@link Message} to the list of forbidden
	 * messages.
	 * 
	 * @param messageEvent
	 * @return messageEvent
	 */
	protected Message setForbidden(Message messageEvent) {
		getForbiddenMessages().add(messageEvent);
		return messageEvent;
	}

	/**
	 * Returns the list of forbidden messages specified by the scenario.
	 * 
	 * @return interruptingEvents
	 */
	public List<Message> getForbiddenMessages() {
		if (forbiddenMessages == null) {
			forbiddenMessages = new ArrayList<Message>();
		}
		return forbiddenMessages;
	}

	/**
	 * Returns whether or not the scenario is active. A scenario is active when
	 * it is initialized and not terminated or violated. A scenario that is
	 * terminated or violated sets its state to not active.
	 * 
	 * @return active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Returns whether or not the scenario is finished. A scenario is finished
	 * when it has completed its lifecycle successfully.
	 * 
	 * @return
	 */
	public boolean isFinished() {
		return finished;
	}

	/**
	 * Sets the process priority in the BP execution.
	 * 
	 * @param priority
	 */
	public void setPriority(double priority) {
		this.priority = priority;
	}

	/**
	 * Returns the process priority in the BP execution.
	 * 
	 * @return priority
	 */
	public double getPriority() {
		return priority;
	}

	/**
	 * This method returns the execution mode of the scenario. The
	 * {@link ExecutionMode} determines when the scenario is restarted. A
	 * SINGULAR scenario is restarted when it is terminated or violated. A
	 * COPY_ON_INITIALIZATION scenario creates and starts a copy of itself when
	 * it is initialized. A CASE scenario is never restarted. A LOOP scenario is
	 * never restarted and goes back to initialization after it is terminated.
	 * 
	 * @return executionMode
	 */
	protected ExecutionMode getExecutionMode() {
		return executionMode;
	}

	/**
	 * This method sets the execution mode of the scenario. The
	 * {@link ExecutionMode} determines when the scenario is restarted. A
	 * SINGULAR scenario is restarted when it is terminated or violated. A
	 * COPY_ON_INITIALIZATION scenario creates and starts a copy of itself when
	 * it is initialized. A CASE scenario is never restarted. A LOOP scenario is
	 * never restarted and goes back to initialization after it is terminated.
	 * 
	 * @param executionMode
	 */
	public void setExecutionMode(ExecutionMode executionMode) {
		this.executionMode = executionMode;
	}

	/**
	 * This method returns whether or not the scenario is a SINGULAR scenario.
	 * 
	 * @return getExecutionMode() == ExecutionMode.SINGULAR
	 */
	protected boolean isSingular() {
		return getExecutionMode() == ExecutionMode.SINGULAR;
	}

	/**
	 * This method makes the scenario a SINGULAR scenario.
	 */
	protected void setSingular() {
		setExecutionMode(ExecutionMode.SINGULAR);
	}

	/**
	 * This method returns whether or not the scenario creates a copy of itself
	 * after initialization. This is the default behavior for non singular
	 * scenarios.
	 * 
	 * @return getExecutionMode() == ExecutionMode.COPY_ON_INITIALIZATION
	 */
	protected boolean isCopyOnInitialization() {
		return getExecutionMode() == ExecutionMode.COPY_ON_INITIALIZATION;
	}

	/**
	 * This method makes the scenario create a copy of itself after
	 * initialization. This is the default behavior for non singular scenarios.
	 */
	protected void setCopyOnInitialization() {
		setExecutionMode(ExecutionMode.COPY_ON_INITIALIZATION);
	}

	private void violate(ViolationKind violationKind, Event expected, Event received)
			throws InterruptViolation, SafetyViolation, Violation {
		if (violationKind == ViolationKind.INTERRUPT) {
			throw new InterruptViolation(this, new MessageViolatedCause(expected, received));
		} else if (violationKind == ViolationKind.SAFETY) {
			throw new SafetyViolation(this, new MessageViolatedCause(expected, received));
		}
	}

	/**
	 * Throws a violation of the given type.
	 * 
	 * @param violationKind
	 * @throws InterruptViolation
	 * @throws SafetyViolation
	 * @throws Violation
	 */
	protected void throwViolation(ViolationKind violationKind) throws InterruptViolation, SafetyViolation, Violation {
		if (violationKind == ViolationKind.INTERRUPT) {
			throw new InterruptViolation(this);
		} else if (violationKind == ViolationKind.SAFETY) {
			throw new SafetyViolation(this);
		}
	}

	/**
	 * This method sets the state of the scenario. A scenario is active when it
	 * is initialized and not terminated or violated. A scenario that is
	 * terminated or violated sets its state to not active.
	 * 
	 * @param active
	 */
	protected void setActive(boolean active) {
		this.active = active;
	}

	private void setFinished(boolean finished) {
		this.finished = finished;
	}

	/**
	 * Requests a given message.
	 * 
	 * @param message
	 * @return
	 * @throws Violation
	 */
	protected Message request(Message message) throws Violation {
		return request(message.getSender(), message.getReceiver(), message.getMessage(), message.getParameters());
	}

	/**
	 * Requests a message with the given values.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @return
	 * @throws Violation
	 */
	protected Message request(Role<?> sender, Role<?> receiver, String message) throws Violation {
		return request(false, sender, receiver, message, new ArrayList<Object>());
	}

	/**
	 * Requests a message with the given values.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message request(Role<?> sender, Role<?> receiver, String message, Object... parameters) throws Violation {
		return request(false, sender, receiver, message, ListUtils.arrayToList(parameters));
	}

	/**
	 * Requests a message with the given values.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message request(Role<?> sender, Role<?> receiver, String message, List<Object> parameters)
			throws Violation {
		return request(false, sender, receiver, message, parameters);
	}

	/**
	 * Requests a message with the given values.
	 * 
	 * @param strict
	 * @param messageEvent
	 * @return
	 * @throws Violation
	 */
	protected Message request(boolean strict, Message messageEvent) throws Violation {
		return request(strict, messageEvent.getSender(), messageEvent.getReceiver(), messageEvent.getMessage(),
				messageEvent.getParameters());
	}

	/**
	 * Requests a message with the given values.
	 * 
	 * @param strict
	 * @param sender
	 * @param receiver
	 * @param message
	 * @return
	 * @throws Violation
	 */
	protected Message request(boolean strict, Role<?> sender, Role<?> receiver, String message) throws Violation {
		return request(strict, sender, receiver, message, new ArrayList<Object>());
	}

	/**
	 * Requests a message with the given values.
	 * 
	 * @param strict
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message request(boolean strict, Role<?> sender, Role<?> receiver, String message, List<Object> parameters)
			throws Violation {
		Message event = new Message(strict, true, sender, receiver, message, parameters);
		if (sender == Role.IDLE || ObjectRegistry.getInstance().isControllable(sender.getBinding())
				|| type.equals(SYSTEM)) {
			getRequestedMessages().add(event);
			raiseEvents();
			getRequestedMessages().remove(event);
			return event;
		} else {
			return waitFor(strict, sender, receiver, message, parameters);
		}
	}

	/**
	 * Requests a message with the given values.
	 * 
	 * @param strict
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message request(boolean strict, Role<?> sender, Role<?> receiver, String message, Object... parameters)
			throws Violation {
		return request(strict, sender, receiver, message, ListUtils.arrayToList(parameters));
	}

	protected Message controlledRequest(Message message) throws Violation {
		getRequestedMessages().add(message);
		raiseEvents();
		getRequestedMessages().remove(message);
		return message;
	}

	/**
	 * Waits for a given message.
	 * 
	 * @param messageEvent
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(Message messageEvent) throws Violation {
		return waitFor(messageEvent.getSender(), messageEvent.getReceiver(), messageEvent.getMessage(),
				messageEvent.getParameters());
	}

	/**
	 * Waits for a message with the given values.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(Role<?> sender, Role<?> receiver, String message) throws Violation {
		return waitFor(false, sender, receiver, message, new ArrayList<Object>());
	}

	/**
	 * Waits for a message with the given values.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(Role<?> sender, Role<?> receiver, String message, Object... parameters) throws Violation {
		return waitFor(false, sender, receiver, message, ListUtils.arrayToList(parameters));
	}

	/**
	 * Waits for a message with the given values.
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(Role<?> sender, Role<?> receiver, String message, List<Object> parameters)
			throws Violation {
		return waitFor(false, sender, receiver, message, parameters);
	}

	/**
	 * Waits for a message with the given values.
	 * 
	 * @param strict
	 * @param messageEvent
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(boolean strict, Message messageEvent) throws Violation {
		return waitFor(strict, messageEvent.getSender(), messageEvent.getReceiver(), messageEvent.getMessage(),
				messageEvent.getParameters());
	}

	/**
	 * Waits for a message with the given values.
	 * 
	 * @param strict
	 * @param sender
	 * @param receiver
	 * @param message
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(boolean strict, Role<?> sender, Role<?> receiver, String message) throws Violation {
		return waitFor(strict, sender, receiver, message, new ArrayList<Object>());
	}

	/**
	 * Waits for a message with the given values.
	 * 
	 * @param strict
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(boolean strict, Role<?> sender, Role<?> receiver, String message, List<Object> parameters)
			throws Violation {
		Message event = new Message(strict, false, sender, receiver, message, parameters);
		getWaitedForEvents().add(event);
		raiseEvents();
		getWaitedForEvents().remove(event);
		return event;
	}

	/**
	 * Waits for a message with the given values.
	 * 
	 * @param strict
	 * @param sender
	 * @param receiver
	 * @param message
	 * @param parameters
	 * @return
	 * @throws Violation
	 */
	protected Message waitFor(boolean strict, Role<?> sender, Role<?> receiver, String message, Object... parameters)
			throws Violation {
		return waitFor(strict, sender, receiver, message, ListUtils.arrayToList(parameters));
	}

	/**
	 * This method makes the scenario wait for any message.
	 */
	protected void waitForAnything() {
		bp.bSync(none, all, none);
	}

	/**
	 * This method runs one or more {@link ParallelCase} scenarios. The scenario
	 * calling this method sleeps until all cases are terminated.
	 * 
	 * @param cases
	 * @throws Violation
	 */
	protected void runParallel(ParallelCase... cases) throws Violation {
		for (ParallelCase caseScenario : cases) {
			getParallelCaseScenarios().add(caseScenario);
			caseScenario.setPriority(getNextFreeSubPriority());
			bp.add(caseScenario, caseScenario.getPriority());
		}
		for (ParallelCase caseScenario : cases) {
			caseScenario.startBThread();
		}
		while (isAnyParallelCaseActive(cases) && !getParallelCaseScenarios().isEmpty()) {
			synchronizeWithParallelCases();
			for (ParallelCase p : getParallelCaseScenarios()) {
				getEnabledMessages().addAll(p.getEnabledMessages());
				getWaitedForEvents().addAll(p.getWaitedForEvents());
			}
			if (getEnabledMessages().isEmpty()) {
				break;
			}
			raiseEvents();
		}
		clearParallelCaseScenarios();
		synchronizeWithParallelCases();
	}

	private void synchronizeWithParallelCases() throws Violation {
		bp.bSync(Message.SYNCHRONIZE, none, none);
	}

	private List<Message> requestedMessages;
	private List<Message> waitedForMessages;

	private List<ScenarioObserver> observers;

	/**
	 * Returns the list of observers.
	 * 
	 * @return
	 */
	public List<ScenarioObserver> getObservers() {
		if (observers == null)
			observers = new ArrayList<ScenarioObserver>();
		return observers;
	}

	/**
	 * Returns the list of currently requested messages.
	 * 
	 * @return
	 */
	public synchronized List<Message> getRequestedMessages() {
		if (requestedMessages == null) {
			requestedMessages = new ArrayList<Message>();
		}
		return requestedMessages;
	}

	/**
	 * Returns the list of messages the scenario currently waits for.
	 * 
	 * @return
	 */
	public synchronized List<Message> getWaitedForEvents() {
		if (waitedForMessages == null) {
			waitedForMessages = new ArrayList<Message>();
		}
		return waitedForMessages;
	}

	/**
	 * Returns a list that combines the requested and waited for lists.
	 * 
	 * @return
	 */
	public synchronized List<Message> getEnabledMessages() {
		List<Message> enabledEvents = new ArrayList<Message>();
		enabledEvents.addAll(getRequestedMessages());
		enabledEvents.addAll(getWaitedForEvents());
		return enabledEvents;
	}

	protected void raiseEvents() throws InterruptViolation, SafetyViolation, Violation {
		List<Message> blockedEvents = new ArrayList<Message>();
		List<Message> enabledEvents = getEnabledMessages();
		if (enabledEvents.isEmpty()) {
			return;
		}
		blockedEvents.addAll(retrieveBlockedMessages(getRequestedMessages(), getWaitedForEvents()));
		// Initializing events
		if (!isActive()) {
			for (Message event : enabledEvents) {
				event.setInitializing(true);
			}
		}
		// Cut strictness
		boolean cutIsStrict = false;
		for (Message event : enabledEvents) {
			if (event.isStrict()) {
				cutIsStrict = true;
			}
		}
		// bSync
		RequestableEventSet r = new RequestableEventSet();
		EventSet w = new EventSet();
		EventSet b = new EventSet();
		if (cutIsStrict) {
			// Cut is strict
			r.addAll(requestedMessages);
			w.addAll(waitedForMessages);
			w.addAll(getInterruptingMessages());
			if (isActive()) {
				b.addAll(blockedEvents);
				b.addAll(getForbiddenMessages());
			}
			bp.bSync(r, w, b);
			if (getInterruptingMessages().contains(getLastMessage()) && !enabledEvents.contains(getLastMessage())
					&& isActive()) {
				// Interrupt Scenario
				violate(ViolationKind.INTERRUPT, null, getLastMessage());
			}
		} else {
			// Cut is not strict
			r.addAll(requestedMessages);
			w.addAll(waitedForMessages);
			if (isActive()) {
				w.addAll(getInterruptingMessages());
				w.addAll(blockedEvents);
				b.addAll(getForbiddenMessages());
			}
			bp.bSync(r, w, b);
			if (blockedEvents.contains(getLastMessage()) && isActive()) {
				// Interrupt Scenario
				violate(ViolationKind.INTERRUPT, null, getLastMessage()); // TODO:
																			// show
				// all
				// events
			}
			if (getInterruptingMessages().contains(getLastMessage()) && !enabledEvents.contains(getLastMessage())
					&& isActive()) {
				// Interrupt Scenario
				violate(ViolationKind.INTERRUPT, null, getLastMessage());
			}
		}
		// Copy role bindings
		for (Message event : enabledEvents) {
			Message other = (Message) bp.lastEvent;
			if (event.isInitializing() && event.equals(other)) {
				event.getSender().copyBinding(other.getSender());
				event.getReceiver().copyBinding(other.getReceiver());
			}
		}
		// Wait while Executor is running
		waitForExecutor();
		if ((Settings.SCENARIO__PRINT_SYSTEM_PROGRESS && this.getType().equals(Scenario.SYSTEM))
				|| (Settings.SCENARIO__PRINT_SPECIFICATION_PROGRESS && this.getType().equals(Scenario.SPECIFICATION))
				|| (Settings.SCENARIO__PRINT_ASSUMPTION_PROGRESS && this.getType().equals(Scenario.ASSUMPTION))) {
			Settings.SCENARIO_OUT.println("!-> " + this.getName() + ": Progressed!");
		}
	}

	private void waitForExecutor() {
		// Sleep to refresh Executor
		sleep(0);
		// Block while executor is active
		if (!(this instanceof ExecutorScenario) && ExecutorScenario.getInstance() != null) {
			while (!ExecutorScenario.getInstance().isFinished()) {
				sleep(5);
			}
		}
	}

	/**
	 * Removes all parallel case scenario.s
	 */
	protected void clearParallelCaseScenarios() {
		getParallelCaseScenarios().clear();
	}

	private boolean isAnyParallelCaseActive(ParallelCase[] cases) {
		for (ParallelCase caseScenario : cases) {
			if (caseScenario.isActive()) {
				return true;
			}
		}
		return false;
	}

	private double getNextFreeSubPriority() {
		double priority = getPriority();
		if (bp == null) {
			return priority;
		}
		while (bp.getAllBThreads().containsKey(priority)) {
			priority = Math.nextUp(priority);
		}
		return priority;
	}

	/**
	 * This method progresses the scenario with more than one possible message.
	 * This is used for alternatives.
	 * 
	 * @param requestedEvents
	 * @param waitedForEvents
	 */
	protected void doStep(List<Message> requestedEvents, List<Message> waitedForEvents) {
		RequestableEventSet requestableEventSet = new RequestableEventSet();
		EventSet waitedEventSet = new EventSet();
		EventSet blockedEventSet = new EventSet();
		if (waitedForEvents != null) {
			for (Message messageEvent : waitedForEvents) {
				waitedEventSet.add(messageEvent);
			}
		}
		if (requestedEvents != null) {
			for (Message messageEvent : requestedEvents) {
				if (ObjectRegistry.getInstance().isControllable(messageEvent.getSender().getBinding())) {
					requestableEventSet.add(messageEvent);
				} else {
					waitedEventSet.add(messageEvent);
				}
			}
		}
		blockedEventSet = retrieveBlockedMessages(requestableEventSet, waitedEventSet);
		bp.bSync(requestableEventSet, waitedEventSet, blockedEventSet);
	}

	/**
	 * This method returns a list of blocked events corresponding to the input
	 * event. It returns all events of the blockedEvents list, except for the
	 * input event.
	 * 
	 * @param event
	 * @return
	 */
	protected EventSet retrieveBlockedMessages(Message event) {
		EventSet blocked = new EventSet();
		if (isActive()) { // Only active scenarios can block events
			for (Message e : getBlockedMessages()) {
				if (!e.equals(event)) {
					blocked.add(e);
				}
			}
		}
		return blocked;
	}

	/**
	 * This method returns a list of blocked events corresponding to the input
	 * event lists. It returns all events of the blockedEvents list, except for
	 * the input events of the requested and waitedFor lists.
	 *
	 * @param requestedEvents
	 * @param waitedForEvents
	 * @return
	 */
	protected EventSet retrieveBlockedMessages(RequestableInterface requestedEvents,
			EventSetInterface waitedForEvents) {
		EventSet blocked = new EventSet();
		if (isActive()) { // Only active scenarios can block events
			for (Message e : getBlockedMessages()) {
				if (!requestedEvents.contains(e) && !waitedForEvents.contains(e)) {
					blocked.add(e);
				}
			}
		}
		return blocked;
	}

	/**
	 * This method returns a list of blocked events corresponding to the input
	 * event lists. It returns all events of the blockedEvents list, except for
	 * the input events of the requested and waitedFor lists.
	 *
	 * @param requestedEvents
	 * @param waitedForEvents
	 * @return
	 */
	protected List<Message> retrieveBlockedMessages(List<Message> requestedEvents, List<Message> waitedForEvents) {
		List<Message> blocked = new ArrayList<Message>();
		if (isActive()) { // Only active scenarios can block events
			for (Message e : getBlockedMessages()) {
				if (!requestedEvents.contains(e) && !waitedForEvents.contains(e)) {
					blocked.add(e);
				}
			}
		}
		return blocked;
	}

	@Override
	public void runBThread() throws InterruptedException, BPJRequestableSetException {
		do {
			try {
				runLifeCycle();
			} catch (InterruptViolation e) {
				catchInterruptViolation(e);
				setActive(false);
			} catch (SafetyViolation e) {
				// Restart scenarios in package? Future work? What is the
				// problem here?
				catchSafetyViolation(e);
				setActive(false);
			} catch (Violation e) {
				e.printStackTrace();
			}
		} while (getExecutionMode() == ExecutionMode.LOOP);
		setActive(false);
		setFinished(true);
	}

	/**
	 * This method is run when a safety violation occurred.
	 * 
	 * @param e
	 */
	protected void catchSafetyViolation(SafetyViolation e) {
		System.out.println("<!> " + e.getMessage());
	}

	/**
	 * This method is run when an interrupt violation occurred.
	 * 
	 * @param e
	 */
	protected void catchInterruptViolation(InterruptViolation e) {
		System.out.println("<!> " + e.getMessage());
	}

	protected void runLifeCycle() throws Violation {
		// Wait while Executor is running
		waitForExecutor();
		// Copy bindings from initializing event
		copyBindingsFromInitializingEvent();
		try {
			registerRoleBindings();
		} catch (Exception e) {
			throwViolation(INTERRUPT);
		}
		registerAlphabet();
		setActive(true);
		body();
		setActive(false);
		setFinished(true);
		reset();
	}

	@SuppressWarnings("unchecked")
	private void copyBindingsFromInitializingEvent() {
		Message lastEvent = getLastMessage();
		for (Message initializingEvent : getInitializingMessages()) {
			if (lastEvent.equals(initializingEvent)) {
				initializingEvent.getSender().copyBinding(lastEvent.getSender());
				initializingEvent.getReceiver().copyBinding(lastEvent.getReceiver());
				getActiveBindings().add(initializingEvent.getSender().getBinding());
				getActiveBindings().add(initializingEvent.getReceiver().getBinding());
			}
		}
	}

	/**
	 * This method binds a role of the scenario to a given object. The role
	 * binding will be reset after the scenario is terminated.
	 * 
	 * @param role
	 *            to bind to object
	 * @param object
	 *            to bind role to
	 */
	protected <T> void bindRoleToObject(Role<T> role, T object) {
		role.setBinding(object);
		getActiveBindings().add(object);
	}

	/**
	 * Makes the execution of the scenario sleep. Beware: The whole system will
	 * sleep since there is no synchronization until the scenario wakes up
	 * again.
	 * 
	 * @param millis
	 */
	protected void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the last caught message.
	 * 
	 * @return
	 */
	protected Message getLastMessage() {
		return (Message) bp.lastEvent;
	}

	/**
	 * Adds an observer to this scenario.
	 * 
	 * @param scenarioObserver
	 */
	public void addObserver(ScenarioObserver scenarioObserver) {
		getObservers().add(scenarioObserver);
	}

	/**
	 * Removes an observer from this scenario.
	 * 
	 * @param scenarioObserver
	 */
	public void removeObserver(ScenarioObserver scenarioObserver) {
		getObservers().remove(scenarioObserver);
	}

	/**
	 * Initializes a scenario.
	 */
	public void initialize() {
		initialisation();
	}

	/**
	 * Creates an inactive copy of the scenario.
	 * 
	 * @return
	 */
	public Scenario copy() {
		Scenario newInstance = null;
		try {
			newInstance = this.getClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		newInstance.initialize();
		return newInstance;
	}

	/**
	 * Returns active bindings of a scenario.
	 * 
	 * @return
	 */
	public List<Object> getActiveBindings() {
		if (activeBindings == null) {
			activeBindings = new ArrayList<Object>();
		}
		return activeBindings;
	}

}
