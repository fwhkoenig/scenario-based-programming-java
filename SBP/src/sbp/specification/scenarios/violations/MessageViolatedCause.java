package sbp.specification.scenarios.violations;

import bp.Event;

public class MessageViolatedCause implements ViolationCause {

	private Event expected;
	private Event received;

	public MessageViolatedCause(Event expected, Event received) {
		this.expected = expected;
		this.received = received;
	}

	@Override
	public String toString() {
		return "Caused by: Expected " + expected + " | received " + received + ".";
	}

}
