package sbp.specification.scenarios.violations;

import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ViolationKind;

@SuppressWarnings("serial")
public class InterruptViolation extends Violation {

	public InterruptViolation(Scenario scenario, ViolationCause violationCause) {
		super(scenario, violationCause, ViolationKind.INTERRUPT);
	}

	public InterruptViolation(Scenario scenario) {
		super(scenario, ViolationKind.INTERRUPT);
	}

}
