package sbp.specification.events;

import java.util.ArrayList;
import java.util.List;

import bp.Event;
import sbp.runtime.ObjectRegistry;
import sbp.specification.role.Role;

@SuppressWarnings("serial")
public class Message extends Event {

	public static final Message IDLE = new Message(Role.IDLE, Role.IDLE, "idle");
	public static final Message SYNCHRONIZE = new Message(Role.IDLE, Role.IDLE, "synchronize");

	private Role sender;
	private Role receiver;
	private String message;
	private List<Object> parameters;

	private boolean strict = false;
	private boolean requested = false;

	private boolean initializing;

	public Message(Role sender, Role receiver, String message) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.message = message;
		this.parameters = new ArrayList<Object>();
		setName(sender + "->" + receiver + "." + message);
	}

	public Message(Role sender, Role receiver, String message, Object... parameters) {
		this(sender, receiver, message);
		for (int i = 0; i < parameters.length; i++) {
			Object p = parameters[i];
			this.getParameters().add(p);
		}
	}

	public Message(boolean strict, boolean requested, Role sender, Role receiver, String message) {
		this(sender, receiver, message);
		this.strict = strict;
		this.requested = requested;
	}

	public Message(boolean strict, boolean requested, Role sender, Role receiver, String message,
			List<Object> parameters) {
		this(sender, receiver, message);
		this.strict = strict;
		this.parameters = parameters;
		this.requested = requested;
	}

	public Message(boolean strict, boolean requested, Role sender, Role receiver, String message,
			Object... parameters) {
		this(sender, receiver, message, parameters);
		this.strict = strict;
		this.requested = requested;
	}

	public Message(boolean strict, Role sender, Role receiver, String message, Object... parameters) {
		this(sender, receiver, message, parameters);
		this.strict = strict;
	}

	public Role getSender() {
		return sender;
	}

	public Role getReceiver() {
		return receiver;
	}

	public String getMessage() {
		return message;
	}

	public List<Object> getParameters() {
		return parameters;
	}

	public boolean isStrict() {
		return strict;
	}

	public boolean isRequested() {
		return requested;
	}

	public void setStrict(boolean strict) {
		this.strict = strict;
	}

	public void setRequested(boolean requested) {
		this.requested = requested;
	}

	@Override
	public boolean equals(Object obj) {
		boolean unifiable = false;
		if (obj instanceof Message) {
			Message other = (Message) obj;
			unifiable = isUnifiable(other);
		}
		return unifiable;
	}

	private boolean isUnifiable(Message other) {
		if (!message.equals(other.message)) {
			return false;
		}
		if (!sender.equals(other.sender)) {
			return false;
		}
		if (!receiver.equals(other.receiver)) {
			return false;
		}
		if (!MessageParameter.compareParameterLists(getParameters(), other.getParameters())) {
			return false;
		}
		if (this.hasBindings() && other.hasBindings()) {
			// Check for equal bindings
			boolean sendersAreEqual = this.getSender().getBinding() == other.getSender().getBinding();
			boolean receiversAreEqual = this.getReceiver().getBinding() == other.getReceiver().getBinding();
			if (sendersAreEqual && receiversAreEqual) {
				return true;
			}
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String out = "message ";
		if (strict)
			out += "strict ";
		if (requested)
			out += "requested ";
		return out + getName();
	}

	@Override
	public String getName() {
		String n = "(";
		for (int i = 0; i < getParameters().size(); i++) {
			Object p = getParameters().get(i);
			n += p.toString();
			if (i < getParameters().size() - 1) {
				n += ", ";
			}
		}
		n += ")";
		return getSender() + " -> " + getReceiver() + "." + getMessage() + n;
	}

	public void setInitializing(boolean initializing) {
		this.initializing = initializing;
	}

	public boolean isInitializing() {
		return initializing;
	}

	public boolean hasBindings() {
		return getSender().hasBinding() && getReceiver().hasBinding();
	}

	public Message serializeForNetwork() {
		Message out;
		Role newSender = new Role(sender.getType(),sender.getName());
		Role newReceiver= new Role(receiver.getType(),receiver.getName());
		newSender.copyBinding(sender);
		newReceiver.copyBinding(receiver);
		if (getParameters().isEmpty()) {
			out = new Message(newSender, newReceiver, message);
		} else {
			List<Object> newParameters = new ArrayList<Object>();
			for (Object parameter : getParameters()) {
				if (ObjectRegistry.getInstance().getID(parameter) == null) {
					newParameters.add(parameter);
				} else {
					newParameters.add(new ObjectParameter(ObjectRegistry.getInstance().getID(parameter)));
				}
			}
			out = new Message(newSender, newReceiver, message, newParameters.toArray());
		}
		return out;
	}

	public Message deserializeForNetwork() {
		Message out;
		if (getParameters().isEmpty()) {
			out = new Message(sender, receiver, message);
		} else {
			List<Object> newParameters = new ArrayList<Object>();
			for (Object parameter : getParameters()) {
				if (parameter instanceof ObjectParameter) {
					newParameters.add(ObjectRegistry.getInstance().getObject(((ObjectParameter) parameter).getId()));
				} else {
					newParameters.add(parameter);
				}
			}
			out = new Message(sender, receiver, message, newParameters.toArray());
		}
		return out;
	}

}
