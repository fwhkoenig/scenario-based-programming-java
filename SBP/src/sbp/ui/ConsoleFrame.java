package sbp.ui;

import java.awt.FlowLayout;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

@SuppressWarnings("serial")
public class ConsoleFrame extends JFrame {

	private ConsolePanel runtimeConsolePanel;
	private ConsolePanel serverConsolePanel;

	JTabbedPane tabbedPane;

	public ConsoleFrame(String name) {
		setName(name);
		setTitle(name);
		setLayout(new FlowLayout());

		tabbedPane = new JTabbedPane();
		add(tabbedPane);

		runtimeConsolePanel = new ConsolePanel();
		serverConsolePanel = new ConsolePanel();

		tabbedPane.addTab("Runtime", null, runtimeConsolePanel, "Runtime Console");
		tabbedPane.addTab("Server", null, serverConsolePanel, "Server Console");

		pack();
		setResizable(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public PrintStream getRuntimeLog() {
		return runtimeConsolePanel.getLog();
	}

	public PrintStream getServerLog() {
		return serverConsolePanel.getLog();
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void addPanel(String title, JPanel panel) {
		tabbedPane.addTab(title, panel);
	}

}