package sbp.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.utils.ListUtils;

@SuppressWarnings("serial")
public class MessageButton<S, R> extends JButton implements ActionListener {

	public static final String SENDER_NAME = "sender";
	public static final String RECEIVER_NAME = "receiver";

	private RuntimeAdapter runtimeAdapter;
	private Message messageEvent;

	@SuppressWarnings("unchecked")
	public MessageButton(RuntimeAdapter runtimeAdapter, S senderObject,
			R receiverObject, String message, Object... parameters) {
		super(message);
		this.runtimeAdapter = runtimeAdapter;
		Role<S> sender = new Role<S>((Class<S>) senderObject.getClass(), SENDER_NAME);
		Role<R> receiver = new Role<R>((Class<R>) receiverObject.getClass(), RECEIVER_NAME);
		sender.setBinding(senderObject);
		receiver.setBinding(receiverObject);
		this.messageEvent = new Message(false, true, sender, receiver, message, ListUtils.arrayToList(parameters));
		setText(this.messageEvent.getName());
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		runtimeAdapter.receive(messageEvent);
		runtimeAdapter.publish(messageEvent);
	}

}
