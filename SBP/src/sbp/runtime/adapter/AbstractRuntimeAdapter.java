package sbp.runtime.adapter;

import sbp.runtime.SpecificationRunconfig;
import sbp.specification.scenarios.system.EventQueueScenario;

public abstract class AbstractRuntimeAdapter implements RuntimeAdapter {

	private SpecificationRunconfig<?> specificationRunconfig;
	
	public AbstractRuntimeAdapter(SpecificationRunconfig<?> specificationRunconfig) {
		setSpecificationRunconfig(specificationRunconfig);
	}

	@Override
	public SpecificationRunconfig<?> getSpecificationRunconfig() {
		return specificationRunconfig;
	}

	@Override
	public void setSpecificationRunconfig(SpecificationRunconfig<?> specificationRunconfig) {
		this.specificationRunconfig = specificationRunconfig;
	}

	@Override
	public EventQueueScenario getEventQueueScenario() {
		return specificationRunconfig.getEventQueueScenario();
	}

}
