package sbp.runtime.network.client;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import sbp.runtime.network.exceptions.UnableToConnectException;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;

public class Client {

	private static final int MAX_CONNECTION_COUNT = 100;
	private static final long DELAY_BETWEEN_RECONNECTS = 500;

	private List<ServerConnector> servers;

	public List<ServerConnector> getServers() {
		if (servers == null) {
			servers = new ArrayList<ServerConnector>();
		}
		return servers;
	}

	public void connect(String hostName, int port, Object object)
			throws UnknownHostException, IOException, UnableToConnectException {
		int connectionCount = 0;
		boolean connected = false;
		do {
			try {
				Settings.NETWORK_OUT.println("Connecting to " + hostName + ":" + port + "...");
				ServerConnector server = new ServerConnector(hostName, port, new Socket(hostName, port));
				server.setObjectBinding(object);
				getServers().add(server);
				Settings.NETWORK_OUT.println("Connected to " + hostName + ":" + port);
				connected = true;
			} catch (ConnectException e) {
				Settings.NETWORK_OUT.println("Connection failed: " + e.getMessage());
				connectionCount++;
				sleep();
			}
		} while (connectionCount < MAX_CONNECTION_COUNT && !connected);
		if (!connected) {
			throw new UnableToConnectException();
		}
	}

	private void sleep() {
		try {
			Thread.sleep(DELAY_BETWEEN_RECONNECTS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	public void publish(Message messageEvent) throws UnknownHostException, IOException {
		for (ServerConnector server : getServers()) {
			server.send(messageEvent);
			Settings.NETWORK_OUT
					.println("Sending to " + server.getHostName() + ":" + server.getPort() + " -> " + messageEvent);
		}
	}

	public void publish(Message messageEvent, List<Object> objects) throws UnknownHostException, IOException {
		for (ServerConnector server : getServers()) {
			if (objects.contains(server.getObjectBinding())) {
				server.send(messageEvent);
				Settings.NETWORK_OUT
						.println("Sending to " + server.getHostName() + ":" + server.getPort() + " -> " + messageEvent);
			}
		}
	}

	public void disconnect() throws IOException {
		for (ServerConnector server : getServers()) {
			Settings.NETWORK_OUT.println("Disconnecting from " + server.getHostName() + ":" + server.getPort());
			server.close();
		}
	}

	public boolean isConnectedTo(String hostName, int port) {
		for (ServerConnector server : getServers()) {
			if (server.getHostName().equals(hostName) && server.getPort() == port) {
				return true;
			}
		}
		return false;
	}

}
