package sbp.runtime.network.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import sbp.runtime.network.exceptions.ClientByeException;
import sbp.runtime.network.messages.Messages;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.scenarios.system.EventQueueScenario;

public class ClientConnector extends Thread {

	private static final long RECEIVE_DELAY = 500;
	private Socket socket;
	private ObjectInputStream in;
	private boolean running;
	private EventQueueScenario eventQueueScenario;

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public ClientConnector(Socket socket) {
		this.socket = socket;
		try {
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException ignored) {
		}
		setRunning(true);
	}

	public void setEventQueueScenario(EventQueueScenario eventQueueScenario) {
		this.eventQueueScenario = eventQueueScenario;
	}

	@Override
	public void run() {
		try {
			while (isRunning()) {
				receive(in);
				sleep();
			}
		} catch (ClientByeException e) {

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ignored) {
		} finally {
			try {
				in.close();
				socket.close();
			} catch (IOException ignored) {
			}
			Settings.NETWORK_OUT.println("Client dropped");
		}
	}

	private void sleep() {
		try {
			Thread.sleep(RECEIVE_DELAY);
		} catch (InterruptedException ignored) {
		}
	}

	private void receive(ObjectInputStream in) throws IOException, ClassNotFoundException, ClientByeException {
		Object objectReceived = in.readObject();
		// System.out.println("Received: " + objectReceived);
		if (objectReceived instanceof Message) {
			Message messageEvent = (Message) objectReceived;
			messageEvent = messageEvent.deserializeForNetwork();
			Settings.NETWORK_OUT.println("Received: " + messageEvent.getName());
			eventQueueScenario.enqueueEvent(messageEvent);
		} else if (objectReceived instanceof String) {
			String message = (String) objectReceived;
			if (message.equals(Messages.BYE.toString())) {
				throw new ClientByeException();
			}
		}
	}

	public void shutDown() {
		setRunning(false);
		try {
			in.close();
		} catch (IOException ignored) {
		}
		this.interrupt();
	}

}
