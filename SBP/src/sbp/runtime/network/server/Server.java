package sbp.runtime.network.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import sbp.runtime.settings.Settings;
import sbp.specification.scenarios.system.EventQueueScenario;

public class Server extends Thread {

	private static final int SOCKET_TIME_OUT = 1000;

	private ServerSocket server;
	private List<ClientConnector> clients;
	private boolean running;
	private int port;
	private EventQueueScenario eventQueueScenario;

	public void setEventQueueScenario(EventQueueScenario eventQueueScenario) {
		this.eventQueueScenario = eventQueueScenario;
	}

	public Server(int port) {
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public List<ClientConnector> getClients() {
		if (clients == null) {
			clients = new ArrayList<ClientConnector>();
		}
		return clients;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	@Override
	public void run() {
		try {
			openServer();
			acceptClients();
			closeServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void shutDown() {
		setRunning(false);
	}

	private void openServer() throws IOException {
		Settings.NETWORK_OUT.println("Opening server on port " + getPort());
		server = new ServerSocket(getPort());
		server.setSoTimeout(SOCKET_TIME_OUT);
		setRunning(true);
		Settings.NETWORK_OUT.println("Opened server on port " + getPort());
	}

	private void acceptClients() {
		Settings.NETWORK_OUT.println("Waiting for clients...");
		while (isRunning()) {
			ClientConnector client = null;
			try {
				client = new ClientConnector(server.accept());
				client.setEventQueueScenario(eventQueueScenario);
				Settings.NETWORK_OUT.println("Client accepted");
				getClients().add(client);
				client.start();
			} catch (IOException ignored) {
			}
		}
	}

	private void closeServer() throws IOException {
		for (ClientConnector client : clients) {
			client.shutDown();
		}
		server.close();
	}

}
