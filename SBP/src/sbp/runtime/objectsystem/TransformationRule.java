package sbp.runtime.objectsystem;

import sbp.specification.events.Message;
import sbp.specification.events.ParameterRandom;

public abstract class TransformationRule {

	public static final ParameterRandom RANDOM = ParameterRandom.getInstance();

	public boolean match(Message event) {
		return event.equals(getTriggerMessage());
	}

	public abstract Message getTriggerMessage();

	public abstract void execute(Message event);

}
