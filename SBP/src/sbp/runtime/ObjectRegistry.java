package sbp.runtime;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import sbp.runtime.network.NetworkEntry;
import sbp.runtime.network.ObjectNetworkMapper;

public class ObjectRegistry {

	private static ObjectRegistry instance;

	private Map<Integer, Object> iDToObjectMap;
	private Map<Object, Integer> objectToIDMap;
	private Map<Object, Boolean> objectControllableMap;
	private ObjectNetworkMapper objectNetworkMapper;

	private int currentID = 0;

	public static ObjectRegistry getInstance() {
		if (instance == null) {
			instance = new ObjectRegistry();
		}
		return instance;
	}

	public Map<Integer, Object> getiDToObjectMap() {
		if (iDToObjectMap == null) {
			iDToObjectMap = new HashMap<Integer, Object>();
		}
		return iDToObjectMap;
	}

	public Map<Object, Integer> getObjectToIDMap() {
		if (objectToIDMap == null) {
			objectToIDMap = new HashMap<Object, Integer>();
		}
		return objectToIDMap;
	}

	public Map<Object, Boolean> getObjectControllableMap() {
		if (objectControllableMap == null) {
			objectControllableMap = new HashMap<Object, Boolean>();
		}
		return objectControllableMap;
	}

	public ObjectNetworkMapper getObjectNetworkMapper() {
		if (objectNetworkMapper == null) {
			objectNetworkMapper = new ObjectNetworkMapper();
		}
		return objectNetworkMapper;
	}

	public Object getObject(int id) {
		if (id == -1) {
			return null;
		}
		return getiDToObjectMap().get(id);
	}

	public Integer getID(Object object) {
		if (!getObjectToIDMap().keySet().contains(object)) {
			return -1;
		}
		return getObjectToIDMap().get(object);
	}

	public int addObject(Object object) {
		int id = getFreeID();
		getiDToObjectMap().put(id, object);
		getObjectToIDMap().put(object, id);
		getObjectControllableMap().put(object, true);
		return id;
	}

	private Integer getFreeID() {
		int id = currentID;
		currentID++;
		return id;
	}

	public void setUncontrollable(Object object) {
		getObjectControllableMap().put(object, false);
	}

	public boolean isControllable(Object binding) {
		return getObjectControllableMap().get(binding);
	}

	public boolean isUnControllable(Object binding) {
		return !isControllable(binding);
	}

	public void setNetworkAddress(Object object, String hostName, int port) {
		NetworkEntry networkEntry = new NetworkEntry(hostName, port);
		getObjectNetworkMapper().addMapping(networkEntry, object);
	}

	public Set<NetworkEntry> getNetworkAddresses() {
		return getObjectNetworkMapper().getNetworkToEntryObjectMap().keySet();
	}

}
