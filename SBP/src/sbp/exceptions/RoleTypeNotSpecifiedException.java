package sbp.exceptions;

import sbp.specification.role.Role;

@SuppressWarnings("serial")
public class RoleTypeNotSpecifiedException extends RuntimeException {

	public RoleTypeNotSpecifiedException(Role<?> role) {
		super("The role " + role.getName()
				+ " has no type specified! Use type argument in constructor or use {} after role initialization!");
	}

}
