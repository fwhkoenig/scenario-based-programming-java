### How do I get set up the SBP library project? ###

* Install Eclipse
* Import the SBP project into the workspace
* Import the BP project into the workspace: http://www.wisdom.weizmann.ac.il/~bprogram/bpj/
* Configure the SBP project:
* * Link the SBP project to the BP project or
* * Create a jar from the BP project and load it into the SBP project

### How do I get set up the SBP Generator project? ###

* Install Eclipse Modeling Tools
* Import the SBP-Generator project into the workspace
* Import the compatible ScenarioTools version into the workspace: https://bitbucket.org/jgreenyer/scenariotools-sml/branch/sbp-compatible
* Launch an eclipse runtime application

### How do I get set up the Car-To-X example project? ###

* In the eclipse runtime application:
* Import the sbp.examples.cartox project into the workspace
* Add the BP project or a generated jar to the project

### Who do I talk to? ###

* Repo owner