import "../model/cartox.ecore"

system specification CarToX {

	domain cartox

	define Car as controllable
	define ObstacleControl as controllable
	define Environment as uncontrollable
	define Driver as uncontrollable
	define Dashboard as uncontrollable
	define Obstacle as uncontrollable
	define LaneArea as uncontrollable
	define StreetSection as uncontrollable

	collaboration CarDriving {

		dynamic role Environment env
		dynamic role Car car

		specification scenario CarMovesToNextArea {
			message env -> car.movedToNextArea
		}

		specification scenario CarChangesToOppositeArea {
			message env -> car.changedToOppositeArea
		}

		specification scenario CarMovesToNextAreaOnOvertakingLane {
			message env -> car.movedToNextAreaOnOvertakingLane
		}

	}

	collaboration CarApproachingObstacleAssumptions {

		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea
		dynamic role LaneArea oppositeNextArea
		dynamic role Obstacle obstacle

		/*
		 * When a car approaches an obstacle on the blocked lane,
		 * an according event will occur
		 */
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_CarOnRightLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.next
			bind obstacle to nextArea.obstacle
		] {
			message env -> car.movedToNextArea
			interrupt if [ obstacle == null ]
			message strict requested env -> car.setApproachingObstacle(obstacle)
		} constraints [
			forbidden message env -> car.movedToNextAreaOnOvertakingLane()
		]

		/*
		 * When a car approaches an obstacle but is currently driving on the lane that is
		 * not blocked, an according event will occur 
		 */
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_CarOnLeftLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.next
			bind oppositeNextArea to nextArea.opposite
			bind obstacle to oppositeNextArea.obstacle
		] {
			message env -> car.movedToNextArea
			interrupt if [ obstacle == null ]
			message strict requested env -> car.setApproachingObstacle(obstacle)
		} constraints [
			forbidden message env -> car.movedToNextArea()
		]

	}

	collaboration CarEntersOrLeavesNarrowPassageAssumptions {

		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea oppositeArea
		dynamic role LaneArea oppositePrevArea

		/*
		 * When a car enters the narrow passage area,
		 * a corresponding event "entersNarrowPassage" will occur.
		 */
		assumption scenario CarEntersNarrowPassage_ObstacleOnRightLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind oppositeArea to currentArea.opposite
			bind oppositePrevArea to oppositeArea.previous
		] {
			message env -> car.movedToNextAreaOnOvertakingLane
			// If on the right side is an obstacle AND before there was no obstacle
			// then the car enters the narrow passage
			interrupt if [ oppositeArea.obstacle == null | oppositePrevArea.obstacle != null ]
			message strict requested env -> car.hasEnteredNarrowPassage
		}

		assumption scenario CarEntersNarrowPassage_ObstacleOnLeftLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind oppositeArea to currentArea.opposite
			bind oppositePrevArea to oppositeArea.next
		] {
			message env -> car.movedToNextArea
			// If on the left side is an obstacle AND before there was no obstacle
			// then the car enters the narrow passage
			interrupt if [ oppositeArea.obstacle == null | oppositePrevArea.obstacle != null ]
			message strict requested env -> car.hasEnteredNarrowPassage
		}

		/*
		 * When a car has left the narrow passage area,
		 * a corresponding event "hasLeftNarrowPassage" will occur.
		 */
		assumption scenario CarHasLeftNarrowPassage_ObstacleOnRightLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind oppositeArea to currentArea.opposite
			bind oppositePrevArea to oppositeArea.previous
		] {
			message env -> car.movedToNextAreaOnOvertakingLane
			interrupt if [ oppositeArea.obstacle != null | oppositePrevArea.obstacle == null ]
			message strict requested env -> car.hasLeftNarrowPassage
		} constraints [
			forbidden message env -> car.changedToOppositeArea()
		]

		assumption scenario CarHasLeftNarrowPassage_ObstacleOnLeftLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind oppositeArea to currentArea.opposite
			bind oppositePrevArea to oppositeArea.next
		] {
			message env -> car.movedToNextArea
			interrupt if [ oppositeArea.obstacle != null | oppositePrevArea.obstacle == null ]
			message strict requested env -> car.hasLeftNarrowPassage
		} constraints [
			forbidden message env -> car.changedToOppositeArea()
		]

	}

	collaboration ShowInformationOnDashboard {

		dynamic role Environment env
		dynamic role Car car
		dynamic role Driver driver
		dynamic role Dashboard dashboard
		dynamic role ObstacleControl obstacleControl

		/*
		 * Show stop or go to the driver when approaching the obstacle
		 * before actually reaching it
		 */
		specification scenario DashboardOfCarOnBlockedLaneShowsStopOrGo
		with dynamic bindings [
			bind driver to car.driver
			bind dashboard to car.dashboard
		] {
			message env -> car.setApproachingObstacle(*)
			alternative {
				message strict car -> dashboard.showGo
			} or {
				message strict car -> dashboard.showStop
			}
		}

		/*
		 * The dashboard shows a go or a stop light as reaction to the obstacle
		 * controls response after registering
		 */
		specification scenario DashboardShowsGo
		with dynamic bindings [
			bind dashboard to car.dashboard
		] {
			message obstacleControl -> car.enteringAllowed
			message strict requested car -> dashboard.showGo
		}

		specification scenario DashboardShowsStop
		with dynamic bindings [
			bind dashboard to car.dashboard
		] {
			message obstacleControl -> car.enteringDisallowed
			message strict requested car -> dashboard.showStop
		}

	}

	collaboration CarsRegisterWithObstacleControl {

		dynamic role Environment env
		dynamic role Car car
		dynamic role Car nextCar
		dynamic role Dashboard dashboard
		dynamic role Obstacle obstacle
		dynamic role ObstacleControl obstacleControl
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea

		/*
		 * Stop or go must be shown to the driver when the car
		 * approaches an obstacle
		 */
		specification scenario ControlStationAllowsOrDisallowsCarToEnterNarrowPassage
		with dynamic bindings [
			bind obstacle to car.approachingObstacle
			bind obstacleControl to obstacle.controlledBy
			bind dashboard to car.dashboard
		] {
			message env -> car.setApproachingObstacle(*)
			message strict requested car -> obstacleControl.register
			alternative {
				message strict requested obstacleControl -> car.enteringAllowed
			} or {
				message strict requested obstacleControl -> car.enteringDisallowed
			}
		}

		specification scenario ControlStationAllowsOrDisallowsCarOnBlockedLaneToEnter
		with dynamic bindings [
			bind obstacle to car.approachingObstacle
			bind obstacleControl to obstacle.controlledBy
			bind dashboard to car.dashboard
		] {
			message env -> car.setApproachingObstacle(*)
			message strict requested car -> obstacleControl.register
			alternative if [ obstacleControl.carsOnNarrowPassageLaneAllowedToPass.isEmpty() ] {
				message strict requested obstacleControl -> car.enteringAllowed
				message strict car -> dashboard.showGo
			} or if [ ! obstacleControl.carsOnNarrowPassageLaneAllowedToPass.isEmpty() ] {
				message strict requested obstacleControl -> car.enteringDisallowed
				message strict car -> dashboard.showStop
			}
		}

		specification scenario CarUnregistersAfterLeavingNarrowPassage
		with dynamic bindings [
			bind obstacle to car.approachingObstacle
			bind obstacleControl to obstacle.controlledBy
		] {
			message env -> car.hasLeftNarrowPassage
			message strict requested car -> obstacleControl.unregister
		}

		/*
		 * The obstacle control tells the next car that it may now enter the
		 * narrow passage
		 */
		specification scenario ControlStationTellsNextRegisteredCarToEnter
		with dynamic bindings [
			bind nextCar to obstacleControl.registeredCarsWaiting.first()
		] {
			message car -> obstacleControl.unregister
			interrupt if [ nextCar == null ]
			message strict requested obstacleControl -> nextCar.enteringAllowed
		}

		/*
		 * A car that has been told to wait may eventually continue driving
		 * and pass the obstacle
		 */
		specification scenario CarEventuallyAllowedToPassObstacle {
			message obstacleControl -> car.enteringDisallowed()
			message strict obstacleControl -> car.enteringAllowed()
		}

		/*
		 * A car that has been told that it may pass the obstacle will enter
		 * the narrow passage
		 */
		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle_CarOnRightLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.next
			bind env to car.environment
		] {
			message obstacleControl -> car.enteringAllowed
			interrupt if [ car.onLane != car.drivingInDirectionOfLane ]
			alternative if [ nextArea.obstacle == null ] {
				message strict requested env -> car.movedToNextArea
			} or if [ nextArea.obstacle != null ] {
				message strict requested env -> car.changedToOppositeArea
				message strict requested env -> car.movedToNextAreaOnOvertakingLane
			}
		}

		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle_CarOnLeftLane
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.previous
			bind env to car.environment
		] {
			message obstacleControl -> car.enteringAllowed
			interrupt if [ car.onLane == car.drivingInDirectionOfLane ]
			alternative if [ nextArea.obstacle == null ] {
				message strict requested env -> car.movedToNextAreaOnOvertakingLane
			} or if [ nextArea.obstacle != null ] {
				message strict requested env -> car.changedToOppositeArea
				message strict requested env -> car.movedToNextArea
			}
		}

	}

}