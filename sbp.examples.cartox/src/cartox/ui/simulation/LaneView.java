package cartox.ui.simulation;

import java.awt.Color;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import cartox.Lane;
import cartox.LaneArea;

@SuppressWarnings("serial")
public class LaneView extends JPanel {

	private Lane lane;

	/**
	 * Create the panel.
	 * 
	 * @param carToXSimulationFrame
	 */
	public LaneView(Lane lane, StreetSectionView streetSectionView) {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0 };
		gridBagLayout.rowHeights = new int[] { 0 };
		gridBagLayout.columnWeights = new double[] { Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { Double.MIN_VALUE };
		setLayout(gridBagLayout);
		this.lane = lane;
	}

	public Lane getLane() {
		return lane;
	}

	public LaneAreaView getNext(LaneAreaView areaView) {
		StreetSectionView streetSectionView = (StreetSectionView) getParent();
		LaneArea model = areaView.getLaneArea();
		return streetSectionView.getViewForLane(model.getNext());
	}

	public LaneAreaView getPrevious(LaneAreaView areaView) {
		StreetSectionView streetSectionView = (StreetSectionView) getParent();
		LaneArea model = areaView.getLaneArea();
		return streetSectionView.getViewForLane(model.getPrevious());
	}

	public LaneAreaView getOpposite(LaneAreaView areaView) {
		StreetSectionView streetSectionView = (StreetSectionView) getParent();
		LaneArea model = areaView.getLaneArea();
		return streetSectionView.getViewForLane(model.getOpposite());
	}

}
