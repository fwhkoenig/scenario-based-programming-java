package cartox.ui.simulation;

import javax.swing.JLabel;

import cartox.Obstacle;

@SuppressWarnings("serial")
public class ObstacleView extends JLabel {

	private Obstacle obstacle;

	public ObstacleView(Obstacle obstacle) {
		this.obstacle = obstacle;
		setText(obstacle.getName());
	}

	public Obstacle getObstacle() {
		return obstacle;
	}

}
