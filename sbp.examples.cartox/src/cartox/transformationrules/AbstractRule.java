package cartox.transformationrules;

import cartox.Car;
import cartox.Environment;
import cartox.ObstacleControl;
import sbp.runtime.objectsystem.TransformationRule;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public abstract class AbstractRule extends TransformationRule {

	protected Role<Environment> env = new Role<Environment>(Environment.class, "env");
	protected Role<Car> car = new Role<Car>(Car.class, "car");
	protected Role<ObstacleControl> obstacleControl = new Role<ObstacleControl>(ObstacleControl.class,
			"obstacleControl");

	protected Message movedToNextArea = new Message(env, car, "movedToNextArea");
	protected Message changedToOppositeArea = new Message(env, car, "changedToOppositeArea");
	protected Message movedToNextAreaOnOvertakingLane = new Message(env, car,
			"movedToNextAreaOnOvertakingLane");
	protected Message register = new Message(car, obstacleControl, "register");
	protected Message enteringAllowed = new Message(obstacleControl, car, "enteringAllowed");
	protected Message unregister = new Message(car, obstacleControl, "unregister");

}
