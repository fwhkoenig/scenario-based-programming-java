package cartox.transformationrules;

import cartox.Car;
import cartox.Lane;
import cartox.LaneArea;
import cartox.Obstacle;
import sbp.specification.events.Message;

public class RuleForChangedToOppositeArea extends AbstractRule {

	@Override
	public Message getTriggerMessage() {
		return changedToOppositeArea;
	}

	@Override
	public void execute(Message event) {
		Car car = (Car) event.getReceiver().getBinding();
		LaneArea currentArea = car.getInArea();
		LaneArea oppositeArea = currentArea.getOpposite();
		Obstacle obstacle = oppositeArea.getObstacle();
		Lane oppositeLane = (Lane) oppositeArea.eContainer();
		if (obstacle != null) {
			throw new RuntimeException("Car " + car.getName() + " crashed into obstacle " + obstacle.getName());
		}
		car.setInArea(oppositeArea);
		car.setOnLane(oppositeLane);
	}

}
