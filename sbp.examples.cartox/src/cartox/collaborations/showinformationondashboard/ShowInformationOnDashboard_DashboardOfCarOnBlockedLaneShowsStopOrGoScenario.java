package cartox.collaborations.showinformationondashboard;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

import java.util.List;
import java.util.ArrayList;

// Collaboration
import cartox.collaborations.showinformationondashboard.ShowInformationOnDashboardCollaboration;

// Roles
import cartox.Driver;
import cartox.Dashboard;

@SuppressWarnings("serial")
public class ShowInformationOnDashboard_DashboardOfCarOnBlockedLaneShowsStopOrGoScenario extends ShowInformationOnDashboardCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "setApproachingObstacle", 
		RANDOM
		);
		setBlocked(car, dashboard, "showGo");
		setBlocked(car, dashboard, "showStop");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "setApproachingObstacle", 
		RANDOM
		));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(driver, (Driver) car
		.getBinding().getDriver()
		);
		bindRoleToObject(dashboard, (Dashboard) car
		.getBinding().getDashboard()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		// Begin Alternative
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		if (true) {
		waitedForMessages.add(new Message(STRICT, car, dashboard, "showGo"));
		//true
		}
		if (true) {
		waitedForMessages.add(new Message(STRICT, car, dashboard, "showStop"));
		//true
		}
		// Insert into BP
		doStep(requestedMessages, waitedForMessages);
		// Determine which path has been chosen
		if (getLastMessage().equals(new Message(car, dashboard, "showGo"))) {
			//true
		}else
		if (getLastMessage().equals(new Message(car, dashboard, "showStop"))) {
			//true
		}
		// End Alternative
	}

	

}
