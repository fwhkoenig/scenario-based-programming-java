package cartox.collaborations.showinformationondashboard;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.showinformationondashboard.ShowInformationOnDashboardCollaboration;

// Roles
import cartox.Dashboard;

@SuppressWarnings("serial")
public class ShowInformationOnDashboard_DashboardShowsStopScenario extends ShowInformationOnDashboardCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(obstacleControl, car, "enteringDisallowed");
		setBlocked(car, dashboard, "showStop");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(obstacleControl, car, "enteringDisallowed"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(dashboard, (Dashboard) car
		.getBinding().getDashboard()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		request(STRICT, car, dashboard, "showStop");
	}

	

}
