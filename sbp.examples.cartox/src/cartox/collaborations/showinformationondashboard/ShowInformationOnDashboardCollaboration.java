package cartox.collaborations.showinformationondashboard;

// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
import cartox.Environment;
import cartox.Car;
import cartox.Driver;
import cartox.Dashboard;
import cartox.ObstacleControl;

@SuppressWarnings("serial")
public abstract class ShowInformationOnDashboardCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<Car> car = createRole(Car.class, "car");
	protected Role<Driver> driver = createRole(Driver.class, "driver");
	protected Role<Dashboard> dashboard = createRole(Dashboard.class, "dashboard");
	protected Role<ObstacleControl> obstacleControl = createRole(ObstacleControl.class, "obstacleControl");
	
}