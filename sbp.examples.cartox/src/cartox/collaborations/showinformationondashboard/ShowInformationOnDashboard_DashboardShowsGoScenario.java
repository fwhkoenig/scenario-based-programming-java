package cartox.collaborations.showinformationondashboard;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.showinformationondashboard.ShowInformationOnDashboardCollaboration;

// Roles
import cartox.Dashboard;

@SuppressWarnings("serial")
public class ShowInformationOnDashboard_DashboardShowsGoScenario extends ShowInformationOnDashboardCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(obstacleControl, car, "enteringAllowed");
		setBlocked(car, dashboard, "showGo");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(obstacleControl, car, "enteringAllowed"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(dashboard, (Dashboard) car
		.getBinding().getDashboard()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		request(STRICT, car, dashboard, "showGo");
	}

	

}
