package cartox.collaborations.carapproachingobstacleassumptions;

// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
import cartox.Environment;
import cartox.Car;
import cartox.LaneArea;
import cartox.LaneArea;
import cartox.LaneArea;
import cartox.Obstacle;

@SuppressWarnings("serial")
public abstract class CarApproachingObstacleAssumptionsCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<Car> car = createRole(Car.class, "car");
	protected Role<LaneArea> currentArea = createRole(LaneArea.class, "currentArea");
	protected Role<LaneArea> nextArea = createRole(LaneArea.class, "nextArea");
	protected Role<LaneArea> oppositeNextArea = createRole(LaneArea.class, "oppositeNextArea");
	protected Role<Obstacle> obstacle = createRole(Obstacle.class, "obstacle");
	
}