package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.carsregisterwithobstaclecontrol.CarsRegisterWithObstacleControlCollaboration;

// Roles

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_CarEventuallyAllowedToPassObstacleScenario extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(obstacleControl, car, "enteringDisallowed");
		setBlocked(obstacleControl, car, "enteringAllowed");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(obstacleControl, car, "enteringDisallowed"));
	}
	
	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		//true
		waitFor(STRICT, obstacleControl, car, "enteringAllowed");
	}

	

}
