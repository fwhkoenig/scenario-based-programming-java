package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.carsregisterwithobstaclecontrol.CarsRegisterWithObstacleControlCollaboration;

// Roles
import cartox.Obstacle;
import cartox.ObstacleControl;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_CarUnregistersAfterLeavingNarrowPassageScenario extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "hasLeftNarrowPassage");
		setBlocked(car, obstacleControl, "unregister");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "hasLeftNarrowPassage"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(obstacle, (Obstacle) car
		.getBinding().getApproachingObstacle()
		);
		bindRoleToObject(obstacleControl, (ObstacleControl) obstacle
		.getBinding().getControlledBy()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		request(STRICT, car, obstacleControl, "unregister");
	}

	

}
