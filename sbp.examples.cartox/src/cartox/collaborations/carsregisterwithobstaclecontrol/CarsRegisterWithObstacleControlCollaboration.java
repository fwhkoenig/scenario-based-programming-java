package cartox.collaborations.carsregisterwithobstaclecontrol;

// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
import cartox.Environment;
import cartox.Car;
import cartox.Car;
import cartox.Dashboard;
import cartox.Obstacle;
import cartox.ObstacleControl;
import cartox.LaneArea;
import cartox.LaneArea;

@SuppressWarnings("serial")
public abstract class CarsRegisterWithObstacleControlCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<Car> car = createRole(Car.class, "car");
	protected Role<Car> nextCar = createRole(Car.class, "nextCar");
	protected Role<Dashboard> dashboard = createRole(Dashboard.class, "dashboard");
	protected Role<Obstacle> obstacle = createRole(Obstacle.class, "obstacle");
	protected Role<ObstacleControl> obstacleControl = createRole(ObstacleControl.class, "obstacleControl");
	protected Role<LaneArea> currentArea = createRole(LaneArea.class, "currentArea");
	protected Role<LaneArea> nextArea = createRole(LaneArea.class, "nextArea");
	
}