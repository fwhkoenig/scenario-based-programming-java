package cartox.collaborations.carentersorleavesnarrowpassageassumptions;

// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
import cartox.Environment;
import cartox.Car;
import cartox.LaneArea;
import cartox.LaneArea;
import cartox.LaneArea;

@SuppressWarnings("serial")
public abstract class CarEntersOrLeavesNarrowPassageAssumptionsCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<Car> car = createRole(Car.class, "car");
	protected Role<LaneArea> currentArea = createRole(LaneArea.class, "currentArea");
	protected Role<LaneArea> oppositeArea = createRole(LaneArea.class, "oppositeArea");
	protected Role<LaneArea> oppositePrevArea = createRole(LaneArea.class, "oppositePrevArea");
	
}