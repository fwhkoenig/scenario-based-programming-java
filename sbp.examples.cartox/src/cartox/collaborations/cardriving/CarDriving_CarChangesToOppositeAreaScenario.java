package cartox.collaborations.cardriving;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.cardriving.CarDrivingCollaboration;

// Roles

@SuppressWarnings("serial")
public class CarDriving_CarChangesToOppositeAreaScenario extends CarDrivingCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "changedToOppositeArea");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "changedToOppositeArea"));
	}
	
	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		//true
	}

	

}
