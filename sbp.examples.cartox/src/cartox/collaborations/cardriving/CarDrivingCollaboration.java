package cartox.collaborations.cardriving;

// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
import cartox.Environment;
import cartox.Car;

@SuppressWarnings("serial")
public abstract class CarDrivingCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<Car> car = createRole(Car.class, "car");
	
}