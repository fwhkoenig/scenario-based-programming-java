/**
 */
package cartox;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see cartox.CartoxFactory
 * @model kind="package"
 * @generated
 */
public interface CartoxPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "cartox";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://sbp.examples.cartox/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sbp.examples.cartox";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CartoxPackage eINSTANCE = cartox.impl.CartoxPackageImpl.init();

	/**
	 * The meta object id for the '{@link cartox.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.NamedElementImpl
	 * @see cartox.impl.CartoxPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link cartox.impl.CarImpl <em>Car</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.CarImpl
	 * @see cartox.impl.CartoxPackageImpl#getCar()
	 * @generated
	 */
	int CAR = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>In Area</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__IN_AREA = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>On Lane</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__ON_LANE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Approaching Obstacle</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__APPROACHING_OBSTACLE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Dashboard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__DASHBOARD = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Driver</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__DRIVER = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Driving In Direction Of Lane</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__DRIVING_IN_DIRECTION_OF_LANE = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR__ENVIRONMENT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Car</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Moved To Next Area</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___MOVED_TO_NEXT_AREA = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Changed To Opposite Area</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___CHANGED_TO_OPPOSITE_AREA = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Moved To Next Area On Overtaking Lane</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___MOVED_TO_NEXT_AREA_ON_OVERTAKING_LANE = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Has Entered Narrow Passage</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___HAS_ENTERED_NARROW_PASSAGE = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Has Left Narrow Passage</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___HAS_LEFT_NARROW_PASSAGE = NAMED_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Obstacle Reached</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___OBSTACLE_REACHED = NAMED_ELEMENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Entering Allowed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___ENTERING_ALLOWED = NAMED_ELEMENT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Entering Disallowed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR___ENTERING_DISALLOWED = NAMED_ELEMENT_OPERATION_COUNT + 7;

	/**
	 * The number of operations of the '<em>Car</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 8;

	/**
	 * The meta object id for the '{@link cartox.impl.EnvironmentImpl <em>Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.EnvironmentImpl
	 * @see cartox.impl.CartoxPackageImpl#getEnvironment()
	 * @generated
	 */
	int ENVIRONMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.CarToXImpl <em>Car To X</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.CarToXImpl
	 * @see cartox.impl.CartoxPackageImpl#getCarToX()
	 * @generated
	 */
	int CAR_TO_X = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_TO_X__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Cars</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_TO_X__CARS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_TO_X__ENVIRONMENT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Street Sections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_TO_X__STREET_SECTIONS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Obstacle Controls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_TO_X__OBSTACLE_CONTROLS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Car To X</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_TO_X_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Car To X</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAR_TO_X_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.StreetSectionImpl <em>Street Section</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.StreetSectionImpl
	 * @see cartox.impl.CartoxPackageImpl#getStreetSection()
	 * @generated
	 */
	int STREET_SECTION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STREET_SECTION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Lanes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STREET_SECTION__LANES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Obstacles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STREET_SECTION__OBSTACLES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Street Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STREET_SECTION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Street Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STREET_SECTION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.LaneImpl <em>Lane</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.LaneImpl
	 * @see cartox.impl.CartoxPackageImpl#getLane()
	 * @generated
	 */
	int LANE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Lane Areas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE__LANE_AREAS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Lane</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Lane</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.LaneAreaImpl <em>Lane Area</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.LaneAreaImpl
	 * @see cartox.impl.CartoxPackageImpl#getLaneArea()
	 * @generated
	 */
	int LANE_AREA = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA__OPPOSITE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Previous</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA__PREVIOUS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA__NEXT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Obstacle</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA__OBSTACLE = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Lane Area</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Lane Area</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_AREA_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.ObstacleImpl <em>Obstacle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.ObstacleImpl
	 * @see cartox.impl.CartoxPackageImpl#getObstacle()
	 * @generated
	 */
	int OBSTACLE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Obstacle Area</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__OBSTACLE_AREA = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Controlled By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__CONTROLLED_BY = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Obstacle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Obstacle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.ObstacleBlockingOneLaneImpl <em>Obstacle Blocking One Lane</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.ObstacleBlockingOneLaneImpl
	 * @see cartox.impl.CartoxPackageImpl#getObstacleBlockingOneLane()
	 * @generated
	 */
	int OBSTACLE_BLOCKING_ONE_LANE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_BLOCKING_ONE_LANE__NAME = OBSTACLE__NAME;

	/**
	 * The feature id for the '<em><b>Obstacle Area</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_BLOCKING_ONE_LANE__OBSTACLE_AREA = OBSTACLE__OBSTACLE_AREA;

	/**
	 * The feature id for the '<em><b>Controlled By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_BLOCKING_ONE_LANE__CONTROLLED_BY = OBSTACLE__CONTROLLED_BY;

	/**
	 * The number of structural features of the '<em>Obstacle Blocking One Lane</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_BLOCKING_ONE_LANE_FEATURE_COUNT = OBSTACLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Obstacle Blocking One Lane</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_BLOCKING_ONE_LANE_OPERATION_COUNT = OBSTACLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.ObstacleControlImpl <em>Obstacle Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.ObstacleControlImpl
	 * @see cartox.impl.CartoxPackageImpl#getObstacleControl()
	 * @generated
	 */
	int OBSTACLE_CONTROL = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Controlled Obstacle</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL__CONTROLLED_OBSTACLE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cars On Narrow Passage Lane Allowed To Pass</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Registered Cars Waiting</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL__REGISTERED_CARS_WAITING = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Obstacle Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Register</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL___REGISTER = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Unregister</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL___UNREGISTER = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Obstacle Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_CONTROL_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link cartox.impl.DriverImpl <em>Driver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.DriverImpl
	 * @see cartox.impl.CartoxPackageImpl#getDriver()
	 * @generated
	 */
	int DRIVER = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link cartox.impl.DashboardImpl <em>Dashboard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cartox.impl.DashboardImpl
	 * @see cartox.impl.CartoxPackageImpl#getDashboard()
	 * @generated
	 */
	int DASHBOARD = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Dashboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Show Go</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD___SHOW_GO = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Show Stop</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD___SHOW_STOP = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dashboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASHBOARD_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link cartox.Car <em>Car</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Car</em>'.
	 * @see cartox.Car
	 * @generated
	 */
	EClass getCar();

	/**
	 * Returns the meta object for the reference '{@link cartox.Car#getInArea <em>In Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>In Area</em>'.
	 * @see cartox.Car#getInArea()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_InArea();

	/**
	 * Returns the meta object for the reference '{@link cartox.Car#getOnLane <em>On Lane</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Lane</em>'.
	 * @see cartox.Car#getOnLane()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_OnLane();

	/**
	 * Returns the meta object for the reference '{@link cartox.Car#getApproachingObstacle <em>Approaching Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Approaching Obstacle</em>'.
	 * @see cartox.Car#getApproachingObstacle()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_ApproachingObstacle();

	/**
	 * Returns the meta object for the containment reference '{@link cartox.Car#getDashboard <em>Dashboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dashboard</em>'.
	 * @see cartox.Car#getDashboard()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_Dashboard();

	/**
	 * Returns the meta object for the containment reference '{@link cartox.Car#getDriver <em>Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Driver</em>'.
	 * @see cartox.Car#getDriver()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_Driver();

	/**
	 * Returns the meta object for the reference '{@link cartox.Car#getDrivingInDirectionOfLane <em>Driving In Direction Of Lane</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Driving In Direction Of Lane</em>'.
	 * @see cartox.Car#getDrivingInDirectionOfLane()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_DrivingInDirectionOfLane();

	/**
	 * Returns the meta object for the reference '{@link cartox.Car#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Environment</em>'.
	 * @see cartox.Car#getEnvironment()
	 * @see #getCar()
	 * @generated
	 */
	EReference getCar_Environment();

	/**
	 * Returns the meta object for the '{@link cartox.Car#movedToNextArea() <em>Moved To Next Area</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Moved To Next Area</em>' operation.
	 * @see cartox.Car#movedToNextArea()
	 * @generated
	 */
	EOperation getCar__MovedToNextArea();

	/**
	 * Returns the meta object for the '{@link cartox.Car#changedToOppositeArea() <em>Changed To Opposite Area</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Changed To Opposite Area</em>' operation.
	 * @see cartox.Car#changedToOppositeArea()
	 * @generated
	 */
	EOperation getCar__ChangedToOppositeArea();

	/**
	 * Returns the meta object for the '{@link cartox.Car#movedToNextAreaOnOvertakingLane() <em>Moved To Next Area On Overtaking Lane</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Moved To Next Area On Overtaking Lane</em>' operation.
	 * @see cartox.Car#movedToNextAreaOnOvertakingLane()
	 * @generated
	 */
	EOperation getCar__MovedToNextAreaOnOvertakingLane();

	/**
	 * Returns the meta object for the '{@link cartox.Car#hasEnteredNarrowPassage() <em>Has Entered Narrow Passage</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Has Entered Narrow Passage</em>' operation.
	 * @see cartox.Car#hasEnteredNarrowPassage()
	 * @generated
	 */
	EOperation getCar__HasEnteredNarrowPassage();

	/**
	 * Returns the meta object for the '{@link cartox.Car#hasLeftNarrowPassage() <em>Has Left Narrow Passage</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Has Left Narrow Passage</em>' operation.
	 * @see cartox.Car#hasLeftNarrowPassage()
	 * @generated
	 */
	EOperation getCar__HasLeftNarrowPassage();

	/**
	 * Returns the meta object for the '{@link cartox.Car#obstacleReached() <em>Obstacle Reached</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Obstacle Reached</em>' operation.
	 * @see cartox.Car#obstacleReached()
	 * @generated
	 */
	EOperation getCar__ObstacleReached();

	/**
	 * Returns the meta object for the '{@link cartox.Car#enteringAllowed() <em>Entering Allowed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Entering Allowed</em>' operation.
	 * @see cartox.Car#enteringAllowed()
	 * @generated
	 */
	EOperation getCar__EnteringAllowed();

	/**
	 * Returns the meta object for the '{@link cartox.Car#enteringDisallowed() <em>Entering Disallowed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Entering Disallowed</em>' operation.
	 * @see cartox.Car#enteringDisallowed()
	 * @generated
	 */
	EOperation getCar__EnteringDisallowed();

	/**
	 * Returns the meta object for class '{@link cartox.Environment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment</em>'.
	 * @see cartox.Environment
	 * @generated
	 */
	EClass getEnvironment();

	/**
	 * Returns the meta object for class '{@link cartox.CarToX <em>Car To X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Car To X</em>'.
	 * @see cartox.CarToX
	 * @generated
	 */
	EClass getCarToX();

	/**
	 * Returns the meta object for the containment reference list '{@link cartox.CarToX#getCars <em>Cars</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cars</em>'.
	 * @see cartox.CarToX#getCars()
	 * @see #getCarToX()
	 * @generated
	 */
	EReference getCarToX_Cars();

	/**
	 * Returns the meta object for the containment reference '{@link cartox.CarToX#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Environment</em>'.
	 * @see cartox.CarToX#getEnvironment()
	 * @see #getCarToX()
	 * @generated
	 */
	EReference getCarToX_Environment();

	/**
	 * Returns the meta object for the containment reference list '{@link cartox.CarToX#getStreetSections <em>Street Sections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Street Sections</em>'.
	 * @see cartox.CarToX#getStreetSections()
	 * @see #getCarToX()
	 * @generated
	 */
	EReference getCarToX_StreetSections();

	/**
	 * Returns the meta object for the containment reference list '{@link cartox.CarToX#getObstacleControls <em>Obstacle Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Obstacle Controls</em>'.
	 * @see cartox.CarToX#getObstacleControls()
	 * @see #getCarToX()
	 * @generated
	 */
	EReference getCarToX_ObstacleControls();

	/**
	 * Returns the meta object for class '{@link cartox.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see cartox.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link cartox.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see cartox.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link cartox.StreetSection <em>Street Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Street Section</em>'.
	 * @see cartox.StreetSection
	 * @generated
	 */
	EClass getStreetSection();

	/**
	 * Returns the meta object for the containment reference list '{@link cartox.StreetSection#getLanes <em>Lanes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lanes</em>'.
	 * @see cartox.StreetSection#getLanes()
	 * @see #getStreetSection()
	 * @generated
	 */
	EReference getStreetSection_Lanes();

	/**
	 * Returns the meta object for the containment reference list '{@link cartox.StreetSection#getObstacles <em>Obstacles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Obstacles</em>'.
	 * @see cartox.StreetSection#getObstacles()
	 * @see #getStreetSection()
	 * @generated
	 */
	EReference getStreetSection_Obstacles();

	/**
	 * Returns the meta object for class '{@link cartox.Lane <em>Lane</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lane</em>'.
	 * @see cartox.Lane
	 * @generated
	 */
	EClass getLane();

	/**
	 * Returns the meta object for the containment reference list '{@link cartox.Lane#getLaneAreas <em>Lane Areas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lane Areas</em>'.
	 * @see cartox.Lane#getLaneAreas()
	 * @see #getLane()
	 * @generated
	 */
	EReference getLane_LaneAreas();

	/**
	 * Returns the meta object for class '{@link cartox.LaneArea <em>Lane Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lane Area</em>'.
	 * @see cartox.LaneArea
	 * @generated
	 */
	EClass getLaneArea();

	/**
	 * Returns the meta object for the reference '{@link cartox.LaneArea#getOpposite <em>Opposite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite</em>'.
	 * @see cartox.LaneArea#getOpposite()
	 * @see #getLaneArea()
	 * @generated
	 */
	EReference getLaneArea_Opposite();

	/**
	 * Returns the meta object for the reference '{@link cartox.LaneArea#getPrevious <em>Previous</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous</em>'.
	 * @see cartox.LaneArea#getPrevious()
	 * @see #getLaneArea()
	 * @generated
	 */
	EReference getLaneArea_Previous();

	/**
	 * Returns the meta object for the reference '{@link cartox.LaneArea#getNext <em>Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next</em>'.
	 * @see cartox.LaneArea#getNext()
	 * @see #getLaneArea()
	 * @generated
	 */
	EReference getLaneArea_Next();

	/**
	 * Returns the meta object for the reference '{@link cartox.LaneArea#getObstacle <em>Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Obstacle</em>'.
	 * @see cartox.LaneArea#getObstacle()
	 * @see #getLaneArea()
	 * @generated
	 */
	EReference getLaneArea_Obstacle();

	/**
	 * Returns the meta object for class '{@link cartox.Obstacle <em>Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obstacle</em>'.
	 * @see cartox.Obstacle
	 * @generated
	 */
	EClass getObstacle();

	/**
	 * Returns the meta object for the reference '{@link cartox.Obstacle#getObstacleArea <em>Obstacle Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Obstacle Area</em>'.
	 * @see cartox.Obstacle#getObstacleArea()
	 * @see #getObstacle()
	 * @generated
	 */
	EReference getObstacle_ObstacleArea();

	/**
	 * Returns the meta object for the reference '{@link cartox.Obstacle#getControlledBy <em>Controlled By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Controlled By</em>'.
	 * @see cartox.Obstacle#getControlledBy()
	 * @see #getObstacle()
	 * @generated
	 */
	EReference getObstacle_ControlledBy();

	/**
	 * Returns the meta object for class '{@link cartox.ObstacleBlockingOneLane <em>Obstacle Blocking One Lane</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obstacle Blocking One Lane</em>'.
	 * @see cartox.ObstacleBlockingOneLane
	 * @generated
	 */
	EClass getObstacleBlockingOneLane();

	/**
	 * Returns the meta object for class '{@link cartox.ObstacleControl <em>Obstacle Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obstacle Control</em>'.
	 * @see cartox.ObstacleControl
	 * @generated
	 */
	EClass getObstacleControl();

	/**
	 * Returns the meta object for the reference '{@link cartox.ObstacleControl#getControlledObstacle <em>Controlled Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Controlled Obstacle</em>'.
	 * @see cartox.ObstacleControl#getControlledObstacle()
	 * @see #getObstacleControl()
	 * @generated
	 */
	EReference getObstacleControl_ControlledObstacle();

	/**
	 * Returns the meta object for the reference list '{@link cartox.ObstacleControl#getCarsOnNarrowPassageLaneAllowedToPass <em>Cars On Narrow Passage Lane Allowed To Pass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Cars On Narrow Passage Lane Allowed To Pass</em>'.
	 * @see cartox.ObstacleControl#getCarsOnNarrowPassageLaneAllowedToPass()
	 * @see #getObstacleControl()
	 * @generated
	 */
	EReference getObstacleControl_CarsOnNarrowPassageLaneAllowedToPass();

	/**
	 * Returns the meta object for the reference list '{@link cartox.ObstacleControl#getRegisteredCarsWaiting <em>Registered Cars Waiting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Registered Cars Waiting</em>'.
	 * @see cartox.ObstacleControl#getRegisteredCarsWaiting()
	 * @see #getObstacleControl()
	 * @generated
	 */
	EReference getObstacleControl_RegisteredCarsWaiting();

	/**
	 * Returns the meta object for the '{@link cartox.ObstacleControl#register() <em>Register</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Register</em>' operation.
	 * @see cartox.ObstacleControl#register()
	 * @generated
	 */
	EOperation getObstacleControl__Register();

	/**
	 * Returns the meta object for the '{@link cartox.ObstacleControl#unregister() <em>Unregister</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unregister</em>' operation.
	 * @see cartox.ObstacleControl#unregister()
	 * @generated
	 */
	EOperation getObstacleControl__Unregister();

	/**
	 * Returns the meta object for class '{@link cartox.Driver <em>Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Driver</em>'.
	 * @see cartox.Driver
	 * @generated
	 */
	EClass getDriver();

	/**
	 * Returns the meta object for class '{@link cartox.Dashboard <em>Dashboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dashboard</em>'.
	 * @see cartox.Dashboard
	 * @generated
	 */
	EClass getDashboard();

	/**
	 * Returns the meta object for the '{@link cartox.Dashboard#showGo() <em>Show Go</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Show Go</em>' operation.
	 * @see cartox.Dashboard#showGo()
	 * @generated
	 */
	EOperation getDashboard__ShowGo();

	/**
	 * Returns the meta object for the '{@link cartox.Dashboard#showStop() <em>Show Stop</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Show Stop</em>' operation.
	 * @see cartox.Dashboard#showStop()
	 * @generated
	 */
	EOperation getDashboard__ShowStop();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CartoxFactory getCartoxFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link cartox.impl.CarImpl <em>Car</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.CarImpl
		 * @see cartox.impl.CartoxPackageImpl#getCar()
		 * @generated
		 */
		EClass CAR = eINSTANCE.getCar();

		/**
		 * The meta object literal for the '<em><b>In Area</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__IN_AREA = eINSTANCE.getCar_InArea();

		/**
		 * The meta object literal for the '<em><b>On Lane</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__ON_LANE = eINSTANCE.getCar_OnLane();

		/**
		 * The meta object literal for the '<em><b>Approaching Obstacle</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__APPROACHING_OBSTACLE = eINSTANCE.getCar_ApproachingObstacle();

		/**
		 * The meta object literal for the '<em><b>Dashboard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__DASHBOARD = eINSTANCE.getCar_Dashboard();

		/**
		 * The meta object literal for the '<em><b>Driver</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__DRIVER = eINSTANCE.getCar_Driver();

		/**
		 * The meta object literal for the '<em><b>Driving In Direction Of Lane</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__DRIVING_IN_DIRECTION_OF_LANE = eINSTANCE.getCar_DrivingInDirectionOfLane();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR__ENVIRONMENT = eINSTANCE.getCar_Environment();

		/**
		 * The meta object literal for the '<em><b>Moved To Next Area</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___MOVED_TO_NEXT_AREA = eINSTANCE.getCar__MovedToNextArea();

		/**
		 * The meta object literal for the '<em><b>Changed To Opposite Area</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___CHANGED_TO_OPPOSITE_AREA = eINSTANCE.getCar__ChangedToOppositeArea();

		/**
		 * The meta object literal for the '<em><b>Moved To Next Area On Overtaking Lane</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___MOVED_TO_NEXT_AREA_ON_OVERTAKING_LANE = eINSTANCE.getCar__MovedToNextAreaOnOvertakingLane();

		/**
		 * The meta object literal for the '<em><b>Has Entered Narrow Passage</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___HAS_ENTERED_NARROW_PASSAGE = eINSTANCE.getCar__HasEnteredNarrowPassage();

		/**
		 * The meta object literal for the '<em><b>Has Left Narrow Passage</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___HAS_LEFT_NARROW_PASSAGE = eINSTANCE.getCar__HasLeftNarrowPassage();

		/**
		 * The meta object literal for the '<em><b>Obstacle Reached</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___OBSTACLE_REACHED = eINSTANCE.getCar__ObstacleReached();

		/**
		 * The meta object literal for the '<em><b>Entering Allowed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___ENTERING_ALLOWED = eINSTANCE.getCar__EnteringAllowed();

		/**
		 * The meta object literal for the '<em><b>Entering Disallowed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CAR___ENTERING_DISALLOWED = eINSTANCE.getCar__EnteringDisallowed();

		/**
		 * The meta object literal for the '{@link cartox.impl.EnvironmentImpl <em>Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.EnvironmentImpl
		 * @see cartox.impl.CartoxPackageImpl#getEnvironment()
		 * @generated
		 */
		EClass ENVIRONMENT = eINSTANCE.getEnvironment();

		/**
		 * The meta object literal for the '{@link cartox.impl.CarToXImpl <em>Car To X</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.CarToXImpl
		 * @see cartox.impl.CartoxPackageImpl#getCarToX()
		 * @generated
		 */
		EClass CAR_TO_X = eINSTANCE.getCarToX();

		/**
		 * The meta object literal for the '<em><b>Cars</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR_TO_X__CARS = eINSTANCE.getCarToX_Cars();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR_TO_X__ENVIRONMENT = eINSTANCE.getCarToX_Environment();

		/**
		 * The meta object literal for the '<em><b>Street Sections</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR_TO_X__STREET_SECTIONS = eINSTANCE.getCarToX_StreetSections();

		/**
		 * The meta object literal for the '<em><b>Obstacle Controls</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAR_TO_X__OBSTACLE_CONTROLS = eINSTANCE.getCarToX_ObstacleControls();

		/**
		 * The meta object literal for the '{@link cartox.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.NamedElementImpl
		 * @see cartox.impl.CartoxPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link cartox.impl.StreetSectionImpl <em>Street Section</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.StreetSectionImpl
		 * @see cartox.impl.CartoxPackageImpl#getStreetSection()
		 * @generated
		 */
		EClass STREET_SECTION = eINSTANCE.getStreetSection();

		/**
		 * The meta object literal for the '<em><b>Lanes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STREET_SECTION__LANES = eINSTANCE.getStreetSection_Lanes();

		/**
		 * The meta object literal for the '<em><b>Obstacles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STREET_SECTION__OBSTACLES = eINSTANCE.getStreetSection_Obstacles();

		/**
		 * The meta object literal for the '{@link cartox.impl.LaneImpl <em>Lane</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.LaneImpl
		 * @see cartox.impl.CartoxPackageImpl#getLane()
		 * @generated
		 */
		EClass LANE = eINSTANCE.getLane();

		/**
		 * The meta object literal for the '<em><b>Lane Areas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANE__LANE_AREAS = eINSTANCE.getLane_LaneAreas();

		/**
		 * The meta object literal for the '{@link cartox.impl.LaneAreaImpl <em>Lane Area</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.LaneAreaImpl
		 * @see cartox.impl.CartoxPackageImpl#getLaneArea()
		 * @generated
		 */
		EClass LANE_AREA = eINSTANCE.getLaneArea();

		/**
		 * The meta object literal for the '<em><b>Opposite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANE_AREA__OPPOSITE = eINSTANCE.getLaneArea_Opposite();

		/**
		 * The meta object literal for the '<em><b>Previous</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANE_AREA__PREVIOUS = eINSTANCE.getLaneArea_Previous();

		/**
		 * The meta object literal for the '<em><b>Next</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANE_AREA__NEXT = eINSTANCE.getLaneArea_Next();

		/**
		 * The meta object literal for the '<em><b>Obstacle</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANE_AREA__OBSTACLE = eINSTANCE.getLaneArea_Obstacle();

		/**
		 * The meta object literal for the '{@link cartox.impl.ObstacleImpl <em>Obstacle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.ObstacleImpl
		 * @see cartox.impl.CartoxPackageImpl#getObstacle()
		 * @generated
		 */
		EClass OBSTACLE = eINSTANCE.getObstacle();

		/**
		 * The meta object literal for the '<em><b>Obstacle Area</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTACLE__OBSTACLE_AREA = eINSTANCE.getObstacle_ObstacleArea();

		/**
		 * The meta object literal for the '<em><b>Controlled By</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTACLE__CONTROLLED_BY = eINSTANCE.getObstacle_ControlledBy();

		/**
		 * The meta object literal for the '{@link cartox.impl.ObstacleBlockingOneLaneImpl <em>Obstacle Blocking One Lane</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.ObstacleBlockingOneLaneImpl
		 * @see cartox.impl.CartoxPackageImpl#getObstacleBlockingOneLane()
		 * @generated
		 */
		EClass OBSTACLE_BLOCKING_ONE_LANE = eINSTANCE.getObstacleBlockingOneLane();

		/**
		 * The meta object literal for the '{@link cartox.impl.ObstacleControlImpl <em>Obstacle Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.ObstacleControlImpl
		 * @see cartox.impl.CartoxPackageImpl#getObstacleControl()
		 * @generated
		 */
		EClass OBSTACLE_CONTROL = eINSTANCE.getObstacleControl();

		/**
		 * The meta object literal for the '<em><b>Controlled Obstacle</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTACLE_CONTROL__CONTROLLED_OBSTACLE = eINSTANCE.getObstacleControl_ControlledObstacle();

		/**
		 * The meta object literal for the '<em><b>Cars On Narrow Passage Lane Allowed To Pass</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS = eINSTANCE.getObstacleControl_CarsOnNarrowPassageLaneAllowedToPass();

		/**
		 * The meta object literal for the '<em><b>Registered Cars Waiting</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTACLE_CONTROL__REGISTERED_CARS_WAITING = eINSTANCE.getObstacleControl_RegisteredCarsWaiting();

		/**
		 * The meta object literal for the '<em><b>Register</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSTACLE_CONTROL___REGISTER = eINSTANCE.getObstacleControl__Register();

		/**
		 * The meta object literal for the '<em><b>Unregister</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSTACLE_CONTROL___UNREGISTER = eINSTANCE.getObstacleControl__Unregister();

		/**
		 * The meta object literal for the '{@link cartox.impl.DriverImpl <em>Driver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.DriverImpl
		 * @see cartox.impl.CartoxPackageImpl#getDriver()
		 * @generated
		 */
		EClass DRIVER = eINSTANCE.getDriver();

		/**
		 * The meta object literal for the '{@link cartox.impl.DashboardImpl <em>Dashboard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cartox.impl.DashboardImpl
		 * @see cartox.impl.CartoxPackageImpl#getDashboard()
		 * @generated
		 */
		EClass DASHBOARD = eINSTANCE.getDashboard();

		/**
		 * The meta object literal for the '<em><b>Show Go</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DASHBOARD___SHOW_GO = eINSTANCE.getDashboard__ShowGo();

		/**
		 * The meta object literal for the '<em><b>Show Stop</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DASHBOARD___SHOW_STOP = eINSTANCE.getDashboard__ShowStop();

	}

} //CartoxPackage
