package cartox.runtime;

import cartox.runtime.observers.DashboardObserver;
import cartox.specification.CarToXSpecification;
import cartox.ui.simulation.DashboardFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.DistributedRuntimeAdapter;
import sbp.runtime.settings.Settings;

public class CarToXRunconfig_PC_localhost_Dashboard_9001 extends SpecificationRunconfig<CarToXSpecification> {

	private DashboardFrame frame;
	private DashboardObserver dashboardObserverForJamesCar;

	public CarToXRunconfig_PC_localhost_Dashboard_9001() {
		super(new CarToXSpecification());
		enableDistributedExecutionMode(9001);
		frame = new DashboardFrame("Dashboard of JamesCar");
		dashboardObserverForJamesCar.setLabel(frame.getLabel());
		frame.setVisible(true);
		Settings.setRuntimeOut(frame.getRuntimeLog());
		Settings.setServerOut(frame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerObject(objectSystem.CarToX, UNCONTROLLABLE); // CarToX
		registerObject(objectSystem.CarToX.getCars().get(0), UNCONTROLLABLE); // JamesCar
		registerObject(objectSystem.CarToX.getCars().get(0).getDashboard(), CONTROLLABLE); // JamesCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(0).getDriver(), UNCONTROLLABLE); // James
		registerObject(objectSystem.CarToX.getEnvironment(), UNCONTROLLABLE); // env
		registerObject(objectSystem.CarToX.getStreetSections().get(0), UNCONTROLLABLE); // section1
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0), UNCONTROLLABLE); // lane1
																											// //
																											// TODO:
																											// Check
																											// subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(0),
				UNCONTROLLABLE); // lane1_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(1),
				UNCONTROLLABLE); // lane1_area2_beforeObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(2),
				UNCONTROLLABLE); // lane1_area3_Obstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(3),
				UNCONTROLLABLE); // lane1_area4_afterObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(4),
				UNCONTROLLABLE); // lane1_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1), UNCONTROLLABLE); // lane2
																											// //
																											// TODO:
																											// Check
																											// subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(0),
				UNCONTROLLABLE); // lane2_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(1),
				UNCONTROLLABLE); // lane2_area2_beforeNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(2),
				UNCONTROLLABLE); // lane2_area3_NarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(3),
				UNCONTROLLABLE); // lane2_area4_afterNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(4),
				UNCONTROLLABLE); // lane2_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getObstacles().get(0), UNCONTROLLABLE); // obstacle
																												// //
																												// TODO:
																												// Check
																												// subclasses
		registerObject(objectSystem.CarToX.getObstacleControls().get(0), UNCONTROLLABLE); // obstacleControl
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		String JAMESCAR_HOSTNAME = "localhost";
		int JAMESCAR_PORT = 9000;
		registerAddress(objectSystem.CarToX.getCars().get(0), JAMESCAR_HOSTNAME, JAMESCAR_PORT);
	}

	@Override
	protected void registerObservers() {
		// Add Observers here.
		dashboardObserverForJamesCar = new DashboardObserver();
		addScenarioObserver(dashboardObserverForJamesCar);
	}

	public static void main(String[] args) {
		SpecificationRunconfig.run(CarToXRunconfig_PC_localhost_Dashboard_9001.class);
	}

}
