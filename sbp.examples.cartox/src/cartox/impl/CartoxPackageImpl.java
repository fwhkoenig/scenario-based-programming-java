/**
 */
package cartox.impl;

import cartox.Car;
import cartox.CarToX;
import cartox.CartoxFactory;
import cartox.CartoxPackage;
import cartox.Dashboard;
import cartox.Driver;
import cartox.Environment;
import cartox.Lane;
import cartox.LaneArea;
import cartox.NamedElement;
import cartox.Obstacle;
import cartox.ObstacleBlockingOneLane;
import cartox.ObstacleControl;
import cartox.StreetSection;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CartoxPackageImpl extends EPackageImpl implements CartoxPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass carEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass carToXEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass streetSectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass laneEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass laneAreaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass obstacleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass obstacleBlockingOneLaneEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass obstacleControlEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass driverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dashboardEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see cartox.CartoxPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CartoxPackageImpl() {
		super(eNS_URI, CartoxFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CartoxPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CartoxPackage init() {
		if (isInited) return (CartoxPackage)EPackage.Registry.INSTANCE.getEPackage(CartoxPackage.eNS_URI);

		// Obtain or create and register package
		CartoxPackageImpl theCartoxPackage = (CartoxPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CartoxPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CartoxPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theCartoxPackage.createPackageContents();

		// Initialize created meta-data
		theCartoxPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCartoxPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CartoxPackage.eNS_URI, theCartoxPackage);
		return theCartoxPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCar() {
		return carEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCar_InArea() {
		return (EReference)carEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCar_OnLane() {
		return (EReference)carEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCar_ApproachingObstacle() {
		return (EReference)carEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCar_Dashboard() {
		return (EReference)carEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCar_Driver() {
		return (EReference)carEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCar_DrivingInDirectionOfLane() {
		return (EReference)carEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCar_Environment() {
		return (EReference)carEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__MovedToNextArea() {
		return carEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__ChangedToOppositeArea() {
		return carEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__MovedToNextAreaOnOvertakingLane() {
		return carEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__HasEnteredNarrowPassage() {
		return carEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__HasLeftNarrowPassage() {
		return carEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__ObstacleReached() {
		return carEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__EnteringAllowed() {
		return carEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCar__EnteringDisallowed() {
		return carEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironment() {
		return environmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCarToX() {
		return carToXEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCarToX_Cars() {
		return (EReference)carToXEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCarToX_Environment() {
		return (EReference)carToXEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCarToX_StreetSections() {
		return (EReference)carToXEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCarToX_ObstacleControls() {
		return (EReference)carToXEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStreetSection() {
		return streetSectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStreetSection_Lanes() {
		return (EReference)streetSectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStreetSection_Obstacles() {
		return (EReference)streetSectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLane() {
		return laneEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLane_LaneAreas() {
		return (EReference)laneEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLaneArea() {
		return laneAreaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLaneArea_Opposite() {
		return (EReference)laneAreaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLaneArea_Previous() {
		return (EReference)laneAreaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLaneArea_Next() {
		return (EReference)laneAreaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLaneArea_Obstacle() {
		return (EReference)laneAreaEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObstacle() {
		return obstacleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstacle_ObstacleArea() {
		return (EReference)obstacleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstacle_ControlledBy() {
		return (EReference)obstacleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObstacleBlockingOneLane() {
		return obstacleBlockingOneLaneEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObstacleControl() {
		return obstacleControlEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstacleControl_ControlledObstacle() {
		return (EReference)obstacleControlEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstacleControl_CarsOnNarrowPassageLaneAllowedToPass() {
		return (EReference)obstacleControlEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstacleControl_RegisteredCarsWaiting() {
		return (EReference)obstacleControlEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObstacleControl__Register() {
		return obstacleControlEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObstacleControl__Unregister() {
		return obstacleControlEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDriver() {
		return driverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDashboard() {
		return dashboardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDashboard__ShowGo() {
		return dashboardEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDashboard__ShowStop() {
		return dashboardEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CartoxFactory getCartoxFactory() {
		return (CartoxFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		carEClass = createEClass(CAR);
		createEReference(carEClass, CAR__IN_AREA);
		createEReference(carEClass, CAR__ON_LANE);
		createEReference(carEClass, CAR__APPROACHING_OBSTACLE);
		createEReference(carEClass, CAR__DASHBOARD);
		createEReference(carEClass, CAR__DRIVER);
		createEReference(carEClass, CAR__DRIVING_IN_DIRECTION_OF_LANE);
		createEReference(carEClass, CAR__ENVIRONMENT);
		createEOperation(carEClass, CAR___MOVED_TO_NEXT_AREA);
		createEOperation(carEClass, CAR___CHANGED_TO_OPPOSITE_AREA);
		createEOperation(carEClass, CAR___MOVED_TO_NEXT_AREA_ON_OVERTAKING_LANE);
		createEOperation(carEClass, CAR___HAS_ENTERED_NARROW_PASSAGE);
		createEOperation(carEClass, CAR___HAS_LEFT_NARROW_PASSAGE);
		createEOperation(carEClass, CAR___OBSTACLE_REACHED);
		createEOperation(carEClass, CAR___ENTERING_ALLOWED);
		createEOperation(carEClass, CAR___ENTERING_DISALLOWED);

		environmentEClass = createEClass(ENVIRONMENT);

		carToXEClass = createEClass(CAR_TO_X);
		createEReference(carToXEClass, CAR_TO_X__CARS);
		createEReference(carToXEClass, CAR_TO_X__ENVIRONMENT);
		createEReference(carToXEClass, CAR_TO_X__STREET_SECTIONS);
		createEReference(carToXEClass, CAR_TO_X__OBSTACLE_CONTROLS);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		streetSectionEClass = createEClass(STREET_SECTION);
		createEReference(streetSectionEClass, STREET_SECTION__LANES);
		createEReference(streetSectionEClass, STREET_SECTION__OBSTACLES);

		laneEClass = createEClass(LANE);
		createEReference(laneEClass, LANE__LANE_AREAS);

		laneAreaEClass = createEClass(LANE_AREA);
		createEReference(laneAreaEClass, LANE_AREA__OPPOSITE);
		createEReference(laneAreaEClass, LANE_AREA__PREVIOUS);
		createEReference(laneAreaEClass, LANE_AREA__NEXT);
		createEReference(laneAreaEClass, LANE_AREA__OBSTACLE);

		obstacleEClass = createEClass(OBSTACLE);
		createEReference(obstacleEClass, OBSTACLE__OBSTACLE_AREA);
		createEReference(obstacleEClass, OBSTACLE__CONTROLLED_BY);

		obstacleBlockingOneLaneEClass = createEClass(OBSTACLE_BLOCKING_ONE_LANE);

		obstacleControlEClass = createEClass(OBSTACLE_CONTROL);
		createEReference(obstacleControlEClass, OBSTACLE_CONTROL__CONTROLLED_OBSTACLE);
		createEReference(obstacleControlEClass, OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS);
		createEReference(obstacleControlEClass, OBSTACLE_CONTROL__REGISTERED_CARS_WAITING);
		createEOperation(obstacleControlEClass, OBSTACLE_CONTROL___REGISTER);
		createEOperation(obstacleControlEClass, OBSTACLE_CONTROL___UNREGISTER);

		driverEClass = createEClass(DRIVER);

		dashboardEClass = createEClass(DASHBOARD);
		createEOperation(dashboardEClass, DASHBOARD___SHOW_GO);
		createEOperation(dashboardEClass, DASHBOARD___SHOW_STOP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		carEClass.getESuperTypes().add(this.getNamedElement());
		environmentEClass.getESuperTypes().add(this.getNamedElement());
		carToXEClass.getESuperTypes().add(this.getNamedElement());
		streetSectionEClass.getESuperTypes().add(this.getNamedElement());
		laneEClass.getESuperTypes().add(this.getNamedElement());
		laneAreaEClass.getESuperTypes().add(this.getNamedElement());
		obstacleEClass.getESuperTypes().add(this.getNamedElement());
		obstacleBlockingOneLaneEClass.getESuperTypes().add(this.getObstacle());
		obstacleControlEClass.getESuperTypes().add(this.getNamedElement());
		driverEClass.getESuperTypes().add(this.getNamedElement());
		dashboardEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(carEClass, Car.class, "Car", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCar_InArea(), this.getLaneArea(), null, "inArea", null, 0, 1, Car.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCar_OnLane(), this.getLane(), null, "onLane", null, 0, 1, Car.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCar_ApproachingObstacle(), this.getObstacle(), null, "approachingObstacle", null, 0, 1, Car.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCar_Dashboard(), this.getDashboard(), null, "dashboard", null, 0, 1, Car.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCar_Driver(), this.getDriver(), null, "driver", null, 0, 1, Car.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCar_DrivingInDirectionOfLane(), this.getLane(), null, "drivingInDirectionOfLane", null, 0, 1, Car.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCar_Environment(), this.getEnvironment(), null, "environment", null, 0, 1, Car.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getCar__MovedToNextArea(), null, "movedToNextArea", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCar__ChangedToOppositeArea(), null, "changedToOppositeArea", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCar__MovedToNextAreaOnOvertakingLane(), null, "movedToNextAreaOnOvertakingLane", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCar__HasEnteredNarrowPassage(), null, "hasEnteredNarrowPassage", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCar__HasLeftNarrowPassage(), null, "hasLeftNarrowPassage", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCar__ObstacleReached(), null, "obstacleReached", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCar__EnteringAllowed(), null, "enteringAllowed", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getCar__EnteringDisallowed(), null, "enteringDisallowed", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(environmentEClass, Environment.class, "Environment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(carToXEClass, CarToX.class, "CarToX", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCarToX_Cars(), this.getCar(), null, "cars", null, 0, -1, CarToX.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCarToX_Environment(), this.getEnvironment(), null, "environment", null, 0, 1, CarToX.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCarToX_StreetSections(), this.getStreetSection(), null, "streetSections", null, 0, -1, CarToX.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCarToX_ObstacleControls(), this.getObstacleControl(), null, "obstacleControls", null, 0, -1, CarToX.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(streetSectionEClass, StreetSection.class, "StreetSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStreetSection_Lanes(), this.getLane(), null, "lanes", null, 0, -1, StreetSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStreetSection_Obstacles(), this.getObstacle(), null, "obstacles", null, 0, -1, StreetSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(laneEClass, Lane.class, "Lane", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLane_LaneAreas(), this.getLaneArea(), null, "laneAreas", null, 0, -1, Lane.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(laneAreaEClass, LaneArea.class, "LaneArea", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLaneArea_Opposite(), this.getLaneArea(), null, "opposite", null, 0, 1, LaneArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLaneArea_Previous(), this.getLaneArea(), this.getLaneArea_Next(), "previous", null, 0, 1, LaneArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLaneArea_Next(), this.getLaneArea(), this.getLaneArea_Previous(), "next", null, 0, 1, LaneArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLaneArea_Obstacle(), this.getObstacle(), this.getObstacle_ObstacleArea(), "obstacle", null, 0, 1, LaneArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(obstacleEClass, Obstacle.class, "Obstacle", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObstacle_ObstacleArea(), this.getLaneArea(), this.getLaneArea_Obstacle(), "obstacleArea", null, 0, 1, Obstacle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObstacle_ControlledBy(), this.getObstacleControl(), this.getObstacleControl_ControlledObstacle(), "controlledBy", null, 0, 1, Obstacle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(obstacleBlockingOneLaneEClass, ObstacleBlockingOneLane.class, "ObstacleBlockingOneLane", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(obstacleControlEClass, ObstacleControl.class, "ObstacleControl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObstacleControl_ControlledObstacle(), this.getObstacle(), this.getObstacle_ControlledBy(), "controlledObstacle", null, 0, 1, ObstacleControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObstacleControl_CarsOnNarrowPassageLaneAllowedToPass(), this.getCar(), null, "carsOnNarrowPassageLaneAllowedToPass", null, 0, -1, ObstacleControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObstacleControl_RegisteredCarsWaiting(), this.getCar(), null, "registeredCarsWaiting", null, 0, -1, ObstacleControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getObstacleControl__Register(), null, "register", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getObstacleControl__Unregister(), null, "unregister", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(driverEClass, Driver.class, "Driver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dashboardEClass, Dashboard.class, "Dashboard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getDashboard__ShowGo(), null, "showGo", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getDashboard__ShowStop(), null, "showStop", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //CartoxPackageImpl
