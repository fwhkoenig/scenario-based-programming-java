/**
 */
package cartox.impl;

import cartox.Car;
import cartox.CarToX;
import cartox.CartoxPackage;
import cartox.Environment;
import cartox.ObstacleControl;
import cartox.StreetSection;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Car To X</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link cartox.impl.CarToXImpl#getCars <em>Cars</em>}</li>
 *   <li>{@link cartox.impl.CarToXImpl#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link cartox.impl.CarToXImpl#getStreetSections <em>Street Sections</em>}</li>
 *   <li>{@link cartox.impl.CarToXImpl#getObstacleControls <em>Obstacle Controls</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CarToXImpl extends NamedElementImpl implements CarToX {
	/**
	 * The cached value of the '{@link #getCars() <em>Cars</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCars()
	 * @generated
	 * @ordered
	 */
	protected EList<Car> cars;

	/**
	 * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected Environment environment;

	/**
	 * The cached value of the '{@link #getStreetSections() <em>Street Sections</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreetSections()
	 * @generated
	 * @ordered
	 */
	protected EList<StreetSection> streetSections;

	/**
	 * The cached value of the '{@link #getObstacleControls() <em>Obstacle Controls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObstacleControls()
	 * @generated
	 * @ordered
	 */
	protected EList<ObstacleControl> obstacleControls;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CarToXImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CartoxPackage.Literals.CAR_TO_X;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Car> getCars() {
		if (cars == null) {
			cars = new EObjectContainmentEList<Car>(Car.class, this, CartoxPackage.CAR_TO_X__CARS);
		}
		return cars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getEnvironment() {
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnvironment(Environment newEnvironment, NotificationChain msgs) {
		Environment oldEnvironment = environment;
		environment = newEnvironment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR_TO_X__ENVIRONMENT, oldEnvironment, newEnvironment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironment(Environment newEnvironment) {
		if (newEnvironment != environment) {
			NotificationChain msgs = null;
			if (environment != null)
				msgs = ((InternalEObject)environment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CartoxPackage.CAR_TO_X__ENVIRONMENT, null, msgs);
			if (newEnvironment != null)
				msgs = ((InternalEObject)newEnvironment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CartoxPackage.CAR_TO_X__ENVIRONMENT, null, msgs);
			msgs = basicSetEnvironment(newEnvironment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR_TO_X__ENVIRONMENT, newEnvironment, newEnvironment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StreetSection> getStreetSections() {
		if (streetSections == null) {
			streetSections = new EObjectContainmentEList<StreetSection>(StreetSection.class, this, CartoxPackage.CAR_TO_X__STREET_SECTIONS);
		}
		return streetSections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObstacleControl> getObstacleControls() {
		if (obstacleControls == null) {
			obstacleControls = new EObjectContainmentEList<ObstacleControl>(ObstacleControl.class, this, CartoxPackage.CAR_TO_X__OBSTACLE_CONTROLS);
		}
		return obstacleControls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.CAR_TO_X__CARS:
				return ((InternalEList<?>)getCars()).basicRemove(otherEnd, msgs);
			case CartoxPackage.CAR_TO_X__ENVIRONMENT:
				return basicSetEnvironment(null, msgs);
			case CartoxPackage.CAR_TO_X__STREET_SECTIONS:
				return ((InternalEList<?>)getStreetSections()).basicRemove(otherEnd, msgs);
			case CartoxPackage.CAR_TO_X__OBSTACLE_CONTROLS:
				return ((InternalEList<?>)getObstacleControls()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CartoxPackage.CAR_TO_X__CARS:
				return getCars();
			case CartoxPackage.CAR_TO_X__ENVIRONMENT:
				return getEnvironment();
			case CartoxPackage.CAR_TO_X__STREET_SECTIONS:
				return getStreetSections();
			case CartoxPackage.CAR_TO_X__OBSTACLE_CONTROLS:
				return getObstacleControls();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CartoxPackage.CAR_TO_X__CARS:
				getCars().clear();
				getCars().addAll((Collection<? extends Car>)newValue);
				return;
			case CartoxPackage.CAR_TO_X__ENVIRONMENT:
				setEnvironment((Environment)newValue);
				return;
			case CartoxPackage.CAR_TO_X__STREET_SECTIONS:
				getStreetSections().clear();
				getStreetSections().addAll((Collection<? extends StreetSection>)newValue);
				return;
			case CartoxPackage.CAR_TO_X__OBSTACLE_CONTROLS:
				getObstacleControls().clear();
				getObstacleControls().addAll((Collection<? extends ObstacleControl>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CartoxPackage.CAR_TO_X__CARS:
				getCars().clear();
				return;
			case CartoxPackage.CAR_TO_X__ENVIRONMENT:
				setEnvironment((Environment)null);
				return;
			case CartoxPackage.CAR_TO_X__STREET_SECTIONS:
				getStreetSections().clear();
				return;
			case CartoxPackage.CAR_TO_X__OBSTACLE_CONTROLS:
				getObstacleControls().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CartoxPackage.CAR_TO_X__CARS:
				return cars != null && !cars.isEmpty();
			case CartoxPackage.CAR_TO_X__ENVIRONMENT:
				return environment != null;
			case CartoxPackage.CAR_TO_X__STREET_SECTIONS:
				return streetSections != null && !streetSections.isEmpty();
			case CartoxPackage.CAR_TO_X__OBSTACLE_CONTROLS:
				return obstacleControls != null && !obstacleControls.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CarToXImpl
