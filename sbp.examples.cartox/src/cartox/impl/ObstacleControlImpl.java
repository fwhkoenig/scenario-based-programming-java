/**
 */
package cartox.impl;

import cartox.Car;
import cartox.CartoxPackage;
import cartox.Obstacle;
import cartox.ObstacleControl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obstacle Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link cartox.impl.ObstacleControlImpl#getControlledObstacle <em>Controlled Obstacle</em>}</li>
 *   <li>{@link cartox.impl.ObstacleControlImpl#getCarsOnNarrowPassageLaneAllowedToPass <em>Cars On Narrow Passage Lane Allowed To Pass</em>}</li>
 *   <li>{@link cartox.impl.ObstacleControlImpl#getRegisteredCarsWaiting <em>Registered Cars Waiting</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObstacleControlImpl extends NamedElementImpl implements ObstacleControl {
	/**
	 * The cached value of the '{@link #getControlledObstacle() <em>Controlled Obstacle</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlledObstacle()
	 * @generated
	 * @ordered
	 */
	protected Obstacle controlledObstacle;

	/**
	 * The cached value of the '{@link #getCarsOnNarrowPassageLaneAllowedToPass() <em>Cars On Narrow Passage Lane Allowed To Pass</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCarsOnNarrowPassageLaneAllowedToPass()
	 * @generated
	 * @ordered
	 */
	protected EList<Car> carsOnNarrowPassageLaneAllowedToPass;

	/**
	 * The cached value of the '{@link #getRegisteredCarsWaiting() <em>Registered Cars Waiting</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredCarsWaiting()
	 * @generated
	 * @ordered
	 */
	protected EList<Car> registeredCarsWaiting;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstacleControlImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CartoxPackage.Literals.OBSTACLE_CONTROL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Obstacle getControlledObstacle() {
		if (controlledObstacle != null && controlledObstacle.eIsProxy()) {
			InternalEObject oldControlledObstacle = (InternalEObject)controlledObstacle;
			controlledObstacle = (Obstacle)eResolveProxy(oldControlledObstacle);
			if (controlledObstacle != oldControlledObstacle) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE, oldControlledObstacle, controlledObstacle));
			}
		}
		return controlledObstacle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Obstacle basicGetControlledObstacle() {
		return controlledObstacle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledObstacle(Obstacle newControlledObstacle, NotificationChain msgs) {
		Obstacle oldControlledObstacle = controlledObstacle;
		controlledObstacle = newControlledObstacle;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE, oldControlledObstacle, newControlledObstacle);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledObstacle(Obstacle newControlledObstacle) {
		if (newControlledObstacle != controlledObstacle) {
			NotificationChain msgs = null;
			if (controlledObstacle != null)
				msgs = ((InternalEObject)controlledObstacle).eInverseRemove(this, CartoxPackage.OBSTACLE__CONTROLLED_BY, Obstacle.class, msgs);
			if (newControlledObstacle != null)
				msgs = ((InternalEObject)newControlledObstacle).eInverseAdd(this, CartoxPackage.OBSTACLE__CONTROLLED_BY, Obstacle.class, msgs);
			msgs = basicSetControlledObstacle(newControlledObstacle, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE, newControlledObstacle, newControlledObstacle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Car> getCarsOnNarrowPassageLaneAllowedToPass() {
		if (carsOnNarrowPassageLaneAllowedToPass == null) {
			carsOnNarrowPassageLaneAllowedToPass = new EObjectResolvingEList<Car>(Car.class, this, CartoxPackage.OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS);
		}
		return carsOnNarrowPassageLaneAllowedToPass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Car> getRegisteredCarsWaiting() {
		if (registeredCarsWaiting == null) {
			registeredCarsWaiting = new EObjectResolvingEList<Car>(Car.class, this, CartoxPackage.OBSTACLE_CONTROL__REGISTERED_CARS_WAITING);
		}
		return registeredCarsWaiting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void register() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unregister() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE:
				if (controlledObstacle != null)
					msgs = ((InternalEObject)controlledObstacle).eInverseRemove(this, CartoxPackage.OBSTACLE__CONTROLLED_BY, Obstacle.class, msgs);
				return basicSetControlledObstacle((Obstacle)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE:
				return basicSetControlledObstacle(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE:
				if (resolve) return getControlledObstacle();
				return basicGetControlledObstacle();
			case CartoxPackage.OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS:
				return getCarsOnNarrowPassageLaneAllowedToPass();
			case CartoxPackage.OBSTACLE_CONTROL__REGISTERED_CARS_WAITING:
				return getRegisteredCarsWaiting();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE:
				setControlledObstacle((Obstacle)newValue);
				return;
			case CartoxPackage.OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS:
				getCarsOnNarrowPassageLaneAllowedToPass().clear();
				getCarsOnNarrowPassageLaneAllowedToPass().addAll((Collection<? extends Car>)newValue);
				return;
			case CartoxPackage.OBSTACLE_CONTROL__REGISTERED_CARS_WAITING:
				getRegisteredCarsWaiting().clear();
				getRegisteredCarsWaiting().addAll((Collection<? extends Car>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE:
				setControlledObstacle((Obstacle)null);
				return;
			case CartoxPackage.OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS:
				getCarsOnNarrowPassageLaneAllowedToPass().clear();
				return;
			case CartoxPackage.OBSTACLE_CONTROL__REGISTERED_CARS_WAITING:
				getRegisteredCarsWaiting().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE:
				return controlledObstacle != null;
			case CartoxPackage.OBSTACLE_CONTROL__CARS_ON_NARROW_PASSAGE_LANE_ALLOWED_TO_PASS:
				return carsOnNarrowPassageLaneAllowedToPass != null && !carsOnNarrowPassageLaneAllowedToPass.isEmpty();
			case CartoxPackage.OBSTACLE_CONTROL__REGISTERED_CARS_WAITING:
				return registeredCarsWaiting != null && !registeredCarsWaiting.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case CartoxPackage.OBSTACLE_CONTROL___REGISTER:
				register();
				return null;
			case CartoxPackage.OBSTACLE_CONTROL___UNREGISTER:
				unregister();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //ObstacleControlImpl
