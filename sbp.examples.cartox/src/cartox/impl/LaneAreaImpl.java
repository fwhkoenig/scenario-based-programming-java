/**
 */
package cartox.impl;

import cartox.CartoxPackage;
import cartox.LaneArea;
import cartox.Obstacle;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lane Area</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link cartox.impl.LaneAreaImpl#getOpposite <em>Opposite</em>}</li>
 *   <li>{@link cartox.impl.LaneAreaImpl#getPrevious <em>Previous</em>}</li>
 *   <li>{@link cartox.impl.LaneAreaImpl#getNext <em>Next</em>}</li>
 *   <li>{@link cartox.impl.LaneAreaImpl#getObstacle <em>Obstacle</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LaneAreaImpl extends NamedElementImpl implements LaneArea {
	/**
	 * The cached value of the '{@link #getOpposite() <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpposite()
	 * @generated
	 * @ordered
	 */
	protected LaneArea opposite;

	/**
	 * The cached value of the '{@link #getPrevious() <em>Previous</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrevious()
	 * @generated
	 * @ordered
	 */
	protected LaneArea previous;

	/**
	 * The cached value of the '{@link #getNext() <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNext()
	 * @generated
	 * @ordered
	 */
	protected LaneArea next;

	/**
	 * The cached value of the '{@link #getObstacle() <em>Obstacle</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObstacle()
	 * @generated
	 * @ordered
	 */
	protected Obstacle obstacle;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LaneAreaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CartoxPackage.Literals.LANE_AREA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea getOpposite() {
		if (opposite != null && opposite.eIsProxy()) {
			InternalEObject oldOpposite = (InternalEObject)opposite;
			opposite = (LaneArea)eResolveProxy(oldOpposite);
			if (opposite != oldOpposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.LANE_AREA__OPPOSITE, oldOpposite, opposite));
			}
		}
		return opposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea basicGetOpposite() {
		return opposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpposite(LaneArea newOpposite) {
		LaneArea oldOpposite = opposite;
		opposite = newOpposite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.LANE_AREA__OPPOSITE, oldOpposite, opposite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea getPrevious() {
		if (previous != null && previous.eIsProxy()) {
			InternalEObject oldPrevious = (InternalEObject)previous;
			previous = (LaneArea)eResolveProxy(oldPrevious);
			if (previous != oldPrevious) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.LANE_AREA__PREVIOUS, oldPrevious, previous));
			}
		}
		return previous;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea basicGetPrevious() {
		return previous;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrevious(LaneArea newPrevious, NotificationChain msgs) {
		LaneArea oldPrevious = previous;
		previous = newPrevious;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.LANE_AREA__PREVIOUS, oldPrevious, newPrevious);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrevious(LaneArea newPrevious) {
		if (newPrevious != previous) {
			NotificationChain msgs = null;
			if (previous != null)
				msgs = ((InternalEObject)previous).eInverseRemove(this, CartoxPackage.LANE_AREA__NEXT, LaneArea.class, msgs);
			if (newPrevious != null)
				msgs = ((InternalEObject)newPrevious).eInverseAdd(this, CartoxPackage.LANE_AREA__NEXT, LaneArea.class, msgs);
			msgs = basicSetPrevious(newPrevious, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.LANE_AREA__PREVIOUS, newPrevious, newPrevious));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea getNext() {
		if (next != null && next.eIsProxy()) {
			InternalEObject oldNext = (InternalEObject)next;
			next = (LaneArea)eResolveProxy(oldNext);
			if (next != oldNext) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.LANE_AREA__NEXT, oldNext, next));
			}
		}
		return next;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea basicGetNext() {
		return next;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNext(LaneArea newNext, NotificationChain msgs) {
		LaneArea oldNext = next;
		next = newNext;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.LANE_AREA__NEXT, oldNext, newNext);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNext(LaneArea newNext) {
		if (newNext != next) {
			NotificationChain msgs = null;
			if (next != null)
				msgs = ((InternalEObject)next).eInverseRemove(this, CartoxPackage.LANE_AREA__PREVIOUS, LaneArea.class, msgs);
			if (newNext != null)
				msgs = ((InternalEObject)newNext).eInverseAdd(this, CartoxPackage.LANE_AREA__PREVIOUS, LaneArea.class, msgs);
			msgs = basicSetNext(newNext, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.LANE_AREA__NEXT, newNext, newNext));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Obstacle getObstacle() {
		if (obstacle != null && obstacle.eIsProxy()) {
			InternalEObject oldObstacle = (InternalEObject)obstacle;
			obstacle = (Obstacle)eResolveProxy(oldObstacle);
			if (obstacle != oldObstacle) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.LANE_AREA__OBSTACLE, oldObstacle, obstacle));
			}
		}
		return obstacle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Obstacle basicGetObstacle() {
		return obstacle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObstacle(Obstacle newObstacle, NotificationChain msgs) {
		Obstacle oldObstacle = obstacle;
		obstacle = newObstacle;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.LANE_AREA__OBSTACLE, oldObstacle, newObstacle);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObstacle(Obstacle newObstacle) {
		if (newObstacle != obstacle) {
			NotificationChain msgs = null;
			if (obstacle != null)
				msgs = ((InternalEObject)obstacle).eInverseRemove(this, CartoxPackage.OBSTACLE__OBSTACLE_AREA, Obstacle.class, msgs);
			if (newObstacle != null)
				msgs = ((InternalEObject)newObstacle).eInverseAdd(this, CartoxPackage.OBSTACLE__OBSTACLE_AREA, Obstacle.class, msgs);
			msgs = basicSetObstacle(newObstacle, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.LANE_AREA__OBSTACLE, newObstacle, newObstacle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.LANE_AREA__PREVIOUS:
				if (previous != null)
					msgs = ((InternalEObject)previous).eInverseRemove(this, CartoxPackage.LANE_AREA__NEXT, LaneArea.class, msgs);
				return basicSetPrevious((LaneArea)otherEnd, msgs);
			case CartoxPackage.LANE_AREA__NEXT:
				if (next != null)
					msgs = ((InternalEObject)next).eInverseRemove(this, CartoxPackage.LANE_AREA__PREVIOUS, LaneArea.class, msgs);
				return basicSetNext((LaneArea)otherEnd, msgs);
			case CartoxPackage.LANE_AREA__OBSTACLE:
				if (obstacle != null)
					msgs = ((InternalEObject)obstacle).eInverseRemove(this, CartoxPackage.OBSTACLE__OBSTACLE_AREA, Obstacle.class, msgs);
				return basicSetObstacle((Obstacle)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.LANE_AREA__PREVIOUS:
				return basicSetPrevious(null, msgs);
			case CartoxPackage.LANE_AREA__NEXT:
				return basicSetNext(null, msgs);
			case CartoxPackage.LANE_AREA__OBSTACLE:
				return basicSetObstacle(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CartoxPackage.LANE_AREA__OPPOSITE:
				if (resolve) return getOpposite();
				return basicGetOpposite();
			case CartoxPackage.LANE_AREA__PREVIOUS:
				if (resolve) return getPrevious();
				return basicGetPrevious();
			case CartoxPackage.LANE_AREA__NEXT:
				if (resolve) return getNext();
				return basicGetNext();
			case CartoxPackage.LANE_AREA__OBSTACLE:
				if (resolve) return getObstacle();
				return basicGetObstacle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CartoxPackage.LANE_AREA__OPPOSITE:
				setOpposite((LaneArea)newValue);
				return;
			case CartoxPackage.LANE_AREA__PREVIOUS:
				setPrevious((LaneArea)newValue);
				return;
			case CartoxPackage.LANE_AREA__NEXT:
				setNext((LaneArea)newValue);
				return;
			case CartoxPackage.LANE_AREA__OBSTACLE:
				setObstacle((Obstacle)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CartoxPackage.LANE_AREA__OPPOSITE:
				setOpposite((LaneArea)null);
				return;
			case CartoxPackage.LANE_AREA__PREVIOUS:
				setPrevious((LaneArea)null);
				return;
			case CartoxPackage.LANE_AREA__NEXT:
				setNext((LaneArea)null);
				return;
			case CartoxPackage.LANE_AREA__OBSTACLE:
				setObstacle((Obstacle)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CartoxPackage.LANE_AREA__OPPOSITE:
				return opposite != null;
			case CartoxPackage.LANE_AREA__PREVIOUS:
				return previous != null;
			case CartoxPackage.LANE_AREA__NEXT:
				return next != null;
			case CartoxPackage.LANE_AREA__OBSTACLE:
				return obstacle != null;
		}
		return super.eIsSet(featureID);
	}

} //LaneAreaImpl
