/**
 */
package cartox.impl;

import cartox.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CartoxFactoryImpl extends EFactoryImpl implements CartoxFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CartoxFactory init() {
		try {
			CartoxFactory theCartoxFactory = (CartoxFactory)EPackage.Registry.INSTANCE.getEFactory(CartoxPackage.eNS_URI);
			if (theCartoxFactory != null) {
				return theCartoxFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CartoxFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CartoxFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CartoxPackage.CAR: return createCar();
			case CartoxPackage.ENVIRONMENT: return createEnvironment();
			case CartoxPackage.CAR_TO_X: return createCarToX();
			case CartoxPackage.NAMED_ELEMENT: return createNamedElement();
			case CartoxPackage.STREET_SECTION: return createStreetSection();
			case CartoxPackage.LANE: return createLane();
			case CartoxPackage.LANE_AREA: return createLaneArea();
			case CartoxPackage.OBSTACLE_BLOCKING_ONE_LANE: return createObstacleBlockingOneLane();
			case CartoxPackage.OBSTACLE_CONTROL: return createObstacleControl();
			case CartoxPackage.DRIVER: return createDriver();
			case CartoxPackage.DASHBOARD: return createDashboard();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Car createCar() {
		CarImpl car = new CarImpl();
		return car;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment createEnvironment() {
		EnvironmentImpl environment = new EnvironmentImpl();
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CarToX createCarToX() {
		CarToXImpl carToX = new CarToXImpl();
		return carToX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElement createNamedElement() {
		NamedElementImpl namedElement = new NamedElementImpl();
		return namedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StreetSection createStreetSection() {
		StreetSectionImpl streetSection = new StreetSectionImpl();
		return streetSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lane createLane() {
		LaneImpl lane = new LaneImpl();
		return lane;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea createLaneArea() {
		LaneAreaImpl laneArea = new LaneAreaImpl();
		return laneArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstacleBlockingOneLane createObstacleBlockingOneLane() {
		ObstacleBlockingOneLaneImpl obstacleBlockingOneLane = new ObstacleBlockingOneLaneImpl();
		return obstacleBlockingOneLane;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstacleControl createObstacleControl() {
		ObstacleControlImpl obstacleControl = new ObstacleControlImpl();
		return obstacleControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Driver createDriver() {
		DriverImpl driver = new DriverImpl();
		return driver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dashboard createDashboard() {
		DashboardImpl dashboard = new DashboardImpl();
		return dashboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CartoxPackage getCartoxPackage() {
		return (CartoxPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CartoxPackage getPackage() {
		return CartoxPackage.eINSTANCE;
	}

} //CartoxFactoryImpl
