/**
 */
package cartox.impl;

import cartox.Car;
import cartox.CartoxPackage;
import cartox.Dashboard;
import cartox.Driver;
import cartox.Environment;
import cartox.Lane;
import cartox.LaneArea;
import cartox.Obstacle;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Car</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link cartox.impl.CarImpl#getInArea <em>In Area</em>}</li>
 *   <li>{@link cartox.impl.CarImpl#getOnLane <em>On Lane</em>}</li>
 *   <li>{@link cartox.impl.CarImpl#getApproachingObstacle <em>Approaching Obstacle</em>}</li>
 *   <li>{@link cartox.impl.CarImpl#getDashboard <em>Dashboard</em>}</li>
 *   <li>{@link cartox.impl.CarImpl#getDriver <em>Driver</em>}</li>
 *   <li>{@link cartox.impl.CarImpl#getDrivingInDirectionOfLane <em>Driving In Direction Of Lane</em>}</li>
 *   <li>{@link cartox.impl.CarImpl#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CarImpl extends NamedElementImpl implements Car {
	/**
	 * The cached value of the '{@link #getInArea() <em>In Area</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInArea()
	 * @generated
	 * @ordered
	 */
	protected LaneArea inArea;

	/**
	 * The cached value of the '{@link #getOnLane() <em>On Lane</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnLane()
	 * @generated
	 * @ordered
	 */
	protected Lane onLane;

	/**
	 * The cached value of the '{@link #getApproachingObstacle() <em>Approaching Obstacle</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApproachingObstacle()
	 * @generated
	 * @ordered
	 */
	protected Obstacle approachingObstacle;

	/**
	 * The cached value of the '{@link #getDashboard() <em>Dashboard</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDashboard()
	 * @generated
	 * @ordered
	 */
	protected Dashboard dashboard;

	/**
	 * The cached value of the '{@link #getDriver() <em>Driver</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDriver()
	 * @generated
	 * @ordered
	 */
	protected Driver driver;

	/**
	 * The cached value of the '{@link #getDrivingInDirectionOfLane() <em>Driving In Direction Of Lane</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDrivingInDirectionOfLane()
	 * @generated
	 * @ordered
	 */
	protected Lane drivingInDirectionOfLane;

	/**
	 * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected Environment environment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CarImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CartoxPackage.Literals.CAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea getInArea() {
		if (inArea != null && inArea.eIsProxy()) {
			InternalEObject oldInArea = (InternalEObject)inArea;
			inArea = (LaneArea)eResolveProxy(oldInArea);
			if (inArea != oldInArea) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.CAR__IN_AREA, oldInArea, inArea));
			}
		}
		return inArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea basicGetInArea() {
		return inArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInArea(LaneArea newInArea) {
		LaneArea oldInArea = inArea;
		inArea = newInArea;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__IN_AREA, oldInArea, inArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lane getOnLane() {
		if (onLane != null && onLane.eIsProxy()) {
			InternalEObject oldOnLane = (InternalEObject)onLane;
			onLane = (Lane)eResolveProxy(oldOnLane);
			if (onLane != oldOnLane) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.CAR__ON_LANE, oldOnLane, onLane));
			}
		}
		return onLane;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lane basicGetOnLane() {
		return onLane;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnLane(Lane newOnLane) {
		Lane oldOnLane = onLane;
		onLane = newOnLane;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__ON_LANE, oldOnLane, onLane));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Obstacle getApproachingObstacle() {
		if (approachingObstacle != null && approachingObstacle.eIsProxy()) {
			InternalEObject oldApproachingObstacle = (InternalEObject)approachingObstacle;
			approachingObstacle = (Obstacle)eResolveProxy(oldApproachingObstacle);
			if (approachingObstacle != oldApproachingObstacle) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.CAR__APPROACHING_OBSTACLE, oldApproachingObstacle, approachingObstacle));
			}
		}
		return approachingObstacle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Obstacle basicGetApproachingObstacle() {
		return approachingObstacle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApproachingObstacle(Obstacle newApproachingObstacle) {
		Obstacle oldApproachingObstacle = approachingObstacle;
		approachingObstacle = newApproachingObstacle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__APPROACHING_OBSTACLE, oldApproachingObstacle, approachingObstacle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dashboard getDashboard() {
		return dashboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDashboard(Dashboard newDashboard, NotificationChain msgs) {
		Dashboard oldDashboard = dashboard;
		dashboard = newDashboard;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__DASHBOARD, oldDashboard, newDashboard);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDashboard(Dashboard newDashboard) {
		if (newDashboard != dashboard) {
			NotificationChain msgs = null;
			if (dashboard != null)
				msgs = ((InternalEObject)dashboard).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CartoxPackage.CAR__DASHBOARD, null, msgs);
			if (newDashboard != null)
				msgs = ((InternalEObject)newDashboard).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CartoxPackage.CAR__DASHBOARD, null, msgs);
			msgs = basicSetDashboard(newDashboard, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__DASHBOARD, newDashboard, newDashboard));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Driver getDriver() {
		return driver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDriver(Driver newDriver, NotificationChain msgs) {
		Driver oldDriver = driver;
		driver = newDriver;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__DRIVER, oldDriver, newDriver);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDriver(Driver newDriver) {
		if (newDriver != driver) {
			NotificationChain msgs = null;
			if (driver != null)
				msgs = ((InternalEObject)driver).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CartoxPackage.CAR__DRIVER, null, msgs);
			if (newDriver != null)
				msgs = ((InternalEObject)newDriver).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CartoxPackage.CAR__DRIVER, null, msgs);
			msgs = basicSetDriver(newDriver, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__DRIVER, newDriver, newDriver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lane getDrivingInDirectionOfLane() {
		if (drivingInDirectionOfLane != null && drivingInDirectionOfLane.eIsProxy()) {
			InternalEObject oldDrivingInDirectionOfLane = (InternalEObject)drivingInDirectionOfLane;
			drivingInDirectionOfLane = (Lane)eResolveProxy(oldDrivingInDirectionOfLane);
			if (drivingInDirectionOfLane != oldDrivingInDirectionOfLane) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.CAR__DRIVING_IN_DIRECTION_OF_LANE, oldDrivingInDirectionOfLane, drivingInDirectionOfLane));
			}
		}
		return drivingInDirectionOfLane;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lane basicGetDrivingInDirectionOfLane() {
		return drivingInDirectionOfLane;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDrivingInDirectionOfLane(Lane newDrivingInDirectionOfLane) {
		Lane oldDrivingInDirectionOfLane = drivingInDirectionOfLane;
		drivingInDirectionOfLane = newDrivingInDirectionOfLane;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__DRIVING_IN_DIRECTION_OF_LANE, oldDrivingInDirectionOfLane, drivingInDirectionOfLane));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getEnvironment() {
		if (environment != null && environment.eIsProxy()) {
			InternalEObject oldEnvironment = (InternalEObject)environment;
			environment = (Environment)eResolveProxy(oldEnvironment);
			if (environment != oldEnvironment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.CAR__ENVIRONMENT, oldEnvironment, environment));
			}
		}
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment basicGetEnvironment() {
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironment(Environment newEnvironment) {
		Environment oldEnvironment = environment;
		environment = newEnvironment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.CAR__ENVIRONMENT, oldEnvironment, environment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void movedToNextArea() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void changedToOppositeArea() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void movedToNextAreaOnOvertakingLane() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void hasEnteredNarrowPassage() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void hasLeftNarrowPassage() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void obstacleReached() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void enteringAllowed() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void enteringDisallowed() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.CAR__DASHBOARD:
				return basicSetDashboard(null, msgs);
			case CartoxPackage.CAR__DRIVER:
				return basicSetDriver(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CartoxPackage.CAR__IN_AREA:
				if (resolve) return getInArea();
				return basicGetInArea();
			case CartoxPackage.CAR__ON_LANE:
				if (resolve) return getOnLane();
				return basicGetOnLane();
			case CartoxPackage.CAR__APPROACHING_OBSTACLE:
				if (resolve) return getApproachingObstacle();
				return basicGetApproachingObstacle();
			case CartoxPackage.CAR__DASHBOARD:
				return getDashboard();
			case CartoxPackage.CAR__DRIVER:
				return getDriver();
			case CartoxPackage.CAR__DRIVING_IN_DIRECTION_OF_LANE:
				if (resolve) return getDrivingInDirectionOfLane();
				return basicGetDrivingInDirectionOfLane();
			case CartoxPackage.CAR__ENVIRONMENT:
				if (resolve) return getEnvironment();
				return basicGetEnvironment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CartoxPackage.CAR__IN_AREA:
				setInArea((LaneArea)newValue);
				return;
			case CartoxPackage.CAR__ON_LANE:
				setOnLane((Lane)newValue);
				return;
			case CartoxPackage.CAR__APPROACHING_OBSTACLE:
				setApproachingObstacle((Obstacle)newValue);
				return;
			case CartoxPackage.CAR__DASHBOARD:
				setDashboard((Dashboard)newValue);
				return;
			case CartoxPackage.CAR__DRIVER:
				setDriver((Driver)newValue);
				return;
			case CartoxPackage.CAR__DRIVING_IN_DIRECTION_OF_LANE:
				setDrivingInDirectionOfLane((Lane)newValue);
				return;
			case CartoxPackage.CAR__ENVIRONMENT:
				setEnvironment((Environment)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CartoxPackage.CAR__IN_AREA:
				setInArea((LaneArea)null);
				return;
			case CartoxPackage.CAR__ON_LANE:
				setOnLane((Lane)null);
				return;
			case CartoxPackage.CAR__APPROACHING_OBSTACLE:
				setApproachingObstacle((Obstacle)null);
				return;
			case CartoxPackage.CAR__DASHBOARD:
				setDashboard((Dashboard)null);
				return;
			case CartoxPackage.CAR__DRIVER:
				setDriver((Driver)null);
				return;
			case CartoxPackage.CAR__DRIVING_IN_DIRECTION_OF_LANE:
				setDrivingInDirectionOfLane((Lane)null);
				return;
			case CartoxPackage.CAR__ENVIRONMENT:
				setEnvironment((Environment)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CartoxPackage.CAR__IN_AREA:
				return inArea != null;
			case CartoxPackage.CAR__ON_LANE:
				return onLane != null;
			case CartoxPackage.CAR__APPROACHING_OBSTACLE:
				return approachingObstacle != null;
			case CartoxPackage.CAR__DASHBOARD:
				return dashboard != null;
			case CartoxPackage.CAR__DRIVER:
				return driver != null;
			case CartoxPackage.CAR__DRIVING_IN_DIRECTION_OF_LANE:
				return drivingInDirectionOfLane != null;
			case CartoxPackage.CAR__ENVIRONMENT:
				return environment != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case CartoxPackage.CAR___MOVED_TO_NEXT_AREA:
				movedToNextArea();
				return null;
			case CartoxPackage.CAR___CHANGED_TO_OPPOSITE_AREA:
				changedToOppositeArea();
				return null;
			case CartoxPackage.CAR___MOVED_TO_NEXT_AREA_ON_OVERTAKING_LANE:
				movedToNextAreaOnOvertakingLane();
				return null;
			case CartoxPackage.CAR___HAS_ENTERED_NARROW_PASSAGE:
				hasEnteredNarrowPassage();
				return null;
			case CartoxPackage.CAR___HAS_LEFT_NARROW_PASSAGE:
				hasLeftNarrowPassage();
				return null;
			case CartoxPackage.CAR___OBSTACLE_REACHED:
				obstacleReached();
				return null;
			case CartoxPackage.CAR___ENTERING_ALLOWED:
				enteringAllowed();
				return null;
			case CartoxPackage.CAR___ENTERING_DISALLOWED:
				enteringDisallowed();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //CarImpl
