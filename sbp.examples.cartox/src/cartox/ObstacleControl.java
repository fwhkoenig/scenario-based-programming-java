/**
 */
package cartox;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obstacle Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link cartox.ObstacleControl#getControlledObstacle <em>Controlled Obstacle</em>}</li>
 *   <li>{@link cartox.ObstacleControl#getCarsOnNarrowPassageLaneAllowedToPass <em>Cars On Narrow Passage Lane Allowed To Pass</em>}</li>
 *   <li>{@link cartox.ObstacleControl#getRegisteredCarsWaiting <em>Registered Cars Waiting</em>}</li>
 * </ul>
 *
 * @see cartox.CartoxPackage#getObstacleControl()
 * @model
 * @generated
 */
public interface ObstacleControl extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Controlled Obstacle</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link cartox.Obstacle#getControlledBy <em>Controlled By</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Obstacle</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Obstacle</em>' reference.
	 * @see #setControlledObstacle(Obstacle)
	 * @see cartox.CartoxPackage#getObstacleControl_ControlledObstacle()
	 * @see cartox.Obstacle#getControlledBy
	 * @model opposite="controlledBy"
	 * @generated
	 */
	Obstacle getControlledObstacle();

	/**
	 * Sets the value of the '{@link cartox.ObstacleControl#getControlledObstacle <em>Controlled Obstacle</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Obstacle</em>' reference.
	 * @see #getControlledObstacle()
	 * @generated
	 */
	void setControlledObstacle(Obstacle value);

	/**
	 * Returns the value of the '<em><b>Cars On Narrow Passage Lane Allowed To Pass</b></em>' reference list.
	 * The list contents are of type {@link cartox.Car}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cars On Narrow Passage Lane Allowed To Pass</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cars On Narrow Passage Lane Allowed To Pass</em>' reference list.
	 * @see cartox.CartoxPackage#getObstacleControl_CarsOnNarrowPassageLaneAllowedToPass()
	 * @model
	 * @generated
	 */
	EList<Car> getCarsOnNarrowPassageLaneAllowedToPass();

	/**
	 * Returns the value of the '<em><b>Registered Cars Waiting</b></em>' reference list.
	 * The list contents are of type {@link cartox.Car}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Cars Waiting</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Cars Waiting</em>' reference list.
	 * @see cartox.CartoxPackage#getObstacleControl_RegisteredCarsWaiting()
	 * @model
	 * @generated
	 */
	EList<Car> getRegisteredCarsWaiting();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void register();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void unregister();

} // ObstacleControl
