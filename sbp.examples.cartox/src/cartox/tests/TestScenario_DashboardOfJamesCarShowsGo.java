package cartox.tests;

import static org.junit.Assert.fail;

import cartox.Dashboard;
import cartox.Obstacle;
import cartox.collaborations.carsregisterwithobstaclecontrol.CarsRegisterWithObstacleControlCollaboration;
import cartox.runtime.CarToXObjectSystem;
import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.InterruptViolation;
import sbp.specification.scenarios.violations.SafetyViolation;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class TestScenario_DashboardOfJamesCarShowsGo extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void initialisation() {
		// No initialization
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(car, CarToXObjectSystem.getInstance().CarToX.getCars().get(0)); // JamesCar
		bindRoleToObject(env, CarToXObjectSystem.getInstance().CarToX.getEnvironment()); // Environment
		bindRoleToObject(dashboard, (Dashboard) car.getBinding().getDashboard()); // JamesCar
																					// Dashboard
		bindRoleToObject(obstacleControl, CarToXObjectSystem.getInstance().CarToX.getObstacleControls().get(0)); // Obstacle
																													// Control
		bindRoleToObject(obstacle, (Obstacle) obstacleControl.getBinding().getControlledObstacle()); // Obstacle
	}

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "movedToNextArea");
		setBlocked(env, car, "setApproachingObstacle", obstacle.getBinding());
		setBlocked(car, obstacleControl, "register");
		setBlocked(obstacleControl, car, "enteringAllowed");
		setBlocked(car, dashboard, "showGo");
		setBlocked(obstacleControl, car, "enteringDisallowed");
		setBlocked(car, dashboard, "showStop");
	}

	@Override
	protected void body() throws Violation {
		// Retrieve targeted obstacle control from object system
		request(new Message(env, car, "movedToNextArea"));
		request(STRICT, new Message(env, car, "setApproachingObstacle", obstacle.getBinding()));
		waitFor(STRICT, car, obstacleControl, "register");
		waitFor(STRICT, new Message(obstacleControl, car, "enteringAllowed"));
		waitFor(STRICT, car, dashboard, "showGo");
	}

	@Override
	protected void catchSafetyViolation(SafetyViolation e) {
		super.catchSafetyViolation(e);
		fail(e.getMessage());
	}

	@Override
	protected void catchInterruptViolation(InterruptViolation e) {
		super.catchInterruptViolation(e);
		fail(e.getMessage());
	}

}
