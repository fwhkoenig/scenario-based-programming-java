/**
 */
package cartox;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see cartox.CartoxPackage#getEnvironment()
 * @model
 * @generated
 */
public interface Environment extends NamedElement {
} // Environment
