/**
 */
package cartox;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Car</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link cartox.Car#getInArea <em>In Area</em>}</li>
 *   <li>{@link cartox.Car#getOnLane <em>On Lane</em>}</li>
 *   <li>{@link cartox.Car#getApproachingObstacle <em>Approaching Obstacle</em>}</li>
 *   <li>{@link cartox.Car#getDashboard <em>Dashboard</em>}</li>
 *   <li>{@link cartox.Car#getDriver <em>Driver</em>}</li>
 *   <li>{@link cartox.Car#getDrivingInDirectionOfLane <em>Driving In Direction Of Lane</em>}</li>
 *   <li>{@link cartox.Car#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @see cartox.CartoxPackage#getCar()
 * @model
 * @generated
 */
public interface Car extends NamedElement {
	/**
	 * Returns the value of the '<em><b>In Area</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Area</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Area</em>' reference.
	 * @see #setInArea(LaneArea)
	 * @see cartox.CartoxPackage#getCar_InArea()
	 * @model
	 * @generated
	 */
	LaneArea getInArea();

	/**
	 * Sets the value of the '{@link cartox.Car#getInArea <em>In Area</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Area</em>' reference.
	 * @see #getInArea()
	 * @generated
	 */
	void setInArea(LaneArea value);

	/**
	 * Returns the value of the '<em><b>On Lane</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Lane</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Lane</em>' reference.
	 * @see #setOnLane(Lane)
	 * @see cartox.CartoxPackage#getCar_OnLane()
	 * @model
	 * @generated
	 */
	Lane getOnLane();

	/**
	 * Sets the value of the '{@link cartox.Car#getOnLane <em>On Lane</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Lane</em>' reference.
	 * @see #getOnLane()
	 * @generated
	 */
	void setOnLane(Lane value);

	/**
	 * Returns the value of the '<em><b>Approaching Obstacle</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Approaching Obstacle</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Approaching Obstacle</em>' reference.
	 * @see #setApproachingObstacle(Obstacle)
	 * @see cartox.CartoxPackage#getCar_ApproachingObstacle()
	 * @model
	 * @generated
	 */
	Obstacle getApproachingObstacle();

	/**
	 * Sets the value of the '{@link cartox.Car#getApproachingObstacle <em>Approaching Obstacle</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Approaching Obstacle</em>' reference.
	 * @see #getApproachingObstacle()
	 * @generated
	 */
	void setApproachingObstacle(Obstacle value);

	/**
	 * Returns the value of the '<em><b>Dashboard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dashboard</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dashboard</em>' containment reference.
	 * @see #setDashboard(Dashboard)
	 * @see cartox.CartoxPackage#getCar_Dashboard()
	 * @model containment="true"
	 * @generated
	 */
	Dashboard getDashboard();

	/**
	 * Sets the value of the '{@link cartox.Car#getDashboard <em>Dashboard</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dashboard</em>' containment reference.
	 * @see #getDashboard()
	 * @generated
	 */
	void setDashboard(Dashboard value);

	/**
	 * Returns the value of the '<em><b>Driver</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Driver</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Driver</em>' containment reference.
	 * @see #setDriver(Driver)
	 * @see cartox.CartoxPackage#getCar_Driver()
	 * @model containment="true"
	 * @generated
	 */
	Driver getDriver();

	/**
	 * Sets the value of the '{@link cartox.Car#getDriver <em>Driver</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Driver</em>' containment reference.
	 * @see #getDriver()
	 * @generated
	 */
	void setDriver(Driver value);

	/**
	 * Returns the value of the '<em><b>Driving In Direction Of Lane</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Driving In Direction Of Lane</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Driving In Direction Of Lane</em>' reference.
	 * @see #setDrivingInDirectionOfLane(Lane)
	 * @see cartox.CartoxPackage#getCar_DrivingInDirectionOfLane()
	 * @model
	 * @generated
	 */
	Lane getDrivingInDirectionOfLane();

	/**
	 * Sets the value of the '{@link cartox.Car#getDrivingInDirectionOfLane <em>Driving In Direction Of Lane</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Driving In Direction Of Lane</em>' reference.
	 * @see #getDrivingInDirectionOfLane()
	 * @generated
	 */
	void setDrivingInDirectionOfLane(Lane value);

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' reference.
	 * @see #setEnvironment(Environment)
	 * @see cartox.CartoxPackage#getCar_Environment()
	 * @model
	 * @generated
	 */
	Environment getEnvironment();

	/**
	 * Sets the value of the '{@link cartox.Car#getEnvironment <em>Environment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(Environment value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void movedToNextArea();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void changedToOppositeArea();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void movedToNextAreaOnOvertakingLane();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void hasEnteredNarrowPassage();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void hasLeftNarrowPassage();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void obstacleReached();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void enteringAllowed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void enteringDisallowed();

} // Car
