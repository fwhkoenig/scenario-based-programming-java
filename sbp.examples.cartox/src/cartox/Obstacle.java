/**
 */
package cartox;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obstacle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link cartox.Obstacle#getObstacleArea <em>Obstacle Area</em>}</li>
 *   <li>{@link cartox.Obstacle#getControlledBy <em>Controlled By</em>}</li>
 * </ul>
 *
 * @see cartox.CartoxPackage#getObstacle()
 * @model abstract="true"
 * @generated
 */
public interface Obstacle extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Obstacle Area</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link cartox.LaneArea#getObstacle <em>Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Obstacle Area</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Obstacle Area</em>' reference.
	 * @see #setObstacleArea(LaneArea)
	 * @see cartox.CartoxPackage#getObstacle_ObstacleArea()
	 * @see cartox.LaneArea#getObstacle
	 * @model opposite="obstacle"
	 * @generated
	 */
	LaneArea getObstacleArea();

	/**
	 * Sets the value of the '{@link cartox.Obstacle#getObstacleArea <em>Obstacle Area</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Obstacle Area</em>' reference.
	 * @see #getObstacleArea()
	 * @generated
	 */
	void setObstacleArea(LaneArea value);

	/**
	 * Returns the value of the '<em><b>Controlled By</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link cartox.ObstacleControl#getControlledObstacle <em>Controlled Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled By</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled By</em>' reference.
	 * @see #setControlledBy(ObstacleControl)
	 * @see cartox.CartoxPackage#getObstacle_ControlledBy()
	 * @see cartox.ObstacleControl#getControlledObstacle
	 * @model opposite="controlledObstacle"
	 * @generated
	 */
	ObstacleControl getControlledBy();

	/**
	 * Sets the value of the '{@link cartox.Obstacle#getControlledBy <em>Controlled By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled By</em>' reference.
	 * @see #getControlledBy()
	 * @generated
	 */
	void setControlledBy(ObstacleControl value);

} // Obstacle
